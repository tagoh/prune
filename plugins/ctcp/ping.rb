# ping.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'prune/plugin'


class CTCP_PING < PRUNE::Plugin

  def plugin_init(opts)
    
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_prefix
    '_ctcp_ping'
  end # def plugin_prefix

  def plugin_userdefined_subsignals
    ['ctcp_ping']
  end # def plugin_userdefined_subsignals

  protected

  def _ctcp_ping_ctcp_ping_userdefined(ifs, key, *args)
    msg = args[0]
    params = args[1]

    ifs.emit(:UserDefined, 'ctcp_ping_reply',
             msg.nick, params[1..-1])

    true
  end # def _ctcp_ping_ctcp_ping_userdefined

end # class CTCP_PING
