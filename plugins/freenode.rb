# freenode.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin enables some freenode.net feature in prune.

  NickServ things:

  if you have registered your nickname to NickServ, you can identify it
  automatically with this plugin. set your password into NickPassword.
  if there are any custom IRC server has similar feature as NickServ
  but provided with the different nick name, you can change it
  with NickServName. IdentifiedMessage to change the message when NickServ
  confirmed it.

  Example:

  Plugins:
   -
     - freenode.rb
     -
       - NickServName: NickServ
       - IdentifiedMessage: You are now identified for
       - NickPassword: blahblahblah

=end

require 'prune/plugin'


class FreeNodeSupport < PRUNE::Plugin

  def plugin_init(opts)
    @identified = false
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_prefix
    '_freenode'
  end # def plugin_prefix

  protected

  def _freenode_loggedin(ifs, key, *args)
    identify_nick(ifs, key)

    false
  end # def _freenode_loggedin

  def _freenode_privmsg_received(ifs, key, *args)
    msg = args[0]

    popts = ifs.plugin_options
    unless @identified then
      identified = popts.has_key?('IdentifiedMessage') ? popts['IdentifiedMessage'] : 'You are now identified for '
      if msg.params(-1) =~ /\A#{identified}/ then
        @identified = true
      end
    end

    false
  end # def _freenode_privmsg_received

  private

  def identify_nick(ifs, key)
    return if @identified

    popts = ifs.plugin_options
    nickserv = popts.has_key?('NickServName') ? popts['NickServName'] : 'NickServ'
    if popts.has_key?('NickPassword') then
      ifs.emit_message(:Sent, PRUNE::Message::PRIVMSG.new(nickserv, sprintf("IDENTIFY %s", popts['NickPassword'])))
    end
  end # def identify_nick

end # class FreeNodeSupport
