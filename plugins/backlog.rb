# backlog.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin will send the logs during you leave IRC.

  Example:

  Plugins:
   -
     - backlog.rb
     -
       LogCount: 30
       ResendLog: true|false
       TimeFormat: %H:%M:%S
       DBFile: /path/to/db

  LogCount is the amont of the logs you want to see when you connect to
  any Prune instance. ResendLog would helps even if the last messages are
  already seen but want to see what exactly the last message was. otherwise
  the logs will be removed from the queue once it's sent.  TimeFormat is used
  to indicate on the message when the instance received.

  NOTE: This plugin is designed to work as the client-side plugin. so if you
  add this into the connectors' configuration, it won't work as expected.
  
=end

require 'pstore'
require 'prune/plugin'


class BackLog < PRUNE::Plugin

  def plugin_init(opts)
    dbfile = opts.has_key?('DBFile') ? opts['DBFile'] : "#{ENV['HOME']}/.prune/backlog.db"
    @backlogq = nil
    @idq = nil
    @dbversion = 2

    begin
      if !dbfile.nil? && !dbfile.empty? then
        p = PStore.new(dbfile)
        p.transaction do
          if p.root?('version') && @dbversion == p['version'] then
            @backlogq = p['backlog']
            @idq = p['id']
          end
        end
      end
    rescue
    ensure
      unless @backlogq.kind_of?(PRUNE::CiHash) then
        @backlogq = PRUNE::CiHash.new
      end
      unless @idq.kind_of?(PRUNE::CiHash) then
        @idq = PRUNE::CiHash.new
      end
    end
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    dbfile = opts.has_key?('DBFile') ? opts['DBFile'] : "#{ENV['HOME']}/.prune/backlog.db"

    return if dbfile.nil? || dbfile.empty?

    begin
      p = PStore.new(dbfile)
      p.transaction do
        p['version'] = @dbversion
        p['backlog'] = @backlogq
        p['id'] = @idq
        p.commit
      end
    rescue
    end
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # plugin_type

  def plugin_prefix
    '_backlog'
  end # def plugin_prefix

  protected

  def _backlog_received(ifs, key, *args)
    to_backlog(ifs, key, *args)

    false
  end # def _backlog_received

  def _backlog_sent(ifs, key, *args)
    to_backlog(ifs, key, *args)

    false
  end # def _backlog_sent

  def _backlog_lloggedin(ifs, key, *args)
    skey = args[0]

    @backlogq.keys.dup.each do |ch|
      from_backlog(ifs, skey, ch)
    end

    false
  end # def _backlog_lloggedin

  private

  def from_backlog(ifs, key, ch)
    resent = ifs.plugin_options.has_key?('ResendLog') ? ifs.plugin_options['ResendLog'] == true : false
    timeformat = ifs.plugin_options.has_key?('TimeFormat') ? ifs.plugin_options['TimeFormat'] : "%H:%M:%S"

    if @backlogq.has_key?(ch) &&
        (ifs.state.joined?(ch, ifs.state.nick) ||
         ch !~ /#{PRUNE::PATTERN::CHANNEL}/) then
      @backlogq[ch].delete_if do |m|
        dm = Marshal.load(Marshal.dump(m.instance_variable_get("@message")))
        dm[:params][-1] = "[#{m.time.strftime(timeformat)}] #{dm[:params][-1]}"
        dm[:backlog] = true
        msg = PRUNE::MessageHandler.instance.new(dm)
        ifs.emit_message(:Sent, msg, key.unicast)
        @idq[ch].delete(m.object_id) unless resent
        !resent
      end
    end
  end # def from_backlog

  def to_backlog(ifs, key, *args)
    msg = args[0]
    cnt = ifs.plugin_options.has_key?('LogCount') ? ifs.plugin_options['LogCount'] : 5

    if !msg.channel.nil? &&
        !msg.channel.empty? &&
        msg.command =~ /\APRIVMSG/ then
      unless @backlogq.include?(msg.channel({:suffix=>true})) then
        @backlogq[msg.channel({:suffix=>true})] = PRUNE::Queue.new
        @idq[msg.channel({:suffix=>true})] = PRUNE::Queue.new
      end
      rmsg = msg.instance_variable_get("@message")
      if !@idq[msg.channel({:suffix=>true})].include?(msg.object_id) &&
          !rmsg.has_key?(:backlog) &&
          !rmsg.has_key?(:highlight) then
        @backlogq[msg.channel({:suffix=>true})] << msg
        @idq[msg.channel({:suffix=>true})] << msg.object_id
        (1..(@backlogq[msg.channel({:suffix=>true})].length - cnt)).each do |i|
          @backlogq[msg.channel({:suffix=>true})].shift
          @idq[msg.channel({:suffix=>true})].shift
        end
      end
    end
  end # def to_backlog

end # class BackLog
