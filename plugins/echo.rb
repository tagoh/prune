# echo.rb - sample plugin for helper.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'prune/plugin'


class EchoPlugin < PRUNE::Plugin

  def plugin_init(opts)
    @initialized = false
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    ifs.emit(:UserDefined, 'helper_remove',
             '/echo', self)
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # def plugin_type

  def plugin_prefix
    '_echo'
  end # def plugin_prefix

  def plugin_userdefined_subsignals
    ['helper_ping']
  end # def plugin_userdefined_subsignals

  protected

  def _echo_helper_async(ifs, key, *args)
    unless @initialized then
      @initialized = true
      _echo_helper_ping_userdefined(ifs, key, *args)
    end

    false
  end # def _echo_helper_async

  def _echo_helper_ping_userdefined(ifs, key, *args)
    ifs.emit(:UserDefined, 'helper_pong',
             '/echo',
             'Send back something you typed.',
             "\\A__COMMAND__\\s+__PARAMS__\\Z",
             :_echo_echo,
             self)

    false
  end # def _echo_helper_ping_userdefined

  private

  def _echo_echo(ifs, key, ckey, channel, retval)
    ifs.emit_message(:Sent,
                     PRUNE::Message::NOTICE.new(ifs.state.nick, retval['__PARAMS__']),
                     ckey.unicast)

    # stop sending the message about this command to the server.
    true
  end # def _echo_echo

end # class EchoPlugin
