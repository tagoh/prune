# helper.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin provides a help facilities to the plugins.
  This must be loaded prior to the plugin that wants to use this feature.

  Example:

  Plugins:
   -
     - helper.rb

==How to create a plugin with the help feature

  All of the plugins needs to have plugin_userdefined_subsignals like:

  def plugin_userdefined_subsignals
    ['helper_ping']
  end

  To register your command to helper.rb:

  def plugin_init(opts)
    ...
    @initialized = false
    ...
  end

  def _yourpluginprefix_helper_async(ifs, key, *args)
    unless @initialized then
      @initialized = true
      ifs.emit(:UserDefined, 'helper_pong',
               ...)
    end

    false
  end

  def _yourpluginprefix_helper_ping_userdefined(ifs, key, *args)
    ifs.emit(:UserDefined, 'helper_pong',
             ...)

    false
  end

  For finalization:

  def plugin_finalize(ifs, opts)
    ifs.emit(:UserDefined, 'helper_remove',
             "commandname",
             self)
  end

  Signals' details are here:

  helper_ping:
    This signal prompts plugins to register the command.
    All of the plugins has to ack with 'helper_pong' signal on it.

  helper_pong:
    This signal provides the certain information to helper.rb to register
    the command.

    args[0] ... String - the command name.
    args[1] ... String - the description of the command.
    args[2] ... String|Array - the format string that is passed to PRUNE::MessageComposer.
    args[3] ... Symbol|Array - callbacks that acts something for the same position of the format.
    args[4] ... Object - the instance that contains callbacks.

    callback method:
    <method>(ifs, target, client_terget, channel, result)
    ifs           ... PRUNE::PluginInterface
    target        ... String
    client_target ... String where actually received the command from.
    channel       ... String where actually is sent the command to.
    result        ... Hash that contains the result against the specified format.

  helper_remove:
    This signal acts to unregister the command provided by your plugin.

    args[0] ... String - the command name you want to remove.
    args[1] ... Object - the instance that contains callbacks for the command.

=end

require 'prune/plugin'


class CommandHelper < PRUNE::Plugin
  HelpDescriptionStruct = Struct.new('HelpDescriptionStruct', :command, :description, :formats, :callbacks, :instance)

  def plugin_init(opts)
    @commandlist = {}
    @initialized = false
    @composer = PRUNE::MessageComposer.new
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # def plugin_type

  def plugin_prefix
    '_helper'
  end # def plugin_prefix

  def plugin_userdefined_subsignals
    ['helper_ping', 'helper_pong', 'helper_remove']
  end # def plugin_userdefined_subsignals

  protected

  def _helper_helper_async(ifs, key, *args)
    unless @initialized then
      @initialized = true
      ifs.emit(:UserDefined, 'helper_ping')
    end

    false
  end # def _helper_helper_async

  def _helper_helper_remove_userdefined(ifs, key, *args)
    command = args[0]
    ins = args[1]

    if @commandlist.include?(command) then
      s = @commandlist[command]
      if s.instance == ins then
        @commandlist.delete(command)
      else
        m = sprintf("Unauthorized access to remove `%s' command", command)
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                    m),
                         ifs.target.broadcast)
        warning(m)
      end
    else
      m = sprintf("Attempted to remove the unknown command `%s'", command)
      ifs.emit_message(:Sent,
                       PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                  m),
                       ifs.target.broadcast)
    end

    false
  end # def _helper_helper_remove_userdefined

  def _helper_helper_pong_userdefined(ifs, key, *args)
    command = args[0]
    desc = args[1]
    fmt = args[2]
    cb = args[3]
    ins = args[4]

    unless fmt.kind_of?(Array) then
      fmt = [fmt]
    end
    unless cb.kind_of?(Array) then
      cb = [cb]
    end

    if fmt.length != cb.length then
      ifs.emit_message(:Sent,
                       PRUNE::Message::NOTICE.new(ifs.state.nick, sprintf("Different size of formats and callbacks are about to be registered for %s", command)),
                       ifs.target.broadcast)
    else
      s = HelpDescriptionStruct.new(command, desc, fmt, cb, ins)
      @commandlist[command] = s
    end

    true
  end # def _helper_pong_userdefined

  def _helper_privmsg_received(ifs, key, *args)
    msg = args[0]
    ckey = args[1]

    @composer.format = "\\A\/help __COMMAND__"
    retval = @composer.parse(msg)
    if !retval.nil? then
      if @commandlist.has_key?(retval['__COMMAND__']) then
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick, @commandlist[retval['__COMMAND__']].description),
                         ckey.unicast)
      else
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick, sprintf("%s: Unknown command.", retval['__COMMAND__'])),
                         ckey.unicast)
      end
      true
    elsif msg.params(1) =~ /\A\/help\Z/ then
      if @commandlist.keys.empty? then
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick, "No plugins managed under helper.rb"),
                         ckey.unicast)
      else
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick, sprintf("Help: %s", @commandlist.keys.sort.join(','))),
                         ckey.unicast)
      end
      true
    else
      @composer.format = "\\A__COMMAND__(\\Z| __PARAMS__)"
      retval = @composer.parse(msg)
      if !retval.nil? && @commandlist.has_key?(retval['__COMMAND__']) then
        s = @commandlist[retval['__COMMAND__']]
        s.formats.each do |fmt|
          @composer.format = fmt
          r = @composer.parse(msg)
          if !r.nil? then
            idx = s.formats.index(fmt)
            cb = s.callbacks[idx]

            return s.instance.__send__(cb, ifs, key, ckey, msg.channel({:suffix=>true}), r)
          end
        end
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick, sprintf("%s: unknown parameters may be given.", retval['__COMMAND__'])),
                         ckey.unicast)

        true
      else
        false
      end
    end
  end # def _helper_privmsg_received

end # class CommandHelper
