# notifybugs.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin notifies you how much problems happened in a day.
  the notification will be sent when you are sending something or
  logging in to Prune. You'll be aware of them if it's silently
  happened. basically this plugin is for the debugging purpose only.

  Example:

  Plugins:
   -
     - notifybugs.rb
     -
       Interval: 3600
       PrivMsg: true

  this notification may be annoying. you can stop the notification
  during Interval second since it's notified last time.
  If you don't want to miss any important messages, this plugin is
  also capable to notify it as the private message. turn on PrivMsg
  option then.

=end

require 'prune/event'
require 'prune/plugin'


class NotifyBugs < PRUNE::Plugin

  def plugin_init(opts)
    @interval = opts.has_key?('Interval') ? opts['Interval'] : 3600
    @lasttime = Time.now - @interval
    @privmsg = opts['PrivMsg'] == true
    if @privmsg then
      class << ::PRUNE
        def _output(msg)
          prefixmap = {
            'FIXME!!!'=>nil,
            '[BUG]'=>'E:',
            'WARNING ***:'=>'E:',
            'INFO ***:'=>nil,
            'DEBUG ***:'=>nil,
          }
          Kernel.printf("%s\n", msg)
          # get rid of the date
          newmsg = msg.sub(/\A\[\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}\] /, '')
          prefixmap.each do |prefix, newp|
            if newmsg =~ /\A#{Regexp.escape(prefix)} (.*)/ && !newp.nil? then
              newmsg = sprintf("%s %s", newp, $1)
              evm = PRUNE::EventManager.instance
              evm.emit_notice(nil, newmsg.split("\n")[0])
              break
            end
          end
        end
      end
    end
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    if defined?(PRUNE._output) then
      class << PRUNE
        undef _output
      end
    end
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # def plugin_type

  def plugin_prefix
    '_notifybugs'
  end # def plugin_prefix

  protected

  def _notifybugs_lloggedin(ifs, key, *args)
    check_bugs(ifs, args[0])

    false
  end # def _notifybugs_lloggedin

  def _notifybugs_privmsg_received(ifs, key, *args)
    check_bugs(ifs, args[1])

    false
  end # def _notifybugs_privmsg_sent

  def _notifybugs_notice_received(ifs, key, *args)
    check_bugs(ifs, args[1])

    false
  end # def _notifybugs_notice_sent

  private

  def check_bugs(ifs, key)
    t = Time.now
    bc = []
    wc = []
    fc = []

    if t > @lasttime + @interval then
      @lasttime = t

      path = File.join(ENV['HOME'], ".prune")
      (0..1).each do |i|
        tt = t - (i * 60 * 60 * 24)
        fn = File.join(path, tt.strftime("prune_bug_%Y%m%d.log"))
        if File.exist?(fn) then
          File.open(fn) do |f|
            bc[i] = f.read.grep(/\A\[\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}\] \[BUG\]/).length
          end
        end
        fn = File.join(path, tt.strftime("prune_warning_%Y%m%d.log"))
        if File.exist?(fn) then
          File.open(fn) do |f|
            wc[i] = f.read.grep(/\A\[\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}\] WARNING \*\*\*:/).length
          end
        end
        fn = File.join(path, tt.strftime("prune_fixme_%Y%m%d.log"))
        if File.exist?(fn) then
          File.open(fn) do |f|
            fc[i] = f.read.grep(/\A\[\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}\] FIXME!!! /).length
          end
        end
      end
      unless (bc + wc + fc).map{|x| x == 0 ? nil : x}.compact.empty? then
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                    sprintf("%d bug, %d warning, %d fixme messages raised today.",
                                                            bc[0], wc[0], fc[0])),
                         key.unicast)
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                    sprintf("%d bug, %d warning, %d fixme messages raised yesterday.",
                                                            bc[1], wc[1], fc[1])),
                         key.unicast)
      end
    end

    false
  end # def check_bugs

end # class NotifyBugs
