# output.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin outputs the human readable messages to stdout by interpreting
  the raw IRC messages. this may be useful for the debuggin purpose only.

  Example:

  Plugins:
   -
     - output.rb

=end

require 'prune/plugin'
require 'prune/decoration_m'


class OutputMessage < PRUNE::Plugin

  def plugin_init(opts)
    
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # def plugin_type


  def plugin_prefix
    '_output'
  end # def plugin_prefix

  protected

  def _output_received(ifs, key, *args)
    msg = args[0]
    msg.extend PRUNE::Decoration

    printf("%s", msg.to_s)

    false
  end # def _output_received

  def _output_sent(ifs, key, *args)
    msg = args[0]
    msg.extend PRUNE::Decoration

    printf("%s", msg.to_s)

    false
  end # def _output_sent

end # class OutputMessage
