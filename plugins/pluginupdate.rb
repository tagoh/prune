# pluginupdate.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin provides the auto-updator for plugins.
  the following update mode is available:

  :mtime  - check the modification time for the plugins and if changed,
            reload the plugins then.

  :size   - check the file size for the plugins and if changed,
            reload the plugins then.

  Example:

  Plugins:
   -
     - pluginupdate.rb
     -
       UpdateMode: :mtime

  NOTE: This plugin is designed to work as the client-side plugin and
        no way of controlling the update of the plugins per the connections.

=end

require 'prune/plugin'


class PluginUpdator < PRUNE::Plugin

  def plugin_init(opts)
    @warned = false
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # def plugin_type

  def plugin_prefix
    '_plugin_updator'
  end # def plugin_prefix

  protected

  def _plugin_updator_checker_async(ifs, key, *args)
    Thread.current[:keepalive] = true
    popts = ifs.plugin_options
    cmode = popts['UpdateMode'] || :mtime
    mode = nil
    case cmode
    when :mtime
      mode = PRUNE::PluginManager::UPDATE_MTIME
    when :size
      mode = PRUNE::PluginManager::UPDATE_SIZE
    else
      warning("Unknown update mode given: %s", cmode) unless @warned
      @warned = true
    end
    pmgr = PRUNE::PluginManager.instance
    pmgr.update_plugins(mode)

    false
  end # def _plugin_updator_async

end # class PluginUpdator
