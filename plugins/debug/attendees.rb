# attendees.rb
# Copyright (C) 2009-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'uri'
require 'prune/event'
require 'prune/plugin'


class DebugAttendees < PRUNE::Plugin

  def plugin_init(opts)
    @channels = {}
    path = opts.has_key?('LogDir') ? opts['LogDir'] : File.join(ENV['HOME'], ".prune", "debug")
    Dir.glob(File.join(path, "*.log")) do |x|
      _openlog(path, x.sub(/\A#{path}/, '')) do |f, c|
        f.write(sprintf("\n\n%s *** Logging started ***\n\n", Time.now.strftime("%Y/%m/%d %H:%M:%S")))
      end
    end
    @initialized = false
    @time = Time.now
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_register(ifs)
    xids = {}
    ids = ifs.auto_registration('_debug', PRUNE::Plugin::PRIOR_ASYNC_LAST)
    xids[:client] = ids

    xids
  end # def plugin_register

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # def plugin_type

  protected

  def _debug_reset_async(ifs, target, *args)
    unless @initialized then
      return false if Time.now - @time < 10
      xids = {}
      e = PRUNE::EventManager.instance
      i = ifs.instance_variable_get("@instance")
      ObjectSpace.each_object(PRUNE::IRCProxy) do |obj|
        l = obj.instance_variable_get("@connectors").keys
        t = obj.instance_variable_get("@targets")
        l.each do |k|
          xids[k] = e.auto_registration(t[k], i, '_debug2', 1, ifs)
        end
        break
      end
      pm = PRUNE::PluginManager.instance
      ids = pm.instance_variable_get("@pluginsigids")
      xids.keys.each do |kk|
        ids.keys.each do |k|
          if k =~ /\A#{kk}.*#{i.class}\Z/ then
            ids.synchronize do
              ids[k].merge!(xids[kk])
            end
          end
        end
      end
      @initialized = true
    end

    false
  end # def _debug_reset_async

  def _debug_join_sent(ifs, target, *args)
    attendees_tracker(ifs, args[0])

    false
  end # def _debug_join_sent

  def _debug_kick_sent(ifs, target, *args)
    msg = args[0].to_s
    current_attendees(ifs, @channels[msg])
    @channels.delete(msg)

    false
  end # def _debug_kick_sent

  def _debug_nick_sent(ifs, target, *args)
    msg = args[0].to_s
    current_attendees(ifs, @channels[msg])
    @channels.delete(msg)

    false
  end # def _debug_nick_sent

  def _debug_part_sent(ifs, target, *args)
    msg = args[0].to_s
    current_attendees(ifs, @channels[msg])
    @channels.delete(msg)

    false
  end # def _debug_part_sent

  def _debug_quit_sent(ifs, target, *args)
    msg = args[0].to_s
    current_attendees(ifs, @channels[msg])
    @channels.delete(msg)

    false
  end # def _debug_quit_sent

  def _debug_353_sent(ifs, target, *args)
    attendees_tracker(ifs, args[0])

    false
  end # def _debug_353_sent

  def _debug2_kick_received(ifs, target, *args)
    msg = args[0]
    ObjectSpace.each_object(PRUNE::IRCProxy) do |obj|
      m = obj.instance_variable_get("@suffixmap")
      msg.suffix = m[target.unicast.key]
    end
    setup(ifs)
    attendees_tracker(ifs, msg)
    ch = ifs.state.channels
    ch.delete_if do |x|
      !(x =~ /\A#{msg.channel({:suffix=>true})}/)
    end
    @channels[msg.to_s] = ch

    false
  end # def _debug2_kick_received

  def _debug2_nick_received(ifs, target, *args)
    msg = args[0]
    setup(ifs)
    attendees_tracker(ifs, msg)
    ch = ifs.state.channels
    ch.delete_if do |x|
      !ifs.state.joined?(x, msg.nick)
    end
    @channels[msg.to_s] = ch

    false
  end # def _debug2_nick_received

  def _debug2_part_received(ifs, target, *args)
    msg = args[0]
    ObjectSpace.each_object(PRUNE::IRCProxy) do |obj|
      m = obj.instance_variable_get("@suffixmap")
      msg.suffix = m[target.unicast.key]
    end
    setup(ifs)
    attendees_tracker(ifs, msg)
    ch = ifs.state.channels
    ch.delete_if do |x|
      !(x =~ /\A#{msg.channel({:suffix=>true})}/)
    end
    @channels[msg.to_s] = ch

    false
  end # def _debug2_part_received

  def _debug2_quit_received(ifs, target, *args)
    msg = args[0]
    setup(ifs)
    attendees_tracker(ifs, msg)
    ch = ifs.state.channels
    ch.delete_if do |x|
      !ifs.state.joined?(x, msg.nick)
    end
    @channels[msg.to_s] = ch

    false
  end # def _debug2_quit_received

  private

  def openlog(path, prefix, channels, &block)
    channels.each do |ch|
      file = sprintf("%s_%s.log", prefix, URI.escape(ch))
      _openlog(path, file, ch, &block)
    end
  end # def openlog

  def _openlog(path, file, *args, &block)
    if !File.exist?(path) then
      FileUtils.mkdir_p(path)
    end
    if !File.directory?(path) then
      warning("%s: Unable to create the log file directory: %s", __FILE__, path)
    else
      f = File.open(File.join(path, file.downcase), "a")
      begin
        yield f, args[0]
      ensure
        f.close
      end
    end
  end # def _openlog

  def attendees_tracker(ifs, msg)
    path = ifs.plugin_options.has_key?('LogDir') ? ifs.plugin_options['LogDir'] : File.join(ENV['HOME'], ".prune", "debug")
    ch = [msg.channel({:suffix=>true})].compact
    if ch.empty? then
      ch = ifs.state.channels
      ch.delete_if do |x|
        !ifs.state.joined?(x, msg.nick)
      end
    end
    if msg.command == '353' then
      current_attendees(ifs, ch)
    else
      openlog(path, "attendees", ch) do |f, c|
        f.write(sprintf("%s %s %s: %s\n",
                        msg.time.strftime("%Y/%m/%d %H:%M:%S"),
                        msg.nick,
                        msg.command == "JOIN" ? sprintf("joined at %s", c) : msg.command == "NICK" ? sprintf("-> %s", msg.params(0)) : msg.command == "QUIT" ? "left from IRC" : msg.command == "PART" ? sprintf("left from %s", c) : msg.command == "KICK" ? sprintf("kicked from %s", c) : sprintf("got the state changed with %s", msg.command),
                        msg.command == "KICK" || msg.command == "NICK" || msg.command == "PART" || msg.command == "QUIT" ? "" : ifs.state.channels.include?(c) ? ifs.state.channel(c).nicks.inspect : sprintf("UNKNOWN (You may not join at the channel %s, but %s)", c, ifs.state.channels.inspect)))
      end
    end
  end # def attendees_tracker

  def current_attendees(ifs, channels)
    path = ifs.plugin_options.has_key?('LogDir') ? ifs.plugin_options['LogDir'] : File.join(ENV['HOME'], ".prune", "debug")

    openlog(path, "attendees", channels) do |f, c|
      f.write(sprintf("Current attendees are: %s\n", ifs.state.channel(c).nicks.inspect))
    end
  end # def current_attendees

  def setup(ifs)
    ObjectSpace.each_object(PRUNE::IRCProxy) do |obj|
      ifs.instance_variable_set("@state", obj.state)
      break
    end
  end # def setup

end # class DebugAttendees
