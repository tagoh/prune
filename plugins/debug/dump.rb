# dump.rb
# Copyright (C) 2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'prune/event'
require 'prune/plugin'


class Dump < PRUNE::Plugin

  def plugin_init(opts)
    @initialized = false
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    ifs.emit(:UserDefined, 'helper_remove',
             '/dump', self)
  end # def plugin_finalize

  def plugin_required_revision
    310
  end # def plugin_required_revision

  def plugin_prefix
    '_dump'
  end # def plugin_prefix

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # def plugin_type

  def plugin_userdefined_subsignals
    ['helper_ping']
  end # def plugin_userdefined_subsignals

  protected

  def _dump_helper_async(ifs, key, *args)
    unless @initialized then
      @initialized = true
      _dump_helper_ping_userdefined(ifs, key, *args)
    end

    false
  end # def _dump_helper_async

  def _dump_helper_ping_userdefined(ifs, key, *args)
    ifs.emit(:UserDefined, 'helper_pong',
             '/dump',
             '/dump <mode> ... - Dump callbacks registered into the system. see /dump <mode> help for more details',
             ["\\A__COMMAND__\\s+__PARAMS__\\Z", "\\A__COMMAND__\\Z"],
             [:_dump_dump, :_dump_dump],
             self)

    false
  end # def _dump_helper_ping_userdefined

  def _dump_dump(ifs, key, ckey, channel, retval)
    unless retval.include?("__PARAMS__") then
      # listing the available dump mode.
      _dump_funcs_help(ifs, key, ckey, channel, retval)
    else
      dump_mode, *opts = retval['__PARAMS__'].split(/ /)
      unless self.respond_to?(sprintf("_dump_funcs_%s", dump_mode)) then
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                    sprintf("Unknown dump mode: %s", dump_mode)),
                         ckey.unicast)
      else
        __send__(sprintf("_dump_funcs_%s", dump_mode), ifs, key, ckey, channel, opts)
      end
    end

    # stop sending the message about this command to the server.
    true
  end # def _dump_dump

  def _dump_funcs_help(ifs, key, ckey, channel, opts)
    funcs = methods.grep(/\A_dump_funcs_/).map {|x| x.to_s}
    mode = funcs.map {|x| x.to_s.sub(/_dump_funcs_/, '')}.join(',')
    if mode.empty? then
      ifs.emit_message(:Sent,
                       PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                  "No dump mode available."),
                       ckey.unicast)
    else
      ifs.emit_message(:Sent,
                       PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                  sprintf("Available dump mode: %s", mode)),
                       ckey.unicast)
    end
  end # def _dump_funcs_help

  def _dump_funcs_eventhandler(ifs, key, ckey, channel, opts)
    evm = PRUNE::EventManager.instance

    if opts.empty? || opts[0] == 'help' then
      helpmsgs = ["Usage: /dump eventhandler <parameter:value>",
                  "parameters:",
                  "target    - String to be identified the server like @blahblahblah",
                  "signal    - String to be sent the event.",
                  "subsignal - String to be sent the event.",
                 ]

      helpmsgs.each do |x|
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick, x),
                         ckey.unicast)
      end
    else
      params = {}
      opts.each do |x|
        if x =~ /\A([a-zA-Z]+):(.*)/ then
          v = $2
          if v == 'false' || v == 'true' || v == /\A\d+\Z/ then
            v = eval(v)
          end
          params[$1] = $2
        end
      end
      if params['signal'].nil? then
        ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                    "No signal specified."),
                         ckey.unicast)
      elsif !PRUNE::EventManager.is_valid_signal?(params['signal'].to_sym) then
	ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                    sprintf("Invalid signal: %s", params['signal'])),
                         ckey.unicast)
      elsif params['target'].nil? then
	ifs.emit_message(:Sent,
                         PRUNE::Message::NOTICE.new(ifs.state.nick,
                                                    "No target specified."),
                         ckey.unicast)
      else
        channel = params['target'] unless params['target'].nil?
        params['target'] = ifs.get_target(channel)
        PRUNE::Message::IO.open(ifs, "w",
                                {
                                  :target=>ckey.unicast,
                                  :signal=>:Sent,
                                  :message_class=>'NOTICE',
                                  :channel=>ifs.state.nick
                                }) do |io|
          evm.print_dispatchlist(io, params['target'], params['signal'].to_sym, params['subsignal'])
        end
      end
    end
  end # def _dump_funcs_eventhandler

  def _dump_funcs_targets(ifs, key, ckey, channel, opts)
    evm = PRUNE::EventManager.instance

    PRUNE::Message::IO.open(ifs, "w",
                            {
                              :target=>ckey.unicast,
                              :signal=>:Sent,
                              :message_class=>'NOTICE',
                              :channel=>ifs.state.nick
                            }) do |io|
      evm.targets.each do |k, v|
        io.printf("  %s", k)
      end
    end
  end # def _dump_funcs_targets


end # class DumpCallbacks
