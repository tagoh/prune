# rawlog.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'time'
require 'prune/plugin'


class RawLog < PRUNE::Plugin

  def plugin_init(opts)
    @logs = PRUNE::Queue.new
    @enabled = true
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_register(ifs)
    e = PRUNE::EventManager.instance
    i = ifs.instance_variable_get("@instance")
    t = ifs.instance_variable_get("@target")
    e.auto_registration(t, i, '_rawlog', PRUNE::EventManager::PRIOR_SYS_SYNC_EMERGENCY, ifs)
  end # def plugin_register

  protected

  def _rawlog_received(ifs, key, *args)
    return false unless args[0].kind_of?(String)
    @logs << sprintf("[%s <-] %s", Time.now.strftime("%Y-%m-%d %H:%M:%S"), args[0])

    false
  end # def _rawlog_received

  def _rawlog_sent(ifs, key, *args)
    @logs << sprintf("[%s ->] %s", Time.now.strftime("%Y-%m-%d %H:%M:%S"),args[0].to_s)

    false
  end # def _rawlog_sent

  def _rawlog_logger_async(ifs, key, *args)
    return false unless ifs.state.loggedin?
    return false unless @enabled
    return false if @logs.empty?
    
    copts = ifs.channel_options
    popts = ifs.plugin_options
    logdir = "#{ENV['HOME']}/.prune/debug"

    logdir = popts['LogDir'] if popts['LogDir'].kind_of?(String)
    unless File.exist?(logdir) then
      FileUtils.mkdir_p(logdir)
    end
    unless File.directory?(logdir) then
      warning("`%s' in LogDir option doesn't exist or not a directory.",
              logdir)
      @enabled = false
      return false
    end
    unless popts['LogFile'].kind_of?(String) then
      warning("The log file `%s' isn't valid name.", popts['LogFile'])
      @enabled = false
      return false
    end
    flag = popts['IgnorePing'] == true
    logfile = popts['LogFile']

    until @logs.empty? do
      msg = @logs.shift
      if flag && msg =~ / (?:PING|PONG) / then
        next
      end

      to_log(logdir, logfile, msg)
    end

    false
  end # def _rawlog_logger_async

  private

  def to_log(path, file, msg)
    return if file.nil? || path.nil?

    logfile = Time.parse(msg.sub(/\A\[([0-9-]+) ([0-9:]+) .*/, '\1 \2')).strftime(file)
    File.open(File.join(path, logfile), "a") do |f|
      f.write(msg)
    end
  end # def to_log

end # class RawLog
