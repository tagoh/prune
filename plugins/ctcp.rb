# ctcp.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'prune/plugin'


class CTCP < PRUNE::Plugin

  def plugin_init(opts)
    init
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_prefix
    '_ctcp'
  end # def plugin_prefix

  def plugin_userdefined_subsignals
    init
    @cmdtable.map do |k, v|
      if v.nil? then
        sprintf("ctcp_%s_reply", k.downcase)
      else
        v.map do |vv|
          sprintf("ctcp_%s_%s_reply", k.downcase, vv.downcase)
        end
      end
    end.flatten
  end # def plugin_userdefined_subsignals

  protected

  def _ctcp_privmsg_received(ifs, key, *args)
    msg = args[0]

    if msg.params(-1) =~ /\A\001/ then
      parse_ctcp(ifs, key, msg)
    end

    false
  end # def _ctcp_privmsg_received

  def _ctcp_notice_received(ifs, key, *args)
    msg = args[0]

    if msg.params(-1) =~ /\A\001/ then
      parse_ctcp(ifs, key, msg)
    end

    false
  end # def _ctcp_notice_received

  def _ctcp_ctcp_version_reply_userdefined(ifs, key, *args)
    to = args[0]
    client = args[1]
    os = args[2]
    other = args[3]

    ifs.emit_message(:Sent,
                     PRUNE::Message::NOTICE.new(to,
                                                sprintf("\001VERSION %s %s %s\001", client, os, other)))

    true
  end # def _ctcp_ctcp_version_reply_userdefined

  def _ctcp_ctcp_ping_reply_userdefined(ifs, key, *args)
    to = args[0]
    params = args[1].join(' ')

    ifs.emit_message(:Sent,
                     PRUNE::Message::NOTICE.new(to,
                                                sprintf("\001PING%s\001", params.empty? ? "" : " #{params}")))

    true
  end # def _ctcp_ctcp_ping_reply_userdefined

  private

  def init
    @initialized ||= false
    return if @initialized
    @cmdtable = {
      'VERSION'=>nil,
      'PING'=>nil,
      'CLIENTINFO'=>nil,
      'ACTION'=>nil,
      'USERINFO'=>nil,
      'TIME'=>nil,
      'DCC'=>['CHAT', 'XMIT', 'OFFER', 'SEND'],
      'XDCC'=>['LIST', 'SEND'],
      'CDCC'=>['LIST'],
      'URL'=>nil,
      'EXT'=>nil,
      'SCR'=>nil,
      'ECHO'=>nil,
      'SED'=>nil,
      'FINGER'=>nil,
      'UTC'=>nil,
      'ERRMSG'=>nil,
      'SOURCE'=>nil,
    }
    @initialized = true
  end # def init

  def parse_ctcp(ifs, key, msg)
    trailing = msg.params(-1).sub(/\A\001/, '').sub(/\001\Z/, '')
    args = trailing.split
    @cmdtable.each do |k, v|
      if args[0] == k then
        unless v.nil? then
          v.each do |vv|
            if args[1] == vv then
              ifs.emit(:UserDefined, sprintf("ctcp_%s_%s", k.downcase, vv.downcase), msg, args)
              break
            end
          end
        else
          ifs.emit(:UserDefined, sprintf("ctcp_%s", k.downcase), msg, args)
        end
      end
    end
  end # def parse_ctcp

end # class CTCP
