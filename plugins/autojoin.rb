# autojoin.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin will send JOIN command for the channels where you add AutoJoin option in your configuration.

  Examples:

  Channels:
   -
     - "#prune"
     -
       AutoJoin: true
  Plugins:
   -
     - autojoin.rb

=end

require 'prune/plugin'


class AutoJoin < PRUNE::Plugin

  def plugin_init(opts)
    
  end # def plugin_int

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_prefix
    '_autojoin'
  end # def plugin_prefix

  protected

  def _autojoin_loggedin(ifs, key, *args)
    copts = ifs.channel_options

    unless copts.nil? then
      copts.each do |ch, opts|
        next unless opts.kind_of?(Hash)

        if ifs.state.loggedin? && !ifs.state.joined?(ch, ifs.state.nick) then
          if opts.has_key?('AutoJoin') && opts['AutoJoin'] == true then
            if opts.has_key?('ChannelKey') && !opts['ChannelKey'].nil? then
              ifs.emit_message(:Sent, PRUNE::Message::JOIN.new(ch, opts['ChannelKey'], nil))
            else
              ifs.emit_message(:Sent, PRUNE::Message::JOIN.new(ch, nil))
            end
          end
        end
      end
    end

    false
  end # def _autojoin_loggedin

  def _autojoin_loadedplugin(ifs, key, *args)
    _autojoin_loggedin(ifs, key, *args)
  end # def _autojoin_loadedplugin

  def _autojoin_reloaded(ifs, key, *args)
    _autojoin_loggedin(ifs, key, *args)
  end # def _autojoin_reloaded

end # class AutoJoin
