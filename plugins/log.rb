# log.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin stores the messages into the file.

  Example:

  Plugins:
   -
     - log.rb
     -
       LogDir: /path/to/logdir
       LogFile: logfile.log
  Channels:
   -
     - "#foo"
     -
       LogFile: log_foo.log

  LogDir option allows you to specify the directory to store the log files in.
  LogFile option as the channel option is the name to store the channel
  specific logs into it. otherwise LogFile in the plugin option will be used.

=end

require 'prune/plugin'
require 'prune/decoration_m'
require 'prune/queue'


class Logging < PRUNE::Plugin

  def plugin_init(opts)
    @logs = PRUNE::Queue.new
    @enabled = true
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_prefix
    '_log'
  end # def plugin_prefix

  protected

  def _log_received(ifs, key, *args)
    @logs << args[0]

    false
  end # def _log_received

  def _log_sent(ifs, key, *args)
    @logs << args[0]

    false
  end # def _log_sent

  def _log_logger_async(ifs, key, *args)
    return false unless ifs.state.loggedin?
    return false unless @enabled
    return false if @logs.empty?

    copts = ifs.channel_options
    popts = ifs.plugin_options
    logdir = "#{ENV['HOME']}/.prune/logs"

    logdir = popts['LogDir'] if popts['LogDir'].kind_of?(String)
    unless File.exist?(logdir) then
      FileUtils.mkdir_p(logdir)
    end
    unless File.directory?(logdir) then
      warning("`%s' in LogDir option doesn't exist or not a directory.",
              logdir)
      @enabled = false
      return false
    end
    unless popts['LogFile'].kind_of?(String) then
      warning("The log file `%s' isn't valid name.", popts['LogFile'])
      @enabled = false
      return false
    end

    until @logs.empty? do
      msg = @logs.shift
      logfile = popts['LogFile']

      unless msg.channel.nil? then
        logfile = copts[msg.channel]['LogFile'] if copts[msg.channel].kind_of?(Hash) && copts[msg.channel]['LogFile'].kind_of?(String)
      end

      to_log(logdir, logfile, msg)
    end

    false
  end # def _log_logger_async

  private

  def to_log(path, file, msg)
    return if file.nil? || path.nil?

    logfile = msg.time.strftime(file)
    File.open(File.join(path, logfile), "a") do |f|
      msg.extend PRUNE::Decoration
      f.write(msg.to_s)
    end
  end # def to_log

end # class Logging
