# highlight.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin notifies you a message contains any keywords you want to
  watch out, when you connect to or send any messages.

  Example:

  Plugins:
   -
     - highlight.rb
     -
       DBFile: /path/to/db
       Keywords:
        - foo
        - bar
       ExcludePrivMsg: true|false

=end

require 'prune/plugin'
require 'prune/decoration_m'
require 'prune/pattern'
require 'prune/queue'


class MessageHighlight < PRUNE::Plugin

  def plugin_init(opts)
    dbfile = opts.has_key?('DBFile') ? opts['DBFile'] : "#{ENV['HOME']}/.prune/highlight.db"
    @q = nil
    @dbversion = 1

    begin
      if !dbfile.nil? && !dbfile.empty? then
        p = PSTore.new(dbfile)
        p.transaction do
          if p.root?('version') && @dbversion == p['version'] then
            @q = p['highlight']
          end
        end
      end
    rescue
    ensure
      unless @q.kind_of?(PRUNE::Queue) then
        @q = PRUNE::Queue.new
      end
    end
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    dbfile = opts.has_key?('DBFile') ? opts['DBFile'] : "#{ENV['HOME']}/.prune/highlight.db"

    return if dbfile.nil? || dbfile.empty?

    begin
      p = PStore.new(dbfile)
      p.transaction do
        p['version'] = @dbversion
        p['highlight'] = @q
        p.commit
      end
    rescue
    end
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_type
    PRUNE::Plugin::TYPE_LISTENER
  end # def plugin_type

  def plugin_prefix
    '_highlight'
  end # def plugin_prefix

  protected

  def _highlight_privmsg_sent(ifs, key, *args)
    check_message(ifs, args[0])

    false
  end # def _highlight_privmsg_sent

  def _highlight_notice_sent(ifs, key, *args)
    check_message(ifs, args[0])

    false
  end # def _highlight_notice_sent

  def _highlight_lloggedin(ifs, key, *args)
    skey = args[0]

    send_highlight(ifs, skey)

    false
  end # def _highlight_lloggedin

  def _highlight_privmsg_received(ifs, key, *args)
    skey = args[1]

    send_highlight(ifs, skey)

    false
  end # def _highlight_privmsg_received

  def _highlight_notice_received(ifs, key, *args)
    skey = args[1]

    send_highlight(ifs, skey)

    false
  end # def _highlight_notice_received

  private

  def check_message(ifs, msg)
    popts = ifs.plugin_options
    flag = popts.has_key?('ExcludePrivMsg') ? popts['ExcludePrivMsg'] : false

    if !flag || msg.channel =~ /#{PRUNE::PATTERN::CHANNEL}/ then
      raw = msg.instance_variable_get("@message")
      if popts.has_key?('Keywords') &&
          !raw.has_key?(:highlight) &&
          !raw.has_key?(:backlog) then
        popts['Keywords'].each do |kw|
          if msg.params(1) =~ /#{kw}/ then
            @q << msg
          end
        end
      end
    end
  end # def check_message

  def send_highlight(ifs, key)
    (1..@q.length).each do |i|
      msg = @q.shift
      msg.extend PRUNE::Decoration
      m = {
        :prefix=>{
          :nick=>msg.nick,
          :user=>msg.user,
          :host=>msg.host},
        :command=>'NOTICE',
        :params=>[ifs.state.nick, msg.to_s.chomp("\n")],
        :time=>Time.now,
        :suffix=>msg.suffix,
        :highlight=>true}
      mm = PRUNE::MessageHandler.instance.new(m)
      ifs.emit_message(:Sent, mm, key.unicast)
    end
  end # def send_highlight

end # class MessageHighlight
