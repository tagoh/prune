# ircd-ratbox.rb
# Copyright (C) 2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

=begin

==Usage:

  This plugin enables the username/nickname management by ircd-ratbox.

  UserServ things:

  if you have registered your nickname to UserServ, you can identify it
  automatically with this plugin. set your username and password into
  UserName and UserPassword.
  if there are any custom IRC server has similar feature as UserServ
  but provided with the different nick name, you can change it
  with UserServName. IdentifiedMessage to change the message when UserServ
  confirmed it.

  Example:

  Plugins:
   -
     - ircd-ratbox.rb
     -
       UserServName: USERSERV
       IdentifiedMessage: Login successful
       UserName: blahblahblah
       UserPassword: blahblahblah

=end

require 'prune/plugin'


class IrcdRatboxSupport < PRUNE::Plugin

  def plugin_init(opts)
    @identified = false
  end # def plugin_init

  def plugin_finalize(ifs, opts)
    
  end # def plugin_finalize

  def plugin_required_revision
    291
  end # def plugin_required_revision

  def plugin_prefix
    '_ircdratbox'
  end # def plugin_prefix

  protected

  def _ircdratbox_loggedin(ifs, key, *args)
    identify_user(ifs, key)

    false
  end # def _freenode_loggedin

  def _ircdratbox_notice_received(ifs, key, *args)
    msg = args[0]

    popts = ifs.plugin_options
    unless @identified then
      identified = popts.has_key?('IdentifiedMessage') ? popts['IdentifiedMessage'] : 'Login successful'
      if msg.params(-1) =~ /\A#{identified}/ then
        @identified = true
      end
    end

    false
  end # def _freenode_privmsg_received

  private

  def identify_user(ifs, key)
    return if @identified

    popts = ifs.plugin_options
    nickserv = popts.has_key?('UserServName') ? popts['NickServName'] : 'USERSERV'
    if popts.has_key?('UserName') && popts.has_key?('UserPassword') then
      ifs.emit_message(:Sent, PRUNE::Message::PRIVMSG.new(nickserv, sprintf("login %s %s", popts['UserName'], popts['UserPassword'])))
    end
  end # def identify_user

end # class IrcdRatboxSupport
