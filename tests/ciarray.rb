# ciarray.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'prune/ciarray'

class TestPRUNE__CiArray < Test::Unit::TestCase

  def setup
    
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    t = PRUNE::CiArray.new
    assert_nothing_raised {t[0] = "Foo"}
    assert_nothing_raised {t[1] = 1}
    assert_nothing_raised {t[2] = :Foo}
  end # def test_functional

  def test_include
    t = PRUNE::CiArray.new
    assert_nothing_raised {t[0] = "Foo"}
    assert_nothing_raised {t[1] = 1}
    assert_nothing_raised {t[2] = :Foo}
    assert_equal(true, t.include?("foo"))
    assert_equal(true, t.include?("fOO"))
    assert_equal(true, t.include?(1))
    assert_equal(true, t.include?(:Foo))
    assert_equal(false, t.include?("1"))
    assert_equal(true, t.include?(:foo))
  end # def test_include

  def test_delete
    t = PRUNE::CiArray.new
    assert_nothing_raised {t[0] = "Foo"}
    assert_nothing_raised {t[1] = 1}
    assert_nothing_raised {t[2] = :Foo}
    assert_nothing_raised {t.delete("fOO")}
    assert_equal(false, t.include?("foo"))
    assert_nothing_raised {t.delete(1)}
    assert_equal(false, t.include?(1))
    assert_nothing_raised {t.delete(:Foo)}
    assert_equal(false, t.include?(:Foo))
  end # def test_delete

end # class TestPRUNE__CiArray

if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
