# pseudoio.rb
# Copyright (C) 2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

$:.push(File.join(File.dirname($0), '..', 'prune', '.libs'))
require 'tmpdir'
require 'pseudoio'

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'tempfile'


class TestPRUNE__PseudoIO < Test::Unit::TestCase

  def setup
    
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(ArgumentError) {PRUNE::PseudoIO.new(nil)}
    assert_raises(TypeError) {PRUNE::PseudoIO.new(nil, nil)}
    assert_raises(TypeError) {PRUNE::PseudoIO.new(1, nil)}
    assert_raises(TypeError) {PRUNE::PseudoIO.new({}, nil)}
    assert_raises(TypeError) {PRUNE::PseudoIO.new("", nil)}
  end # def test_functional

  def test_func_io_r
    tmp = Tempfile.new("pseudoiotestXXXXXXXX")
    File.open(tmp.path, "w") do |f|
      [1,2,3].each do |i|
        f.write(sprintf("%s\n", i))
      end
    end
    x = File.open(tmp.path,"r")
    t = nil
    assert_raises(ArgumentError) {PRUNE::PseudoIO.new(x,"blah")}
    assert_nothing_raised {t = PRUNE::PseudoIO.new(x,"r")}
    assert_equal("1\n", t.readline)
    assert_equal("2\n", t.readline)
    assert_equal("3\n", t.readline)
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
    assert_raises(IOError) {t.write("foo\n")}
    tmp.close
  end # def test_func_io_r

  def test_func_io_w
    tmp = Tempfile.new("pseudoiotestXXXXXXXX")
    x = File.open(tmp.path,"w")
    t = nil
    assert_raises(ArgumentError) {PRUNE::PseudoIO.new(x,"blah")}
    assert_nothing_raised {t = PRUNE::PseudoIO.new(x,"w")}
    assert_nothing_raised {t.write("foo\n")}
    assert_nothing_raised {t.write("bar\n")}
    assert_nothing_raised {t.write("b")}
    assert_nothing_raised {t.write("a")}
    assert_nothing_raised {t.write("z")}
    assert_nothing_raised {t.write("\n")}
    assert_nothing_raised {t.flush}
    x.close
    tmp.open do |f|
      assert_equal("foo\nbar\nbaz\n", f.read)
    end
    assert_raises(IOError) {t.read}
  end # def test_func_io_w

  def test_func_io_rw
    tmp = Tempfile.new("pseudoiotestXXXXXXXX")
    File.open(tmp.path, "w") do |f|
      [1,2,3].each do |i|
        f.write(sprintf("%s\n", i))
      end
    end
    x = File.open(tmp.path,"r")
    tmp2 = Tempfile.new("pseudoiotestXXXXXXXX")
    y = File.open(tmp2.path,"w")
    t = nil
#    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new(x,"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([x,2],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([2,y],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([x,2,3],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([x,y,3],"r+")}
#    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new(x,"w+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([x,2],"w+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([2,y],"w+")}
    l = nil
    assert_nothing_raised {t = PRUNE::PseudoIO.new([x,y],"w+")}
    assert_equal("1\n", l = t.readline)
    assert_nothing_raised {t.write("x" + l)}
    assert_equal("2\n", l = t.readline)
    assert_nothing_raised {t.write("y" + l)}
    assert_equal("3\n", l = t.readline)
    assert_nothing_raised {t.write("z" + l)}
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
    x.close
    tmp.open do |f|
      assert_equal("x1\ny2\nz3\n", x.read)
    end
  end # def test_func_io_rw

  def test_func_array_r
    t = nil
    assert_raises(ArgumentError) {PRUNE::PseudoIO.new([1,2,3],"blah")}
    x = [1,2,3]
    assert_nothing_raised {t = PRUNE::PseudoIO.new(x,"r")}
    assert_equal("1\n", t.readline)
    assert_equal("2\n", t.readline)
    assert_equal("3\n", t.readline)
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
    assert_raises(IOError) {t.write("foo\n")}
    x << 4
    assert_nothing_raised {t.flush}
    assert_equal(false, t.eof?)
    assert_equal("4\n", t.readline)
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
  end # def test_func_array_r

  def test_func_array_w
    t = nil
    assert_raises(ArgumentError) {PRUNE::PseudoIO.new([1,2,3],"blah")}
    x = []
    assert_nothing_raised {t = PRUNE::PseudoIO.new(x,"w")}
    assert_nothing_raised {t.write("foo\n")}
    assert_nothing_raised {t.write("bar\n")}
    assert_nothing_raised {t.write("b")}
    assert_nothing_raised {t.write("a")}
    assert_nothing_raised {t.write("z")}
    assert_nothing_raised {t.write("\n")}
    assert_nothing_raised {t.flush}
    assert_equal(["foo\n","bar\n","baz\n"], x)
    assert_raises(IOError) {t.read}
  end # def test_func_array_w

  def test_func_array_rw
    t = nil
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([1,2,3],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([1,2],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([[],2],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([2,[]],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([1,2,3],"w+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([1,2],"w+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([[],2],"w+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([2,[]],"w+")}
    x = [1,2,3]
    y = []
    l = nil
    assert_nothing_raised {t = PRUNE::PseudoIO.new([x,y],"w+")}
    assert_equal("1\n", l = t.readline)
    assert_nothing_raised {t.write("x" + l)}
    assert_equal("2\n", l = t.readline)
    assert_nothing_raised {t.write("y" + l)}
    assert_equal("3\n", l = t.readline)
    assert_nothing_raised {t.write("z" + l)}
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
    assert_nothing_raised {t.flush}
    assert_equal([], x)
    assert_equal(["x1\n","y2\n","z3\n"], y)
    x << 4
    assert_nothing_raised {t.flush}
    assert_equal(false, t.eof?)
    assert_equal("4\n", l = t.readline)
    assert_nothing_raised {t.write("a" + l)}
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
    assert_nothing_raised {t.flush}
    assert_equal([], x)
    assert_equal(["x1\n","y2\n","z3\n","a4\n"], y)
  end # def test_func_array_rw

  def test_func_stringio_r
    t = nil
    x = StringIO.new("","w+")
    [1,2,3].each do |i|
      x.printf("%s\n", i)
    end
    pos = x.seek(0, File::SEEK_SET)
    assert_raises(ArgumentError) {PRUNE::PseudoIO.new(x,"blah")}
    assert_nothing_raised {t = PRUNE::PseudoIO.new(x,"r")}
    assert_equal("1\n", t.readline)
    assert_equal("2\n", t.readline)
    assert_equal("3\n", t.readline)
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
    assert_raises(IOError) {t.write("foo\n")}
    pos = x.pos
    x.puts("4\n")
    x.seek(pos, File::SEEK_SET)
    assert_nothing_raised {t.flush}
    assert_equal(false, t.eof?)
    assert_equal("4\n", t.readline)
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
  end # def test_func_stringio_r

  def test_func_stringio_w
    t = nil
    x = StringIO.new("","w+")
    assert_raises(ArgumentError) {PRUNE::PseudoIO.new(x,"blah")}
    assert_nothing_raised {t = PRUNE::PseudoIO.new(x,"w")}
    assert_nothing_raised {t.write("foo\n")}
    assert_nothing_raised {t.write("bar\n")}
    assert_nothing_raised {t.write("b")}
    assert_nothing_raised {t.write("a")}
    assert_nothing_raised {t.write("z")}
    assert_nothing_raised {t.write("\n")}
    assert_nothing_raised {t.flush}
    assert_equal("foo\nbar\nbaz\n", x.string)
    assert_raises(IOError) {t.read}
  end # def test_func_stringio_w

  def test_func_stringio_rw
    t = nil
    x = StringIO.new("","w+")
    y = StringIO.new("","w+")
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new(x,"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([x,2],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([2,y],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([x,2,3],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([x,y,3],"r+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new(x,"w+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([x,2],"w+")}
    assert_raises(ArgumentError) {t = PRUNE::PseudoIO.new([2,y],"w+")}
    l = nil
    [1,2,3].each do |i|
      x.printf("%s\n",i)
    end
    x.seek(0, File::SEEK_SET);
    assert_nothing_raised {t = PRUNE::PseudoIO.new([x,y],"w+")}
    assert_equal("1\n", l = t.readline)
    assert_nothing_raised {t.write("x" + l)}
    assert_equal("2\n", l = t.readline)
    assert_nothing_raised {t.write("y" + l)}
    assert_equal("3\n", l = t.readline)
    assert_nothing_raised {t.write("z" + l)}
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
    assert_nothing_raised {t.flush}
    assert_equal("x1\ny2\nz3\n", y.string)
    pos = x.pos
    x.puts("4\n")
    x.seek(pos, File::SEEK_SET)
    assert_nothing_raised {t.flush}
    assert_equal(false, t.eof?)
    assert_equal("4\n", l = t.readline)
    assert_nothing_raised {t.write("a" + l)}
    assert_equal(true, t.eof?)
    assert_raises(EOFError) {t.readline}
    assert_nothing_raised {t.flush}
    assert_equal("x1\ny2\nz3\na4\n", y.string)
  end # def test_func_stringio_rw

end # class TestPRUNE__PseudoIO

if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
