# queue.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'prune/queue'

class TestPRUNE__Queue < Test::Unit::TestCase

  def setup
  end # def setup

  def teardown
  end # def teardown

  def test_clear
    t = PRUNE::Queue.new
    assert_nothing_raised {t.push(1)}
    assert_equal("[]", t.clear.inspect)
    assert_equal("[]", t.clear.inspect)
  end # def test_clear

  def test_delete
    t = PRUNE::Queue.new
    assert_nothing_raised {t.push("foo")}
    assert_nothing_raised {t.push(1)}
    assert_equal("foo", t.delete("foo"))
    assert_equal("[1]", t.inspect)
  end # def test_delete

  def test_delete_if
    t = PRUNE::Queue.new
    assert_nothing_raised {t.push(true)}
    assert_nothing_raised {t.push(false)}
    assert_nothing_raised {t.push(true)}
    assert_nothing_raised {t.push(false)}
    assert_nothing_raised {t.delete_if {|x| x}}
    assert_equal("[false, false]", t.inspect)
  end # def test_delete_if

  def test_each
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push("foo")}
    assert_nothing_raised {t.push(1)}
    v = []
    assert_nothing_raised {t.each {|x|v<<x}}
    assert_equal(["foo", 1], v)
  end # def test_each

  def test_empty?
    t = PRUNE::Queue.new

    assert_equal(true, t.empty?)
    assert_nothing_raised {t.push(1)}
    assert_equal(false, t.empty?)
    assert_nothing_raised {t.shift}
    assert_equal(true, t.empty?)
  end # def test_empty?

  def test_include
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push("foo")}
    assert_nothing_raised {t.push(1)}
    assert_equal(true, t.include?("foo"))
    assert_equal(true, t.include?(1))
    assert_equal(false, t.include?("fOO"))
    assert_equal(false, t.include?("1"))
  end # def test_include

  def test_push
    t = PRUNE::Queue.new

    assert_equal("[nil]", t.push(nil).inspect)
    assert_equal("[nil, 0]", t.push(0).inspect)
    assert_equal("[nil, 0, [1, 2, 3]]", t.push([1, 2, 3]).inspect)
    assert_equal("[nil, 0, [1, 2, 3], {\"foo\"=>0}]", t.push({"foo"=>0}).inspect)

    t = PRUNE::Queue.new
    assert_equal("[[1]]", t.push([1]).inspect)
  end # def test_push

  def test_reverse
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push("foo")}
    assert_nothing_raised {t.push(1)}
    assert_equal('[1, "foo"]', t.reverse.inspect)
  end # def test_reverse

  def test_reverse_each
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push("foo")}
    assert_nothing_raised {t.push(1)}
    v = []
    assert_nothing_raised {t.reverse_each{|x|v<<x}}
    assert_equal([1, "foo"], v)
  end # def test_reverse_each

  def test_shift
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push(0)}
    assert_equal(0, t.shift)
    assert_nothing_raised {t.push("abc")}
    assert_equal("abc", t.shift)
    assert_nothing_raised {t.push([1, 2, 3])}
    assert_equal([1, 2, 3], t.shift)
    assert_nothing_raised {t.push([1])}
    assert_equal([1], t.shift)
    assert_nothing_raised {t.push({"foo"=>1})}
    assert_equal({"foo"=>1}, t.shift)
  end # def test_shift

  def test_size
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push("foo")}
    assert_nothing_raised {t.push(1)}
    assert_equal(2, t.size)
  end # def test_size

  def test_sort
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push(1)}
    assert_nothing_raised {t.push("foo")}
    assert_raises(ArgumentError, "comparison of Fixnum with String failed") {t.sort}

    t = PRUNE::Queue.new
    assert_nothing_raised {t.push(5)}
    assert_nothing_raised {t.push(2)}
    assert_nothing_raised {t.push(4)}
    assert_nothing_raised {t.push(1)}
    t2 = nil
    assert_nothing_raised {t2 = t.sort}
    assert_equal('[1, 2, 4, 5]', t2.inspect)
    assert_equal(true, t2.kind_of?(PRUNE::Queue))
  end # def test_sort

  def test_sort_
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push(1)}
    assert_nothing_raised {t.push("foo")}
    assert_raises(ArgumentError, "comparison of Fixnum with String failed") {t.sort!}

    t = PRUNE::Queue.new
    assert_nothing_raised {t.push(5)}
    assert_nothing_raised {t.push(2)}
    assert_nothing_raised {t.push(4)}
    assert_nothing_raised {t.push(1)}
    assert_nothing_raised {t.sort!}
    assert_equal('[1, 2, 4, 5]', t.inspect)
    assert_equal(true, t.kind_of?(PRUNE::Queue))
  end # def test_sort_

  def test_to_a
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push(1)}
    assert_nothing_raised {t.push("foo")}
    assert_equal([1, "foo"], t.to_a)
  end # def test_to_a

  def test_unshift
    t = PRUNE::Queue.new

    assert_nothing_raised {t.push(1)}
    assert_nothing_raised {t.push("foo")}
    v = nil
    assert_nothing_raised {v = t.shift}
    assert_nothing_raised {t.unshift(v)}
    assert_equal([1, "foo"], t.to_a)
  end # def test_unshift

  def test_set_array
    t = PRUNE::Queue.new

    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[2] = "bar"}
    assert_equal(nil, t[0])
    assert_equal([nil, "foo", "bar"], t.to_a)
  end # def test_set_array

end # class TestPRUNE__Queue

if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
