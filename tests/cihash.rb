# cihash.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rbconfig'
require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'prune/cihash'

class TestPRUNE__CiHash < Test::Unit::TestCase

  def setup

  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
  end # def test_functional

  def test_set_array
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    assert_equal(1, t["Foo"])
    assert_equal(1, t["foo"])
    assert_equal("foo", t[1])
    assert_equal(:bar, t[:Foo])
    assert_equal(:bar, t[:foo])
    assert_equal(nil, t["1"])
  end # def test_set_array

  def test_clear
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    assert_nothing_raised {t.clear}
    assert_equal(0, t.length)
    assert_equal(nil, t["Foo"])
    assert_equal(nil, t["foo"])
    assert_equal(nil, t[1])
    assert_equal(nil, t[:Foo])
  end # def test_clear

  def test_delete
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    assert_nothing_raised {t.delete("foo")}
    assert_equal(nil, t["Foo"])
    assert_equal(nil, t["foo"])
    assert_equal("foo", t[1])
    assert_equal(:bar, t[:Foo])
    assert_nothing_raised {t["#foo"] = 1}
    assert_equal(1, t["#foo"])
    assert_nothing_raised {t.delete("#foo")}
    assert_equal(nil, t["#foo"])
  end # def test_delete

  def test_fetch
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    assert_equal(1, t.fetch("Foo"))
    assert_equal(1, t.fetch("foo"))
    assert_equal("foo", t.fetch(1))
    assert_equal(:bar, t.fetch(:Foo))
    assert_equal("blahblahblah", t.fetch("Bar","blahblahblah"))
    assert_equal("No bar", t.fetch("bar"){|x| "No #{x}"})
    assert_equal(1, t.fetch("foo"){|x|x+1})
    if RbConfig::CONFIG['ruby_version'] >= "1.9.1" then
      assert_raises(KeyError) {t.fetch("Bar")}
    else
      assert_raises(IndexError) {t.fetch("Bar")}
    end
  end # def test_fetch

  def test_values_at
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    assert_equal([1, 1, "foo"], t.values_at("Foo", "foo", 1))
    assert_equal([:bar, :bar], t.values_at(:Foo, :foo))
  end # def test_values_at

  def test_has_key
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    assert_equal(true, t.has_key?("foo"))
    assert_equal(true, t.has_key?("fOO"))
    assert_equal(true, t.has_key?("Foo"))
    assert_equal(true, t.has_key?(:foo))
    assert_equal(true, t.has_key?(1))
    assert_equal(true, t.has_key?(:Foo))
  end # def test_has_key

  def test_to_origkey
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    assert_equal("Foo", t.to_origkey("fOO"))
    assert_equal(1, t.to_origkey(1))
    assert_equal(nil, t.to_origkey("1"))
    assert_equal(:Foo, t.to_origkey(:Foo))
    assert_equal(:Foo, t.to_origkey(:foo))
  end # def test_to_origkey

  def test_each_key
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    v = []
    assert_nothing_raised {t.each_key{|x|v<<x}}
    assert_equal(true, v.include?("foo"))
    assert_equal(true, v.include?(1))
    assert_equal(false, v.include?(:Foo))
    assert_equal(false, v.include?("Foo"))
    assert_equal(true, v.include?(:foo))
  end # def test_each_key

  def test_each_origkey
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    v = []
    assert_nothing_raised {t.each_origkey{|x|v<<x}}
    assert_equal(true, v.include?("Foo"))
    assert_equal(true, v.include?(1))
    assert_equal(true, v.include?(:Foo))
    assert_equal(false, v.include?("foo"))
    assert_equal(false, v.include?("fOO"))
    assert_equal(false, v.include?("1"))
    assert_equal(false, v.include?(:foo))
  end # def test_each_origkey

  def test_each_pair
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    k = []
    v = []
    assert_nothing_raised {t.each_pair{|x,y|k<<x;v<<y}}
    assert_equal(true, k.include?("foo"))
    assert_equal(1, v[k.index("foo")])
    assert_equal(true, k.include?(1))
    assert_equal("foo", v[k.index(1)])
    assert_equal(false, k.include?(:Foo))
    assert_equal(nil, k.index(:Foo))
    assert_equal(false, k.include?("fOO"))
    assert_equal(false, k.include?("1"))
    assert_equal(true, k.include?(:foo))
  end # def test_each_pair

  def test_each_origpair
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    k = []
    v = []
    assert_nothing_raised {t.each_origpair{|x,y|k<<x;v<<y}}
    assert_equal(true, k.include?("Foo"))
    assert_equal(1, v[k.index("Foo")])
    assert_equal(true, k.include?(1))
    assert_equal("foo", v[k.index(1)])
    assert_equal(true, k.include?(:Foo))
    assert_equal(:bar, v[k.index(:Foo)])
    assert_equal(false, k.include?("fOO"))
    assert_equal(false, k.include?("foo"))
    assert_equal(false, k.include?("1"))
    assert_equal(false, k.include?(:foo))
  end # def test_each_origpair

  def test_keys
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    v = nil
    assert_nothing_raised {v = t.keys}
    assert_equal(true, v.include?("foo"))
    assert_equal(true, v.include?(1))
    assert_equal(false, v.include?(:Foo))
    assert_equal(false, v.include?("Foo"))
    assert_equal(false, v.include?("fOO"))
    assert_equal(false, v.include?("1"))
    assert_equal(true, v.include?(:foo))
  end # def test_keys

  def test_origkeys
    t = PRUNE::CiHash.new
    assert_nothing_raised {t["Foo"] = 1}
    assert_nothing_raised {t[1] = "foo"}
    assert_nothing_raised {t[:Foo] = :bar}
    v = nil
    assert_nothing_raised {v = t.origkeys}
    assert_equal(true, v.include?("Foo"))
    assert_equal(true, v.include?(1))
    assert_equal(true, v.include?(:Foo))
    assert_equal(false, v.include?("foo"))
    assert_equal(false, v.include?("fOO"))
    assert_equal(false, v.include?("1"))
    assert_equal(false, v.include?(:foo))
  end # def test_keys

end # class TestPRUNE__CiHash

if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
