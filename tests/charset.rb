# charset.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'prune/charset'

class TestPRUNE__Charset < Test::Unit::TestCase

  def setup
    
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_nothing_raised {t = PRUNE::Charset.new}
  end # def test_functional

  def test_default
    t = PRUNE::Charset.new
    assert_equal(nil, t.default)
    assert_equal(nil, t[:foo])
    assert_nothing_raised {t.default = 'utf-8'}
    assert_equal('utf-8', t.default)
    assert_equal('utf-8', t[:foo])
    assert_raises(PRUNE::Error::InvalidCharset) {t.default = "foo"}
    assert_equal('utf-8', t.default)
  end # def test_default

  def test_set
    t = PRUNE::Charset.new
    assert_nothing_raised {t.default = 'utf-8'}
    assert_nothing_raised {t[:foo] = 'euc-jp'}
    assert_equal('utf-8', t[:bar])
    assert_equal('euc-jp', t[:foo])
    assert_raises(PRUNE::Error::InvalidCharset) {t[:baz] = 'foo'}
    assert_equal('utf-8', t[:baz])
  end # def test_set

end # class TestPRUNE__Charset

if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
