# composer.rb
# Copyright (C) 2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'prune/composer'
require 'prune/message'



class TestPRUNE__MessageComposer < Test::Unit::TestCase

  def setup
    @t = PRUNE::MessageComposer.new
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_nothing_raised {t = PRUNE::MessageComposer.new}
    assert_nothing_raised {t = PRUNE::MessageComposer.new("__PARAM__")}
  end # def test_functional

  def test_format_
    # should be done in test_each_symbol.
  end # def test_format_

  def test_each_symbol
    assert_nothing_raised {@t.format = "\\A__COMMAND__\\Z"}
    assert_equal(["__COMMAND__"], @t.each_symbol {|x|})
    assert_nothing_raised {@t.format = "\\A__COMMAND__ __PARAMS__\\Z"}
    assert_equal(["__COMMAND__", "__PARAMS__"], @t.each_symbol {|x|})
  end # def test_each_symbol

  def test_parse
    assert_nothing_raised {@t.format = "\\A__COMMAND__ __PARAMS__\\Z"}
    assert_equal(["abc", "def"], @t.parse(PRUNE::Message::PRIVMSG.new("#foo", "abc def")).map {|k, v| v})
    assert_equal(nil, @t.parse(PRUNE::Message::PRIVMSG.new("#foo", "abc")))

    assert_nothing_raised {@t.format = "\\A__COMMAND__(?:\\Z| __PARAMS__)"}
    assert_nothing_raised {@t.parse(PRUNE::Message::PRIVMSG.new("#foo", "blahblah(blahblah"))}
    assert_equal({"__COMMAND__"=>"blahblah(blahblah"},  @t.parse(PRUNE::Message::PRIVMSG.new("#foo", "blahblah(blahblah")))
    assert_equal(["abc(abc", "def"],  @t.parse(PRUNE::Message::PRIVMSG.new("#foo", "abc(abc def")).map {|k, v| v})
  end # def test_parse

  def test_compose
    
  end # def test_compose

end # class TestPRUNE__MessageComposer

if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
