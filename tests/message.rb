# message.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'prune/message'

class TestPRUNE__MessageHandler < Test::Unit::TestCase

  def setup
    @t = PRUNE::MessageHandler.instance
  end # def setup

  def teardown
    
  end # def teardown

  def test_new
    assert_nothing_raised {@t.new({
                                  :prefix=>{
                                    :nick=>'foo',
                                    :user=>'foo',
                                    :host=>'example.com'},
                                  :command=>'328',
                                  :params=>['foo','blahblahblah']
                                })}
  end # def test_new

end # class TestPRUNE__MessageHandler

class TestPRUNE__Message < Test::Unit::TestCase

  def setup
    
  end # def setup

  def teardown
    
  end # def teardown

  def test_normalize
    assert_equal("FOO", PRUNE::Message.normalize("foo"))
    assert_equal("FOO", PRUNE::Message.normalize(:foo))
    assert_equal("RPL_WELCOME", PRUNE::Message.normalize("001"))
    assert_equal("RPL_YOURHOST", PRUNE::Message.normalize("002"))
    assert_equal("RPL_CREATED", PRUNE::Message.normalize("003"))
    assert_equal("RPL_MYINFO", PRUNE::Message.normalize("004"))
    assert_equal("RPL_BOUNCE", PRUNE::Message.normalize("005"))
    assert_equal("RPL_TRACELINK", PRUNE::Message.normalize("200"))
    assert_equal("RPL_TRACECONNECTING", PRUNE::Message.normalize("201"))
    assert_equal("RPL_TRACEHANDSHAKE", PRUNE::Message.normalize("202"))
    assert_equal("RPL_TRACEUNKNOWN", PRUNE::Message.normalize("203"))
    assert_equal("RPL_TRACEOPERATOR", PRUNE::Message.normalize("204"))
    assert_equal("RPL_TRACEUSER", PRUNE::Message.normalize("205"))
    assert_equal("RPL_TRACESERVER", PRUNE::Message.normalize("206"))
    assert_equal("RPL_TRACESERVICE", PRUNE::Message.normalize("207"))
    assert_equal("RPL_TRACENEWTYPE", PRUNE::Message.normalize("208"))
    assert_equal("RPL_TRACECLASS", PRUNE::Message.normalize("209"))
    assert_equal("RPL_TRACERECONNECT", PRUNE::Message.normalize("210"))
    assert_equal("RPL_STATSLINKINFO", PRUNE::Message.normalize("211"))
    assert_equal("RPL_STATSCOMMANDS", PRUNE::Message.normalize("212"))
    assert_equal("RPL_STATSCLINE", PRUNE::Message.normalize("213"))
    assert_equal("RPL_STATSNLINE", PRUNE::Message.normalize("214"))
    assert_equal("RPL_STATSILINE", PRUNE::Message.normalize("215"))
    assert_equal("RPL_STATSKLINE", PRUNE::Message.normalize("216"))
    assert_equal("RPL_STATSQLINE", PRUNE::Message.normalize("217"))
    assert_equal("RPL_STATSYLINE", PRUNE::Message.normalize("218"))
    assert_equal("RPL_ENDOFSTATS", PRUNE::Message.normalize("219"))
    assert_equal("RPL_STATSPLINE", PRUNE::Message.normalize("220"))
    assert_equal("RPL_UMODEIS", PRUNE::Message.normalize("221"))
    assert_equal("RPL_STATSDLINE", PRUNE::Message.normalize("225"))
    assert_equal("RPL_OPTION", PRUNE::Message.normalize("227"))
    assert_equal("RPL_ENDOPTIONS", PRUNE::Message.normalize("228"))
    assert_equal("RPL_SERVICEINFO", PRUNE::Message.normalize("231"))
    assert_equal("RPL_ENDOFSERVICES", PRUNE::Message.normalize("232"))
    assert_equal("RPL_SERVICE", PRUNE::Message.normalize("233"))
    assert_equal("RPL_SERVLIST", PRUNE::Message.normalize("234"))
    assert_equal("RPL_SERVLISTEND", PRUNE::Message.normalize("235"))
    assert_equal("RPL_STATSVLINE", PRUNE::Message.normalize("240"))
    assert_equal("RPL_STATSLLINE", PRUNE::Message.normalize("241"))
    assert_equal("RPL_STATSUPTIME", PRUNE::Message.normalize("242"))
    assert_equal("RPL_STATSOLINE", PRUNE::Message.normalize("243"))
    assert_equal("RPL_STATSHLINE", PRUNE::Message.normalize("244"))
    assert_equal("245", PRUNE::Message.normalize("245"))
    assert_equal("RPL_STATSPING", PRUNE::Message.normalize("246"))
    assert_equal("RPL_STATSXLINE", PRUNE::Message.normalize("247"))
    assert_equal("RPL_STATSULINE", PRUNE::Message.normalize("248"))
    assert_equal("249", PRUNE::Message.normalize("249"))
    assert_equal("RPL_STATSCONN", PRUNE::Message.normalize("250"))
    assert_equal("RPL_LUSERCLIENT", PRUNE::Message.normalize("251"))
    assert_equal("RPL_LUSEROP", PRUNE::Message.normalize("252"))
    assert_equal("RPL_LUSERUNKNOWN", PRUNE::Message.normalize("253"))
    assert_equal("RPL_LUSERCHANNELS", PRUNE::Message.normalize("254"))
    assert_equal("RPL_LUSERME", PRUNE::Message.normalize("255"))
    assert_equal("RPL_ADMINME", PRUNE::Message.normalize("256"))
    assert_equal("RPL_ADMINLOC1", PRUNE::Message.normalize("257"))
    assert_equal("RPL_ADMINLOC2", PRUNE::Message.normalize("258"))
    assert_equal("RPL_ADMINEMAIL", PRUNE::Message.normalize("259"))
    assert_equal("260", PRUNE::Message.normalize("260"))
    assert_equal("RPL_TRACELOG", PRUNE::Message.normalize("261"))
    assert_equal("RPL_TRACEEND", PRUNE::Message.normalize("262"))
    assert_equal("RPL_TRYAGAIN", PRUNE::Message.normalize("263"))
    assert_equal("264", PRUNE::Message.normalize("264"))
    assert_equal("RPL_LOCALUSERS", PRUNE::Message.normalize("265"))
    assert_equal("RPL_GLOBALUSERS", PRUNE::Message.normalize("266"))
    assert_equal("RPL_MODE", PRUNE::Message.normalize("268"))
    assert_equal("RPL_ENDMODE", PRUNE::Message.normalize("269"))
    assert_equal("RPL_SILELIST", PRUNE::Message.normalize("271"))
    assert_equal("RPL_ENDOFSILELIST", PRUNE::Message.normalize("272"))
    assert_equal("RPL_CLIENTCAPAB", PRUNE::Message.normalize("290"))
    assert_equal("RPL_NONE", PRUNE::Message.normalize("300"))
    assert_equal("RPL_AWAY", PRUNE::Message.normalize("301"))
    assert_equal("RPL_USERHOST", PRUNE::Message.normalize("302"))
    assert_equal("RPL_ISON", PRUNE::Message.normalize("303"))
    assert_equal("RPL_TEXT", PRUNE::Message.normalize("304"))
    assert_equal("RPL_UNAWAY", PRUNE::Message.normalize("305"))
    assert_equal("RPL_NOWAWAY", PRUNE::Message.normalize("306"))
    assert_equal("RPL_WHOISUSER", PRUNE::Message.normalize("311"))
    assert_equal("RPL_WHOISSERVER", PRUNE::Message.normalize("312"))
    assert_equal("RPL_WHOISOPERATOR", PRUNE::Message.normalize("313"))
    assert_equal("RPL_WHOWASUSER", PRUNE::Message.normalize("314"))
    assert_equal("RPL_ENDOFWHO", PRUNE::Message.normalize("315"))
    assert_equal("RPL_WHOISCHANOP", PRUNE::Message.normalize("316"))
    assert_equal("RPL_WHOISIDLE", PRUNE::Message.normalize("317"))
    assert_equal("RPL_ENDOFWHOIS", PRUNE::Message.normalize("318"))
    assert_equal("RPL_WHOISCHANNELS", PRUNE::Message.normalize("319"))
    assert_equal("RPL_WHOISIDENTIFIED", PRUNE::Message.normalize("320"))
    assert_equal("RPL_LISTSTART", PRUNE::Message.normalize("321"))
    assert_equal("RPL_LIST", PRUNE::Message.normalize("322"))
    assert_equal("RPL_LISTEND", PRUNE::Message.normalize("323"))
    assert_equal("RPL_CHANNELMODEIS", PRUNE::Message.normalize("324"))
    assert_equal("RPL_UNIQOPIS", PRUNE::Message.normalize("325"))
    assert_equal("RPL_WHOISOPER_PRIVS", PRUNE::Message.normalize("326"))
    assert_equal("RPL_WHOISREALHOST", PRUNE::Message.normalize("327"))
    assert_equal("RPL_328", PRUNE::Message.normalize("328")) # FIXME
    assert_equal("RPL_CHANNELTIMESTAMP", PRUNE::Message.normalize("329"))
    assert_equal("RPL_WHOISLOGGEDIN", PRUNE::Message.normalize("330"))
    assert_equal("RPL_NOTOPIC", PRUNE::Message.normalize("331"))
    assert_equal("RPL_TOPIC", PRUNE::Message.normalize("332"))
    assert_equal("RPL_TOPICTIMESTAMP", PRUNE::Message.normalize("333"))
    assert_equal("RPL_INVITING", PRUNE::Message.normalize("341"))
    assert_equal("RPL_SUMMONING", PRUNE::Message.normalize("342"))
    assert_equal("RPL_INVITELIST", PRUNE::Message.normalize("346"))
    assert_equal("RPL_ENDOFINVITELIST", PRUNE::Message.normalize("347"))
    assert_equal("RPL_EXCEPTLIST", PRUNE::Message.normalize("348"))
    assert_equal("RPL_ENDOFEXCEPTLIST", PRUNE::Message.normalize("349"))
    assert_equal("350", PRUNE::Message.normalize("350"))
    assert_equal("RPL_VERSION", PRUNE::Message.normalize("351"))
    assert_equal("RPL_WHOREPLY", PRUNE::Message.normalize("352"))
    assert_equal("RPL_NAMREPLY", PRUNE::Message.normalize("353"))
    assert_equal("RPL_KILLDONE", PRUNE::Message.normalize("361"))
    assert_equal("RPL_CLOSING", PRUNE::Message.normalize("362"))
    assert_equal("RPL_CLOSEEND", PRUNE::Message.normalize("363"))
    assert_equal("RPL_LINKS", PRUNE::Message.normalize("364"))
    assert_equal("RPL_ENDOFLINKS", PRUNE::Message.normalize("365"))
    assert_equal("RPL_ENDOFNAMES", PRUNE::Message.normalize("366"))
    assert_equal("RPL_BANLIST", PRUNE::Message.normalize("367"))
    assert_equal("RPL_ENDOFBANLIST", PRUNE::Message.normalize("368"))
    assert_equal("RPL_ENDOFWHOWAS", PRUNE::Message.normalize("369"))
    assert_equal("370", PRUNE::Message.normalize("370"))
    assert_equal("RPL_INFO", PRUNE::Message.normalize("371"))
    assert_equal("RPL_MOTD", PRUNE::Message.normalize("372"))
    assert_equal("RPL_INFOSTART", PRUNE::Message.normalize("373"))
    assert_equal("RPL_ENDOFINFO", PRUNE::Message.normalize("374"))
    assert_equal("RPL_MOTDSTART", PRUNE::Message.normalize("375"))
    assert_equal("RPL_ENDOFMOTD", PRUNE::Message.normalize("376"))
    assert_equal("RPL_MAP", PRUNE::Message.normalize("377"))
    assert_equal("RPL_ENDOFMAP", PRUNE::Message.normalize("378"))
    assert_equal("RPL_FORWARD", PRUNE::Message.normalize("379"))
    assert_equal("RPL_YOUREOPER", PRUNE::Message.normalize("381"))
    assert_equal("RPL_REHASHING", PRUNE::Message.normalize("382"))
    assert_equal("RPL_YOURESERVICE", PRUNE::Message.normalize("383"))
    assert_equal("RPL_MYPORTIS", PRUNE::Message.normalize("384"))
    assert_equal("RPL_NOTOPERANYMORE", PRUNE::Message.normalize("385"))
    assert_equal("RPL_TIME", PRUNE::Message.normalize("391"))
    assert_equal("RPL_USERSSTART", PRUNE::Message.normalize("392"))
    assert_equal("RPL_USERS", PRUNE::Message.normalize("393"))
    assert_equal("RPL_ENDOFUSERS", PRUNE::Message.normalize("394"))
    assert_equal("RPL_NOUSERS", PRUNE::Message.normalize("395"))
    assert_equal("RPL_MESSAGE", PRUNE::Message.normalize("399"))
    assert_equal("RPL_CMDACT", PRUNE::Message.normalize("-1"))
    assert_equal("ERR_NOSUCHNICK", PRUNE::Message.normalize("401"))
    assert_equal("ERR_NOSUCHSERVER", PRUNE::Message.normalize("402"))
    assert_equal("ERR_NOSUCHCHANNEL", PRUNE::Message.normalize("403"))
    assert_equal("ERR_CANNOTSENDTOCHAN", PRUNE::Message.normalize("404"))
    assert_equal("ERR_TOOMANYCHANNELS", PRUNE::Message.normalize("405"))
    assert_equal("ERR_WASNOSUCHNICK", PRUNE::Message.normalize("406"))
    assert_equal("ERR_TOOMANYTARGETS", PRUNE::Message.normalize("407"))
    assert_equal("ERR_NOSUCHSERVICE", PRUNE::Message.normalize("408"))
    assert_equal("ERR_NOORIGIN", PRUNE::Message.normalize("409"))
    assert_equal("ERR_SERVICES_OFFLINE", PRUNE::Message.normalize("410"))
    assert_equal("ERR_NORECIPIENT", PRUNE::Message.normalize("411"))
    assert_equal("ERR_NOTEXTTOSEND", PRUNE::Message.normalize("412"))
    assert_equal("ERR_NOTOPLEVEL", PRUNE::Message.normalize("413"))
    assert_equal("ERR_WILDTOPLEVEL", PRUNE::Message.normalize("414"))
    assert_equal("ERR_BADMASK", PRUNE::Message.normalize("415"))
    assert_equal("ERR_UNKNOWNCOMMAND", PRUNE::Message.normalize("421"))
    assert_equal("ERR_NOMOTD", PRUNE::Message.normalize("422"))
    assert_equal("ERR_NOADMININFO", PRUNE::Message.normalize("423"))
    assert_equal("ERR_FILEERROR", PRUNE::Message.normalize("424"))
    assert_equal("ERR_NONICKNAMEGIVEN", PRUNE::Message.normalize("431"))
    assert_equal("ERR_ERRONEUSNICKNAME", PRUNE::Message.normalize("432"))
    assert_equal("ERR_NICKNAMEINUSE", PRUNE::Message.normalize("433"))
    assert_equal("434", PRUNE::Message.normalize("434"))
    assert_equal("435", PRUNE::Message.normalize("435"))
    assert_equal("ERR_NICKCOLLISION", PRUNE::Message.normalize("436"))
    assert_equal("ERR_UNAVAILRESOURCE", PRUNE::Message.normalize("437"))
    assert_equal("ERR_BANNICKCHANGE", PRUNE::Message.normalize("438"))
    assert_equal("ERR_USERNOTINCHANNEL", PRUNE::Message.normalize("441"))
    assert_equal("ERR_NOTONCHANNEL", PRUNE::Message.normalize("442"))
    assert_equal("ERR_USERONCHANNEL", PRUNE::Message.normalize("443"))
    assert_equal("ERR_NOLOGIN", PRUNE::Message.normalize("444"))
    assert_equal("ERR_SUMMONDISABLED", PRUNE::Message.normalize("445"))
    assert_equal("ERR_USERSDISABLED", PRUNE::Message.normalize("446"))
    assert_equal("ERR_TARGETNINVITE", PRUNE::Message.normalize("447"))
    assert_equal("ERR_SOURCENINVITE", PRUNE::Message.normalize("448"))
    assert_equal("ERR_NOTREGISTERED", PRUNE::Message.normalize("451"))
    assert_equal("ERR_NEEDMOREPARAMS", PRUNE::Message.normalize("461"))
    assert_equal("ERR_ALREADYREGISTRED", PRUNE::Message.normalize("462"))
    assert_equal("ERR_NOPERMFORHOST", PRUNE::Message.normalize("463"))
    assert_equal("ERR_PASSWDMISMATCH", PRUNE::Message.normalize("464"))
    assert_equal("ERR_YOUREBANNEDCREEP", PRUNE::Message.normalize("465"))
    assert_equal("ERR_YOUWILLBEBANNED", PRUNE::Message.normalize("466"))
    assert_equal("ERR_KEYSE", PRUNE::Message.normalize("467"))
    assert_equal("ERR_INVALIDUSERNAME", PRUNE::Message.normalize("468"))
    assert_equal("ERR_LINKCHANNEL", PRUNE::Message.normalize("470"))
    assert_equal("ERR_CHANNELISFULL", PRUNE::Message.normalize("471"))
    assert_equal("ERR_UNKNOWNMODE", PRUNE::Message.normalize("472"))
    assert_equal("ERR_INVITEONLYCHAN", PRUNE::Message.normalize("473"))
    assert_equal("ERR_BANNEDFROMCHAN", PRUNE::Message.normalize("474"))
    assert_equal("ERR_BADCHANNELKEY", PRUNE::Message.normalize("475"))
    assert_equal("ERR_BADCHANMASK", PRUNE::Message.normalize("476"))
    assert_equal("ERR_NOCHANMODES", PRUNE::Message.normalize("477"))
    assert_equal("ERR_BANLISTFULL", PRUNE::Message.normalize("478"))
    assert_equal("ERR_BADCHANNAME", PRUNE::Message.normalize("479"))
    assert_equal("ERR_THROTTLED", PRUNE::Message.normalize("480"))
    assert_equal("ERR_NOPRIVILEGES", PRUNE::Message.normalize("481"))
    assert_equal("ERR_CHANOPRIVSNEEDED", PRUNE::Message.normalize("482"))
    assert_equal("ERR_CANTKILLSERVER", PRUNE::Message.normalize("483"))
    assert_equal("ERR_RESTRICTED", PRUNE::Message.normalize("484"))
    assert_equal("ERR_UNIQOPPRIVSNEEDED", PRUNE::Message.normalize("485"))
    assert_equal("ERR_RESTRICTED", PRUNE::Message.normalize("486"))
    assert_equal("ERR_NO_OP_SPLIT", PRUNE::Message.normalize("487"))
    assert_equal("ERR_NEED_UMODE", PRUNE::Message.normalize("488"))
    assert_equal("ERR_NOOPERHOST", PRUNE::Message.normalize("491"))
    assert_equal("ERR_NOSERVICEHOST", PRUNE::Message.normalize("492"))
    assert_equal("ERR_UMODEUNKNOWNFLAG", PRUNE::Message.normalize("501"))
    assert_equal("ERR_USERSDONTMATCH", PRUNE::Message.normalize("502"))
    assert_equal("ERR_GHOSTEDCLIENT", PRUNE::Message.normalize("503"))
    assert_equal("ERR_BLOCKING_NOTID", PRUNE::Message.normalize("505"))
    assert_equal("ERR_QUIETUNIDENT", PRUNE::Message.normalize("506"))
    assert_equal("ERR_SILELISTFULL", PRUNE::Message.normalize("511"))
    assert_equal("ERR_MAXMAPNODES", PRUNE::Message.normalize("512"))
    assert_equal("ERR_MAXFORWARDING", PRUNE::Message.normalize("513"))
    assert_equal("ERR_NOFORWARDING", PRUNE::Message.normalize("514"))
    assert_equal("ERR_NOUNIDENTIFIED", PRUNE::Message.normalize("515"))
    assert_equal("RPL_LOGGEDIN", PRUNE::Message.normalize("901"))
    assert_equal("RPL_LOGGEDOUT", PRUNE::Message.normalize("902"))
    assert_equal(1, PRUNE::Message.normalize(1))
  end # def test_normalize

  def test_sanitize
    assert_equal(:FOO, PRUNE::Message.sanitize("foo"))
    assert_equal(:FOO, PRUNE::Message.sanitize(:foo))
    assert_equal(:RPL_WELCOME, PRUNE::Message.sanitize("001"))
    assert_equal(:RPL_YOURHOST, PRUNE::Message.sanitize("002"))
    assert_equal(:RPL_CREATED, PRUNE::Message.sanitize("003"))
    assert_equal(:RPL_MYINFO, PRUNE::Message.sanitize("004"))
    assert_equal(:RPL_BOUNCE, PRUNE::Message.sanitize("005"))
    assert_equal(:RPL_TRACELINK, PRUNE::Message.sanitize("200"))
    assert_equal(:RPL_TRACECONNECTING, PRUNE::Message.sanitize("201"))
    assert_equal(:RPL_TRACEHANDSHAKE, PRUNE::Message.sanitize("202"))
    assert_equal(:RPL_TRACEUNKNOWN, PRUNE::Message.sanitize("203"))
    assert_equal(:RPL_TRACEOPERATOR, PRUNE::Message.sanitize("204"))
    assert_equal(:RPL_TRACEUSER, PRUNE::Message.sanitize("205"))
    assert_equal(:RPL_TRACESERVER, PRUNE::Message.sanitize("206"))
    assert_equal(:RPL_TRACESERVICE, PRUNE::Message.sanitize("207"))
    assert_equal(:RPL_TRACENEWTYPE, PRUNE::Message.sanitize("208"))
    assert_equal(:RPL_TRACECLASS, PRUNE::Message.sanitize("209"))
    assert_equal(:RPL_TRACERECONNECT, PRUNE::Message.sanitize("210"))
    assert_equal(:RPL_STATSLINKINFO, PRUNE::Message.sanitize("211"))
    assert_equal(:RPL_STATSCOMMANDS, PRUNE::Message.sanitize("212"))
    assert_equal(:RPL_STATSCLINE, PRUNE::Message.sanitize("213"))
    assert_equal(:RPL_STATSNLINE, PRUNE::Message.sanitize("214"))
    assert_equal(:RPL_STATSILINE, PRUNE::Message.sanitize("215"))
    assert_equal(:RPL_STATSKLINE, PRUNE::Message.sanitize("216"))
    assert_equal(:RPL_STATSQLINE, PRUNE::Message.sanitize("217"))
    assert_equal(:RPL_STATSYLINE, PRUNE::Message.sanitize("218"))
    assert_equal(:RPL_ENDOFSTATS, PRUNE::Message.sanitize("219"))
    assert_equal(:RPL_STATSPLINE, PRUNE::Message.sanitize("220"))
    assert_equal(:RPL_UMODEIS, PRUNE::Message.sanitize("221"))
    assert_equal(:RPL_STATSDLINE, PRUNE::Message.sanitize("225"))
    assert_equal(:RPL_OPTION, PRUNE::Message.sanitize("227"))
    assert_equal(:RPL_ENDOPTIONS, PRUNE::Message.sanitize("228"))
    assert_equal(:RPL_SERVICEINFO, PRUNE::Message.sanitize("231"))
    assert_equal(:RPL_ENDOFSERVICES, PRUNE::Message.sanitize("232"))
    assert_equal(:RPL_SERVICE, PRUNE::Message.sanitize("233"))
    assert_equal(:RPL_SERVLIST, PRUNE::Message.sanitize("234"))
    assert_equal(:RPL_SERVLISTEND, PRUNE::Message.sanitize("235"))
    assert_equal(:RPL_STATSVLINE, PRUNE::Message.sanitize("240"))
    assert_equal(:RPL_STATSLLINE, PRUNE::Message.sanitize("241"))
    assert_equal(:RPL_STATSUPTIME, PRUNE::Message.sanitize("242"))
    assert_equal(:RPL_STATSOLINE, PRUNE::Message.sanitize("243"))
    assert_equal(:RPL_STATSHLINE, PRUNE::Message.sanitize("244"))
    assert_equal(:"245", PRUNE::Message.sanitize("245"))
    assert_equal(:RPL_STATSPING, PRUNE::Message.sanitize("246"))
    assert_equal(:RPL_STATSXLINE, PRUNE::Message.sanitize("247"))
    assert_equal(:RPL_STATSULINE, PRUNE::Message.sanitize("248"))
    assert_equal(:"249", PRUNE::Message.sanitize("249"))
    assert_equal(:RPL_STATSCONN, PRUNE::Message.sanitize("250"))
    assert_equal(:RPL_LUSERCLIENT, PRUNE::Message.sanitize("251"))
    assert_equal(:RPL_LUSEROP, PRUNE::Message.sanitize("252"))
    assert_equal(:RPL_LUSERUNKNOWN, PRUNE::Message.sanitize("253"))
    assert_equal(:RPL_LUSERCHANNELS, PRUNE::Message.sanitize("254"))
    assert_equal(:RPL_LUSERME, PRUNE::Message.sanitize("255"))
    assert_equal(:RPL_ADMINME, PRUNE::Message.sanitize("256"))
    assert_equal(:RPL_ADMINLOC1, PRUNE::Message.sanitize("257"))
    assert_equal(:RPL_ADMINLOC2, PRUNE::Message.sanitize("258"))
    assert_equal(:RPL_ADMINEMAIL, PRUNE::Message.sanitize("259"))
    assert_equal(:"260", PRUNE::Message.sanitize("260"))
    assert_equal(:RPL_TRACELOG, PRUNE::Message.sanitize("261"))
    assert_equal(:RPL_TRACEEND, PRUNE::Message.sanitize("262"))
    assert_equal(:RPL_TRYAGAIN, PRUNE::Message.sanitize("263"))
    assert_equal(:"264", PRUNE::Message.sanitize("264"))
    assert_equal(:RPL_LOCALUSERS, PRUNE::Message.sanitize("265"))
    assert_equal(:RPL_GLOBALUSERS, PRUNE::Message.sanitize("266"))
    assert_equal(:RPL_NONE, PRUNE::Message.sanitize("300"))
    assert_equal(:RPL_AWAY, PRUNE::Message.sanitize("301"))
    assert_equal(:RPL_USERHOST, PRUNE::Message.sanitize("302"))
    assert_equal(:RPL_ISON, PRUNE::Message.sanitize("303"))
    assert_equal(:RPL_TEXT, PRUNE::Message.sanitize("304"))
    assert_equal(:RPL_UNAWAY, PRUNE::Message.sanitize("305"))
    assert_equal(:RPL_NOWAWAY, PRUNE::Message.sanitize("306"))
    assert_equal(:RPL_WHOISUSER, PRUNE::Message.sanitize("311"))
    assert_equal(:RPL_WHOISSERVER, PRUNE::Message.sanitize("312"))
    assert_equal(:RPL_WHOISOPERATOR, PRUNE::Message.sanitize("313"))
    assert_equal(:RPL_WHOWASUSER, PRUNE::Message.sanitize("314"))
    assert_equal(:RPL_ENDOFWHO, PRUNE::Message.sanitize("315"))
    assert_equal(:RPL_WHOISCHANOP, PRUNE::Message.sanitize("316"))
    assert_equal(:RPL_WHOISIDLE, PRUNE::Message.sanitize("317"))
    assert_equal(:RPL_ENDOFWHOIS, PRUNE::Message.sanitize("318"))
    assert_equal(:RPL_WHOISCHANNELS, PRUNE::Message.sanitize("319"))
    assert_equal(:RPL_WHOISIDENTIFIED, PRUNE::Message.sanitize("320"))
    assert_equal(:RPL_LISTSTART, PRUNE::Message.sanitize("321"))
    assert_equal(:RPL_LIST, PRUNE::Message.sanitize("322"))
    assert_equal(:RPL_LISTEND, PRUNE::Message.sanitize("323"))
    assert_equal(:RPL_CHANNELMODEIS, PRUNE::Message.sanitize("324"))
    assert_equal(:RPL_UNIQOPIS, PRUNE::Message.sanitize("325"))
    assert_equal(:RPL_CHANNELTIMESTAMP, PRUNE::Message.sanitize("329"))
    assert_equal(:RPL_WHOISLOGGEDIN, PRUNE::Message.sanitize("330"))
    assert_equal(:RPL_NOTOPIC, PRUNE::Message.sanitize("331"))
    assert_equal(:RPL_TOPIC, PRUNE::Message.sanitize("332"))
    assert_equal(:RPL_TOPICTIMESTAMP, PRUNE::Message.sanitize("333"))
    assert_equal(:RPL_INVITING, PRUNE::Message.sanitize("341"))
    assert_equal(:RPL_SUMMONING, PRUNE::Message.sanitize("342"))
    assert_equal(:RPL_INVITELIST, PRUNE::Message.sanitize("346"))
    assert_equal(:RPL_ENDOFINVITELIST, PRUNE::Message.sanitize("347"))
    assert_equal(:RPL_EXCEPTLIST, PRUNE::Message.sanitize("348"))
    assert_equal(:RPL_ENDOFEXCEPTLIST, PRUNE::Message.sanitize("349"))
    assert_equal(:"350", PRUNE::Message.sanitize("350"))
    assert_equal(:RPL_VERSION, PRUNE::Message.sanitize("351"))
    assert_equal(:RPL_WHOREPLY, PRUNE::Message.sanitize("352"))
    assert_equal(:RPL_NAMREPLY, PRUNE::Message.sanitize("353"))
    assert_equal(:RPL_KILLDONE, PRUNE::Message.sanitize("361"))
    assert_equal(:RPL_CLOSING, PRUNE::Message.sanitize("362"))
    assert_equal(:RPL_CLOSEEND, PRUNE::Message.sanitize("363"))
    assert_equal(:RPL_LINKS, PRUNE::Message.sanitize("364"))
    assert_equal(:RPL_ENDOFLINKS, PRUNE::Message.sanitize("365"))
    assert_equal(:RPL_ENDOFNAMES, PRUNE::Message.sanitize("366"))
    assert_equal(:RPL_BANLIST, PRUNE::Message.sanitize("367"))
    assert_equal(:RPL_ENDOFBANLIST, PRUNE::Message.sanitize("368"))
    assert_equal(:RPL_ENDOFWHOWAS, PRUNE::Message.sanitize("369"))
    assert_equal(:"370", PRUNE::Message.sanitize("370"))
    assert_equal(:RPL_INFO, PRUNE::Message.sanitize("371"))
    assert_equal(:RPL_MOTD, PRUNE::Message.sanitize("372"))
    assert_equal(:RPL_INFOSTART, PRUNE::Message.sanitize("373"))
    assert_equal(:RPL_ENDOFINFO, PRUNE::Message.sanitize("374"))
    assert_equal(:RPL_MOTDSTART, PRUNE::Message.sanitize("375"))
    assert_equal(:RPL_ENDOFMOTD, PRUNE::Message.sanitize("376"))
    assert_equal(:RPL_YOUREOPER, PRUNE::Message.sanitize("381"))
    assert_equal(:RPL_REHASHING, PRUNE::Message.sanitize("382"))
    assert_equal(:RPL_YOURESERVICE, PRUNE::Message.sanitize("383"))
    assert_equal(:RPL_MYPORTIS, PRUNE::Message.sanitize("384"))
    assert_equal(:RPL_TIME, PRUNE::Message.sanitize("391"))
    assert_equal(:RPL_USERSSTART, PRUNE::Message.sanitize("392"))
    assert_equal(:RPL_USERS, PRUNE::Message.sanitize("393"))
    assert_equal(:RPL_ENDOFUSERS, PRUNE::Message.sanitize("394"))
    assert_equal(:RPL_NOUSERS, PRUNE::Message.sanitize("395"))
    assert_equal(:RPL_CMDACT, PRUNE::Message.sanitize("-1"))
    assert_equal(:ERR_NOSUCHNICK, PRUNE::Message.sanitize("401"))
    assert_equal(:ERR_NOSUCHSERVER, PRUNE::Message.sanitize("402"))
    assert_equal(:ERR_NOSUCHCHANNEL, PRUNE::Message.sanitize("403"))
    assert_equal(:ERR_CANNOTSENDTOCHAN, PRUNE::Message.sanitize("404"))
    assert_equal(:ERR_TOOMANYCHANNELS, PRUNE::Message.sanitize("405"))
    assert_equal(:ERR_WASNOSUCHNICK, PRUNE::Message.sanitize("406"))
    assert_equal(:ERR_TOOMANYTARGETS, PRUNE::Message.sanitize("407"))
    assert_equal(:ERR_NOSUCHSERVICE, PRUNE::Message.sanitize("408"))
    assert_equal(:ERR_NOORIGIN, PRUNE::Message.sanitize("409"))
    assert_equal(:ERR_SERVICES_OFFLINE, PRUNE::Message.sanitize("410"))
    assert_equal(:ERR_NORECIPIENT, PRUNE::Message.sanitize("411"))
    assert_equal(:ERR_NOTEXTTOSEND, PRUNE::Message.sanitize("412"))
    assert_equal(:ERR_NOTOPLEVEL, PRUNE::Message.sanitize("413"))
    assert_equal(:ERR_WILDTOPLEVEL, PRUNE::Message.sanitize("414"))
    assert_equal(:ERR_BADMASK, PRUNE::Message.sanitize("415"))
    assert_equal(:ERR_UNKNOWNCOMMAND, PRUNE::Message.sanitize("421"))
    assert_equal(:ERR_NOMOTD, PRUNE::Message.sanitize("422"))
    assert_equal(:ERR_NOADMININFO, PRUNE::Message.sanitize("423"))
    assert_equal(:ERR_FILEERROR, PRUNE::Message.sanitize("424"))
    assert_equal(:ERR_NONICKNAMEGIVEN, PRUNE::Message.sanitize("431"))
    assert_equal(:ERR_ERRONEUSNICKNAME, PRUNE::Message.sanitize("432"))
    assert_equal(:ERR_NICKNAMEINUSE, PRUNE::Message.sanitize("433"))
    assert_equal(:"434", PRUNE::Message.sanitize("434"))
    assert_equal(:"435", PRUNE::Message.sanitize("435"))
    assert_equal(:ERR_NICKCOLLISION, PRUNE::Message.sanitize("436"))
    assert_equal(:ERR_UNAVAILRESOURCE, PRUNE::Message.sanitize("437"))
    assert_equal(:ERR_USERNOTINCHANNEL, PRUNE::Message.sanitize("441"))
    assert_equal(:ERR_NOTONCHANNEL, PRUNE::Message.sanitize("442"))
    assert_equal(:ERR_USERONCHANNEL, PRUNE::Message.sanitize("443"))
    assert_equal(:ERR_NOLOGIN, PRUNE::Message.sanitize("444"))
    assert_equal(:ERR_SUMMONDISABLED, PRUNE::Message.sanitize("445"))
    assert_equal(:ERR_USERSDISABLED, PRUNE::Message.sanitize("446"))
    assert_equal(:ERR_NOTREGISTERED, PRUNE::Message.sanitize("451"))
    assert_equal(:ERR_NEEDMOREPARAMS, PRUNE::Message.sanitize("461"))
    assert_equal(:ERR_ALREADYREGISTRED, PRUNE::Message.sanitize("462"))
    assert_equal(:ERR_NOPERMFORHOST, PRUNE::Message.sanitize("463"))
    assert_equal(:ERR_PASSWDMISMATCH, PRUNE::Message.sanitize("464"))
    assert_equal(:ERR_YOUREBANNEDCREEP, PRUNE::Message.sanitize("465"))
    assert_equal(:ERR_YOUWILLBEBANNED, PRUNE::Message.sanitize("466"))
    assert_equal(:ERR_KEYSE, PRUNE::Message.sanitize("467"))
    assert_equal(:ERR_INVALIDUSERNAME, PRUNE::Message.sanitize("468"))
    assert_equal(:ERR_CHANNELISFULL, PRUNE::Message.sanitize("471"))
    assert_equal(:ERR_UNKNOWNMODE, PRUNE::Message.sanitize("472"))
    assert_equal(:ERR_INVITEONLYCHAN, PRUNE::Message.sanitize("473"))
    assert_equal(:ERR_BANNEDFROMCHAN, PRUNE::Message.sanitize("474"))
    assert_equal(:ERR_BADCHANNELKEY, PRUNE::Message.sanitize("475"))
    assert_equal(:ERR_BADCHANMASK, PRUNE::Message.sanitize("476"))
    assert_equal(:ERR_NOCHANMODES, PRUNE::Message.sanitize("477"))
    assert_equal(:ERR_BANLISTFULL, PRUNE::Message.sanitize("478"))
    assert_equal(:ERR_BADCHANNAME, PRUNE::Message.sanitize("479"))
    assert_equal(:ERR_THROTTLED, PRUNE::Message.sanitize("480"))
    assert_equal(:ERR_NOPRIVILEGES, PRUNE::Message.sanitize("481"))
    assert_equal(:ERR_CHANOPRIVSNEEDED, PRUNE::Message.sanitize("482"))
    assert_equal(:ERR_CANTKILLSERVER, PRUNE::Message.sanitize("483"))
    assert_equal(:ERR_RESTRICTED, PRUNE::Message.sanitize("484"))
    assert_equal(:ERR_UNIQOPPRIVSNEEDED, PRUNE::Message.sanitize("485"))
    assert_equal(:ERR_NOOPERHOST, PRUNE::Message.sanitize("491"))
    assert_equal(:ERR_NOSERVICEHOST, PRUNE::Message.sanitize("492"))
    assert_equal(:ERR_UMODEUNKNOWNFLAG, PRUNE::Message.sanitize("501"))
    assert_equal(:ERR_USERSDONTMATCH, PRUNE::Message.sanitize("502"))
    assert_equal(nil, PRUNE::Message.sanitize(1))
  end # def test_sanitize

  def test_channelinfo
    assert_equal(nil, PRUNE::Message.channelinfo("foo"))
    assert_equal(nil, PRUNE::Message.channelinfo("connect"))
    assert_equal(nil, PRUNE::Message.channelinfo("Connect"))
    assert_equal(nil, PRUNE::Message.channelinfo(:connect))
    assert_equal(nil, PRUNE::Message.channelinfo(:die))
    assert_equal(nil, PRUNE::Message.channelinfo(:error))
    assert_equal(nil, PRUNE::Message.channelinfo(:ping))
    assert_equal(nil, PRUNE::Message.channelinfo(:pong))
    assert_equal(nil, PRUNE::Message.channelinfo(:rehash))
    assert_equal(nil, PRUNE::Message.channelinfo(:restart))
    assert_equal(nil, PRUNE::Message.channelinfo(:service))
    assert_equal(nil, PRUNE::Message.channelinfo(:squit))
    assert_equal(nil, PRUNE::Message.channelinfo(:trace))
    assert_equal(nil, PRUNE::Message.channelinfo(:wallops))
    assert_equal(nil, PRUNE::Message.channelinfo(:admin))
    assert_equal(nil, PRUNE::Message.channelinfo(:away))
    assert_equal(nil, PRUNE::Message.channelinfo(:info))
    assert_equal(nil, PRUNE::Message.channelinfo(:links))
    assert_equal(nil, PRUNE::Message.channelinfo(:list))
    assert_equal(nil, PRUNE::Message.channelinfo(:lusers))
    assert_equal(nil, PRUNE::Message.channelinfo(:motd))
    assert_equal(nil, PRUNE::Message.channelinfo(:oper))
    assert_equal(nil, PRUNE::Message.channelinfo(:pass))
    assert_equal(nil, PRUNE::Message.channelinfo(:quit))
    assert_equal(nil, PRUNE::Message.channelinfo(:servlist))
    assert_equal(nil, PRUNE::Message.channelinfo(:squery))
    assert_equal(nil, PRUNE::Message.channelinfo(:stats))
    assert_equal(nil, PRUNE::Message.channelinfo(:time))
    assert_equal(nil, PRUNE::Message.channelinfo(:user))
    assert_equal(nil, PRUNE::Message.channelinfo(:users))
    assert_equal(nil, PRUNE::Message.channelinfo(:version))
    assert_not_nil(PRUNE::Message.channelinfo(:invite))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:invite).location)
    assert_equal(1, PRUNE::Message.channelinfo(:invite).index)
    assert_equal(nil, PRUNE::Message.channelinfo(:ison))
    assert_not_nil(PRUNE::Message.channelinfo(:join))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:join).location)
    assert_equal(0, PRUNE::Message.channelinfo(:join).index)
    assert_not_nil(PRUNE::Message.channelinfo(:kick))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:kick).location)
    assert_equal(0, PRUNE::Message.channelinfo(:kick).index)
    assert_equal(nil, PRUNE::Message.channelinfo(:kill))
    assert_not_nil(PRUNE::Message.channelinfo(:mode))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:mode).location)
    assert_equal(0, PRUNE::Message.channelinfo(:mode).index)
    assert_not_nil(PRUNE::Message.channelinfo(:names))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:names).location)
    assert_equal(0, PRUNE::Message.channelinfo(:names).index)
    assert_equal(nil, PRUNE::Message.channelinfo(:nick))
    assert_not_nil(PRUNE::Message.channelinfo(:notice))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:notice).location)
    assert_equal(0, PRUNE::Message.channelinfo(:notice).index)
    assert_not_nil(PRUNE::Message.channelinfo(:part))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:part).location)
    assert_equal(0, PRUNE::Message.channelinfo(:part).index)
    assert_not_nil(PRUNE::Message.channelinfo(:privmsg))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:privmsg).location)
    assert_equal(0, PRUNE::Message.channelinfo(:privmsg).index)
    assert_equal(nil, PRUNE::Message.channelinfo(:summon))
    assert_not_nil(PRUNE::Message.channelinfo(:topic))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:topic).location)
    assert_equal(0, PRUNE::Message.channelinfo(:topic).index)
    assert_equal(nil, PRUNE::Message.channelinfo(:userhost))
    assert_not_nil(PRUNE::Message.channelinfo(:who))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo(:who).location)
    assert_equal(0, PRUNE::Message.channelinfo(:who).index)
    assert_equal(nil, PRUNE::Message.channelinfo(:whois))
    assert_equal(nil, PRUNE::Message.channelinfo(:whowas))
    assert_equal(nil, PRUNE::Message.channelinfo("foo"))
    assert_equal(nil, PRUNE::Message.channelinfo(:foo))
    assert_equal(nil, PRUNE::Message.channelinfo("001"))
    assert_equal(nil, PRUNE::Message.channelinfo("002"))
    assert_equal(nil, PRUNE::Message.channelinfo("003"))
    assert_equal(nil, PRUNE::Message.channelinfo("004"))
    assert_equal(nil, PRUNE::Message.channelinfo("005"))
    assert_equal(nil, PRUNE::Message.channelinfo("200"))
    assert_equal(nil, PRUNE::Message.channelinfo("201"))
    assert_equal(nil, PRUNE::Message.channelinfo("202"))
    assert_equal(nil, PRUNE::Message.channelinfo("203"))
    assert_equal(nil, PRUNE::Message.channelinfo("204"))
    assert_equal(nil, PRUNE::Message.channelinfo("205"))
    assert_equal(nil, PRUNE::Message.channelinfo("206"))
    assert_equal(nil, PRUNE::Message.channelinfo("207"))
    assert_equal(nil, PRUNE::Message.channelinfo("208"))
    assert_equal(nil, PRUNE::Message.channelinfo("209"))
    assert_equal(nil, PRUNE::Message.channelinfo("210"))
    assert_equal(nil, PRUNE::Message.channelinfo("211"))
    assert_equal(nil, PRUNE::Message.channelinfo("212"))
    assert_equal(nil, PRUNE::Message.channelinfo("213"))
    assert_equal(nil, PRUNE::Message.channelinfo("214"))
    assert_equal(nil, PRUNE::Message.channelinfo("215"))
    assert_equal(nil, PRUNE::Message.channelinfo("216"))
    assert_equal(nil, PRUNE::Message.channelinfo("217"))
    assert_equal(nil, PRUNE::Message.channelinfo("218"))
    assert_equal(nil, PRUNE::Message.channelinfo("219"))
    assert_equal(nil, PRUNE::Message.channelinfo("220"))
    assert_equal(nil, PRUNE::Message.channelinfo("221"))
    assert_equal(nil, PRUNE::Message.channelinfo("231"))
    assert_equal(nil, PRUNE::Message.channelinfo("232"))
    assert_equal(nil, PRUNE::Message.channelinfo("233"))
    assert_equal(nil, PRUNE::Message.channelinfo("234"))
    assert_equal(nil, PRUNE::Message.channelinfo("235"))
    assert_equal(nil, PRUNE::Message.channelinfo("240"))
    assert_equal(nil, PRUNE::Message.channelinfo("241"))
    assert_equal(nil, PRUNE::Message.channelinfo("242"))
    assert_equal(nil, PRUNE::Message.channelinfo("243"))
    assert_equal(nil, PRUNE::Message.channelinfo("244"))
    assert_equal(nil, PRUNE::Message.channelinfo("245"))
    assert_equal(nil, PRUNE::Message.channelinfo("246"))
    assert_equal(nil, PRUNE::Message.channelinfo("247"))
    assert_equal(nil, PRUNE::Message.channelinfo("248"))
    assert_equal(nil, PRUNE::Message.channelinfo("249"))
    assert_equal(nil, PRUNE::Message.channelinfo("250"))
    assert_equal(nil, PRUNE::Message.channelinfo("251"))
    assert_equal(nil, PRUNE::Message.channelinfo("252"))
    assert_equal(nil, PRUNE::Message.channelinfo("253"))
    assert_equal(nil, PRUNE::Message.channelinfo("254"))
    assert_equal(nil, PRUNE::Message.channelinfo("255"))
    assert_equal(nil, PRUNE::Message.channelinfo("256"))
    assert_equal(nil, PRUNE::Message.channelinfo("257"))
    assert_equal(nil, PRUNE::Message.channelinfo("258"))
    assert_equal(nil, PRUNE::Message.channelinfo("259"))
    assert_equal(nil, PRUNE::Message.channelinfo("260"))
    assert_equal(nil, PRUNE::Message.channelinfo("261"))
    assert_equal(nil, PRUNE::Message.channelinfo("262"))
    assert_equal(nil, PRUNE::Message.channelinfo("263"))
    assert_equal(nil, PRUNE::Message.channelinfo("264"))
    assert_equal(nil, PRUNE::Message.channelinfo("265"))
    assert_equal(nil, PRUNE::Message.channelinfo("266"))
    assert_equal(nil, PRUNE::Message.channelinfo("300"))
    assert_equal(nil, PRUNE::Message.channelinfo("301"))
    assert_equal(nil, PRUNE::Message.channelinfo("302"))
    assert_equal(nil, PRUNE::Message.channelinfo("303"))
    assert_equal(nil, PRUNE::Message.channelinfo("304"))
    assert_equal(nil, PRUNE::Message.channelinfo("305"))
    assert_equal(nil, PRUNE::Message.channelinfo("306"))
    assert_equal(nil, PRUNE::Message.channelinfo("311"))
    assert_equal(nil, PRUNE::Message.channelinfo("312"))
    assert_equal(nil, PRUNE::Message.channelinfo("313"))
    assert_equal(nil, PRUNE::Message.channelinfo("314"))
    assert_not_nil(PRUNE::Message.channelinfo("315"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("315").location)
    assert_equal(1, PRUNE::Message.channelinfo("315").index)
    assert_equal(nil, PRUNE::Message.channelinfo("316"))
    assert_equal(nil, PRUNE::Message.channelinfo("317"))
    assert_equal(nil, PRUNE::Message.channelinfo("318"))
    assert_equal(nil, PRUNE::Message.channelinfo("319"))
    assert_equal(nil, PRUNE::Message.channelinfo("320"))
    assert_equal(nil, PRUNE::Message.channelinfo("321"))
    assert_not_nil(PRUNE::Message.channelinfo("322"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("322").location)
    assert_equal(1, PRUNE::Message.channelinfo("322").index)
    assert_equal(nil, PRUNE::Message.channelinfo("323"))
    assert_not_nil(PRUNE::Message.channelinfo("324"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("324").location)
    assert_equal(1, PRUNE::Message.channelinfo("324").index)
    assert_equal(nil, PRUNE::Message.channelinfo("325"))
    assert_not_nil(PRUNE::Message.channelinfo("329"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("329").location)
    assert_equal(1, PRUNE::Message.channelinfo("329").index)
    assert_equal(nil, PRUNE::Message.channelinfo("330"))
    assert_equal(nil, PRUNE::Message.channelinfo("331"))
    assert_not_nil(PRUNE::Message.channelinfo("332"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("332").location)
    assert_equal(1, PRUNE::Message.channelinfo("332").index)
    assert_not_nil(PRUNE::Message.channelinfo("333"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("333").location)
    assert_equal(1, PRUNE::Message.channelinfo("333").index)
    assert_not_nil(PRUNE::Message.channelinfo("341"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("341").location)
    assert_equal(2, PRUNE::Message.channelinfo("341").index)
    assert_equal(nil, PRUNE::Message.channelinfo("342"))
    assert_equal(nil, PRUNE::Message.channelinfo("346"))
    assert_equal(nil, PRUNE::Message.channelinfo("347"))
    assert_equal(nil, PRUNE::Message.channelinfo("348"))
    assert_equal(nil, PRUNE::Message.channelinfo("349"))
    assert_equal(nil, PRUNE::Message.channelinfo("350"))
    assert_equal(nil, PRUNE::Message.channelinfo("351"))
    assert_not_nil(PRUNE::Message.channelinfo("352"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("352").location)
    assert_equal(1, PRUNE::Message.channelinfo("352").index)
    assert_not_nil(PRUNE::Message.channelinfo("353"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("353").location)
    assert_equal(2, PRUNE::Message.channelinfo("353").index)
    assert_equal(nil, PRUNE::Message.channelinfo("361"))
    assert_equal(nil, PRUNE::Message.channelinfo("362"))
    assert_equal(nil, PRUNE::Message.channelinfo("363"))
    assert_equal(nil, PRUNE::Message.channelinfo("364"))
    assert_equal(nil, PRUNE::Message.channelinfo("365"))
    assert_not_nil(PRUNE::Message.channelinfo("366"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("366").location)
    assert_equal(1, PRUNE::Message.channelinfo("366").index)
    assert_not_nil(PRUNE::Message.channelinfo("367"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("367").location)
    assert_equal(1, PRUNE::Message.channelinfo("367").index)
    assert_not_nil(PRUNE::Message.channelinfo("368"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("368").location)
    assert_equal(1, PRUNE::Message.channelinfo("368").index)
    assert_equal(nil, PRUNE::Message.channelinfo("369"))
    assert_equal(nil, PRUNE::Message.channelinfo("370"))
    assert_equal(nil, PRUNE::Message.channelinfo("371"))
    assert_equal(nil, PRUNE::Message.channelinfo("372"))
    assert_equal(nil, PRUNE::Message.channelinfo("373"))
    assert_equal(nil, PRUNE::Message.channelinfo("374"))
    assert_equal(nil, PRUNE::Message.channelinfo("375"))
    assert_equal(nil, PRUNE::Message.channelinfo("376"))
    assert_equal(nil, PRUNE::Message.channelinfo("381"))
    assert_equal(nil, PRUNE::Message.channelinfo("382"))
    assert_equal(nil, PRUNE::Message.channelinfo("383"))
    assert_equal(nil, PRUNE::Message.channelinfo("384"))
    assert_equal(nil, PRUNE::Message.channelinfo("391"))
    assert_equal(nil, PRUNE::Message.channelinfo("392"))
    assert_equal(nil, PRUNE::Message.channelinfo("393"))
    assert_equal(nil, PRUNE::Message.channelinfo("394"))
    assert_equal(nil, PRUNE::Message.channelinfo("395"))
    assert_equal(nil, PRUNE::Message.channelinfo("-1"))
    assert_equal(nil, PRUNE::Message.channelinfo("401"))
    assert_equal(nil, PRUNE::Message.channelinfo("402"))
    assert_not_nil(PRUNE::Message.channelinfo("403"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("403").location)
    assert_equal(1, PRUNE::Message.channelinfo("403").index)
    assert_not_nil(PRUNE::Message.channelinfo("404"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("404").location)
    assert_equal(1, PRUNE::Message.channelinfo("404").index)
    assert_not_nil(PRUNE::Message.channelinfo("405"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("405").location)
    assert_equal(1, PRUNE::Message.channelinfo("405").index)
    assert_equal(nil, PRUNE::Message.channelinfo("406"))
    assert_equal(nil, PRUNE::Message.channelinfo("407"))
    assert_equal(nil, PRUNE::Message.channelinfo("408"))
    assert_equal(nil, PRUNE::Message.channelinfo("409"))
    assert_equal(nil, PRUNE::Message.channelinfo("410"))
    assert_equal(nil, PRUNE::Message.channelinfo("411"))
    assert_equal(nil, PRUNE::Message.channelinfo("412"))
    assert_equal(nil, PRUNE::Message.channelinfo("413"))
    assert_equal(nil, PRUNE::Message.channelinfo("414"))
    assert_equal(nil, PRUNE::Message.channelinfo("415"))
    assert_equal(nil, PRUNE::Message.channelinfo("421"))
    assert_equal(nil, PRUNE::Message.channelinfo("422"))
    assert_equal(nil, PRUNE::Message.channelinfo("423"))
    assert_equal(nil, PRUNE::Message.channelinfo("424"))
    assert_equal(nil, PRUNE::Message.channelinfo("431"))
    assert_equal(nil, PRUNE::Message.channelinfo("432"))
    assert_equal(nil, PRUNE::Message.channelinfo("433"))
    assert_equal(nil, PRUNE::Message.channelinfo("434"))
    assert_equal(nil, PRUNE::Message.channelinfo("435"))
    assert_equal(nil, PRUNE::Message.channelinfo("436"))
    assert_not_nil(PRUNE::Message.channelinfo("437"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("437").location)
    assert_equal(1, PRUNE::Message.channelinfo("437").index)
    assert_not_nil(PRUNE::Message.channelinfo("441"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("441").location)
    assert_equal(2, PRUNE::Message.channelinfo("441").index)
    assert_not_nil(PRUNE::Message.channelinfo("442"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("442").location)
    assert_equal(1, PRUNE::Message.channelinfo("442").index)
    assert_not_nil(PRUNE::Message.channelinfo("443"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("443").location)
    assert_equal(2, PRUNE::Message.channelinfo("443").index)
    assert_equal(nil, PRUNE::Message.channelinfo("444"))
    assert_equal(nil, PRUNE::Message.channelinfo("445"))
    assert_equal(nil, PRUNE::Message.channelinfo("446"))
    assert_equal(nil, PRUNE::Message.channelinfo("451"))
    assert_equal(nil, PRUNE::Message.channelinfo("461"))
    assert_equal(nil, PRUNE::Message.channelinfo("462"))
    assert_equal(nil, PRUNE::Message.channelinfo("463"))
    assert_equal(nil, PRUNE::Message.channelinfo("464"))
    assert_equal(nil, PRUNE::Message.channelinfo("465"))
    assert_equal(nil, PRUNE::Message.channelinfo("466"))
    assert_not_nil(PRUNE::Message.channelinfo("467"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("467").location)
    assert_equal(1, PRUNE::Message.channelinfo("467").index)
    assert_equal(nil, PRUNE::Message.channelinfo("468"))
    assert_not_nil(PRUNE::Message.channelinfo("471"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("471").location)
    assert_equal(1, PRUNE::Message.channelinfo("471").index)
    assert_equal(nil, PRUNE::Message.channelinfo("472"))
    assert_not_nil(PRUNE::Message.channelinfo("473"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("473").location)
    assert_equal(1, PRUNE::Message.channelinfo("473").index)
    assert_not_nil(PRUNE::Message.channelinfo("474"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("474").location)
    assert_equal(1, PRUNE::Message.channelinfo("474").index)
    assert_not_nil(PRUNE::Message.channelinfo("475"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("475").location)
    assert_equal(1, PRUNE::Message.channelinfo("475").index)
    assert_not_nil(PRUNE::Message.channelinfo("476"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("476").location)
    assert_equal(1, PRUNE::Message.channelinfo("476").index)
    assert_not_nil(PRUNE::Message.channelinfo("477"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("477").location)
    assert_equal(1, PRUNE::Message.channelinfo("477").index)
    assert_not_nil(PRUNE::Message.channelinfo("478"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("478").location)
    assert_equal(1, PRUNE::Message.channelinfo("478").index)
    assert_equal(nil, PRUNE::Message.channelinfo("479"))
    assert_equal(nil, PRUNE::Message.channelinfo("480"))
    assert_equal(nil, PRUNE::Message.channelinfo("481"))
    assert_not_nil(PRUNE::Message.channelinfo("482"))
    assert_equal(PRUNE::Message::CH_PARAM, PRUNE::Message.channelinfo("482").location)
    assert_equal(1, PRUNE::Message.channelinfo("482").index)
    assert_equal(nil, PRUNE::Message.channelinfo("483"))
    assert_equal(nil, PRUNE::Message.channelinfo("484"))
    assert_equal(nil, PRUNE::Message.channelinfo("485"))
    assert_equal(nil, PRUNE::Message.channelinfo("491"))
    assert_equal(nil, PRUNE::Message.channelinfo("492"))
    assert_equal(nil, PRUNE::Message.channelinfo("501"))
    assert_equal(nil, PRUNE::Message.channelinfo("502"))
    assert_equal(nil, PRUNE::Message.channelinfo(1))
  end # def test_channelinfo

  def test_validate_command_
    assert_equal(false, PRUNE::Message.valid_command?("foo"))
    assert_equal(true, PRUNE::Message.valid_command?("connect"))
    assert_equal(true, PRUNE::Message.valid_command?("Connect"))
    assert_equal(true, PRUNE::Message.valid_command?(:connect))
    assert_equal(true, PRUNE::Message.valid_command?(:die))
    assert_equal(true, PRUNE::Message.valid_command?(:error))
    assert_equal(true, PRUNE::Message.valid_command?(:ping))
    assert_equal(true, PRUNE::Message.valid_command?(:pong))
    assert_equal(true, PRUNE::Message.valid_command?(:rehash))
    assert_equal(true, PRUNE::Message.valid_command?(:restart))
    assert_equal(true, PRUNE::Message.valid_command?(:service))
    assert_equal(true, PRUNE::Message.valid_command?(:squit))
    assert_equal(true, PRUNE::Message.valid_command?(:trace))
    assert_equal(true, PRUNE::Message.valid_command?(:wallops))
    assert_equal(true, PRUNE::Message.valid_command?(:admin))
    assert_equal(true, PRUNE::Message.valid_command?(:away))
    assert_equal(true, PRUNE::Message.valid_command?(:info))
    assert_equal(true, PRUNE::Message.valid_command?(:links))
    assert_equal(true, PRUNE::Message.valid_command?(:list))
    assert_equal(true, PRUNE::Message.valid_command?(:lusers))
    assert_equal(true, PRUNE::Message.valid_command?(:motd))
    assert_equal(true, PRUNE::Message.valid_command?(:oper))
    assert_equal(true, PRUNE::Message.valid_command?(:pass))
    assert_equal(true, PRUNE::Message.valid_command?(:quit))
    assert_equal(true, PRUNE::Message.valid_command?(:servlist))
    assert_equal(true, PRUNE::Message.valid_command?(:squery))
    assert_equal(true, PRUNE::Message.valid_command?(:stats))
    assert_equal(true, PRUNE::Message.valid_command?(:time))
    assert_equal(true, PRUNE::Message.valid_command?(:user))
    assert_equal(true, PRUNE::Message.valid_command?(:users))
    assert_equal(true, PRUNE::Message.valid_command?(:version))
    assert_equal(true, PRUNE::Message.valid_command?(:invite))
    assert_equal(true, PRUNE::Message.valid_command?(:ison))
    assert_equal(true, PRUNE::Message.valid_command?(:join))
    assert_equal(true, PRUNE::Message.valid_command?(:kick))
    assert_equal(true, PRUNE::Message.valid_command?(:kill))
    assert_equal(true, PRUNE::Message.valid_command?(:mode))
    assert_equal(true, PRUNE::Message.valid_command?(:names))
    assert_equal(true, PRUNE::Message.valid_command?(:nick))
    assert_equal(true, PRUNE::Message.valid_command?(:notice))
    assert_equal(true, PRUNE::Message.valid_command?(:part))
    assert_equal(true, PRUNE::Message.valid_command?(:privmsg))
    assert_equal(true, PRUNE::Message.valid_command?(:summon))
    assert_equal(true, PRUNE::Message.valid_command?(:topic))
    assert_equal(true, PRUNE::Message.valid_command?(:userhost))
    assert_equal(true, PRUNE::Message.valid_command?(:who))
    assert_equal(true, PRUNE::Message.valid_command?(:whois))
    assert_equal(true, PRUNE::Message.valid_command?(:whowas))
  end # def test_validate_command_

  def test_min_params
    assert_equal(nil, PRUNE::Message.min_params("foo"))
    assert_equal(2, PRUNE::Message.min_params("connect"))
    assert_equal(2, PRUNE::Message.min_params("Connect"))
    assert_equal(2, PRUNE::Message.min_params(:connect))
    assert_equal(0, PRUNE::Message.min_params(:die))
    assert_equal(1, PRUNE::Message.min_params(:error))
    assert_equal(1, PRUNE::Message.min_params(:ping))
    assert_equal(1, PRUNE::Message.min_params(:pong))
    assert_equal(0, PRUNE::Message.min_params(:rehash))
    assert_equal(0, PRUNE::Message.min_params(:restart))
    assert_equal(2, PRUNE::Message.min_params(:service))
    assert_equal(2, PRUNE::Message.min_params(:squit))
    assert_equal(0, PRUNE::Message.min_params(:trace))
    assert_equal(1, PRUNE::Message.min_params(:wallops))
    assert_equal(0, PRUNE::Message.min_params(:admin))
    assert_equal(0, PRUNE::Message.min_params(:away))
    assert_equal(0, PRUNE::Message.min_params(:info))
    assert_equal(0, PRUNE::Message.min_params(:links))
    assert_equal(0, PRUNE::Message.min_params(:list))
    assert_equal(0, PRUNE::Message.min_params(:lusers))
    assert_equal(0, PRUNE::Message.min_params(:motd))
    assert_equal(2, PRUNE::Message.min_params(:oper))
    assert_equal(1, PRUNE::Message.min_params(:pass))
    assert_equal(0, PRUNE::Message.min_params(:quit))
    assert_equal(0, PRUNE::Message.min_params(:servlist))
    assert_equal(2, PRUNE::Message.min_params(:squery))
    assert_equal(0, PRUNE::Message.min_params(:stats))
    assert_equal(0, PRUNE::Message.min_params(:time))
    assert_equal(4, PRUNE::Message.min_params(:user))
    assert_equal(0, PRUNE::Message.min_params(:users))
    assert_equal(0, PRUNE::Message.min_params(:version))
    assert_equal(2, PRUNE::Message.min_params(:invite))
    assert_equal(1, PRUNE::Message.min_params(:ison))
    assert_equal(1, PRUNE::Message.min_params(:join))
    assert_equal(2, PRUNE::Message.min_params(:kick))
    assert_equal(2, PRUNE::Message.min_params(:kill))
    assert_equal(1, PRUNE::Message.min_params(:mode))
    assert_equal(0, PRUNE::Message.min_params(:names))
    assert_equal(1, PRUNE::Message.min_params(:nick))
    assert_equal(2, PRUNE::Message.min_params(:notice))
    assert_equal(1, PRUNE::Message.min_params(:part))
    assert_equal(2, PRUNE::Message.min_params(:privmsg))
    assert_equal(1, PRUNE::Message.min_params(:summon))
    assert_equal(1, PRUNE::Message.min_params(:topic))
    assert_equal(1, PRUNE::Message.min_params(:userhost))
    assert_equal(0, PRUNE::Message.min_params(:who))
    assert_equal(1, PRUNE::Message.min_params(:whois))
    assert_equal(1, PRUNE::Message.min_params(:whowas))
  end # def test_min_params

  def test_max_params
    assert_equal(nil, PRUNE::Message.max_params("foo"))
    assert_equal(3, PRUNE::Message.max_params("connect"))
    assert_equal(3, PRUNE::Message.max_params("Connect"))
    assert_equal(3, PRUNE::Message.max_params(:connect))
    assert_equal(0, PRUNE::Message.max_params(:die))
    assert_equal(1, PRUNE::Message.max_params(:error))
    assert_equal(2, PRUNE::Message.max_params(:ping))
    assert_equal(2, PRUNE::Message.max_params(:pong))
    assert_equal(0, PRUNE::Message.max_params(:rehash))
    assert_equal(0, PRUNE::Message.max_params(:restart))
    assert_equal(4, PRUNE::Message.max_params(:service))
    assert_equal(2, PRUNE::Message.max_params(:squit))
    assert_equal(1, PRUNE::Message.max_params(:trace))
    assert_equal(1, PRUNE::Message.max_params(:wallops))
    assert_equal(1, PRUNE::Message.max_params(:admin))
    assert_equal(1, PRUNE::Message.max_params(:away))
    assert_equal(1, PRUNE::Message.max_params(:info))
    assert_equal(2, PRUNE::Message.max_params(:links))
    assert_equal(2, PRUNE::Message.max_params(:list))
    assert_equal(2, PRUNE::Message.max_params(:lusers))
    assert_equal(1, PRUNE::Message.max_params(:motd))
    assert_equal(2, PRUNE::Message.max_params(:oper))
    assert_equal(1, PRUNE::Message.max_params(:pass))
    assert_equal(1, PRUNE::Message.max_params(:quit))
    assert_equal(2, PRUNE::Message.max_params(:servlist))
    assert_equal(2, PRUNE::Message.max_params(:squery))
    assert_equal(2, PRUNE::Message.max_params(:stats))
    assert_equal(1, PRUNE::Message.max_params(:time))
    assert_equal(4, PRUNE::Message.max_params(:user))
    assert_equal(1, PRUNE::Message.max_params(:users))
    assert_equal(1, PRUNE::Message.max_params(:version))
    assert_equal(2, PRUNE::Message.max_params(:invite))
    assert_equal(15, PRUNE::Message.max_params(:ison))
    assert_equal(2, PRUNE::Message.max_params(:join))
    assert_equal(3, PRUNE::Message.max_params(:kick))
    assert_equal(2, PRUNE::Message.max_params(:kill))
    assert_equal(15, PRUNE::Message.max_params(:mode))
    assert_equal(2, PRUNE::Message.max_params(:names))
    assert_equal(1, PRUNE::Message.max_params(:nick))
    assert_equal(2, PRUNE::Message.max_params(:notice))
    assert_equal(2, PRUNE::Message.max_params(:part))
    assert_equal(2, PRUNE::Message.max_params(:privmsg))
    assert_equal(3, PRUNE::Message.max_params(:summon))
    assert_equal(2, PRUNE::Message.max_params(:topic))
    assert_equal(15, PRUNE::Message.max_params(:userhost))
    assert_equal(2, PRUNE::Message.max_params(:who))
    assert_equal(2, PRUNE::Message.max_params(:whois))
    assert_equal(3, PRUNE::Message.max_params(:whowas))
  end # def test_max_params

  def test_n_params
    assert_equal(nil, PRUNE::Message.n_params("foo"))
    assert_equal(2..3, PRUNE::Message.n_params("connect"))
    assert_equal(2..3, PRUNE::Message.n_params("Connect"))
    assert_equal(2..3, PRUNE::Message.n_params(:connect))
    assert_equal(0, PRUNE::Message.n_params(:die))
    assert_equal(1, PRUNE::Message.n_params(:error))
    assert_equal(1..2, PRUNE::Message.n_params(:ping))
    assert_equal(1..2, PRUNE::Message.n_params(:pong))
    assert_equal(0, PRUNE::Message.n_params(:rehash))
    assert_equal(0, PRUNE::Message.n_params(:restart))
    assert_equal([2, 4], PRUNE::Message.n_params(:service))
    assert_equal(2, PRUNE::Message.n_params(:squit))
    assert_equal(0..1, PRUNE::Message.n_params(:trace))
    assert_equal(1, PRUNE::Message.n_params(:wallops))
    assert_equal(0..1, PRUNE::Message.n_params(:admin))
    assert_equal(0..1, PRUNE::Message.n_params(:away))
    assert_equal(0..1, PRUNE::Message.n_params(:info))
    assert_equal(0..2, PRUNE::Message.n_params(:links))
    assert_equal(0..2, PRUNE::Message.n_params(:list))
    assert_equal(0..2, PRUNE::Message.n_params(:lusers))
    assert_equal(0..1, PRUNE::Message.n_params(:motd))
    assert_equal(2, PRUNE::Message.n_params(:oper))
    assert_equal(1, PRUNE::Message.n_params(:pass))
    assert_equal(0..1, PRUNE::Message.n_params(:quit))
    assert_equal(0..2, PRUNE::Message.n_params(:servlist))
    assert_equal(2, PRUNE::Message.n_params(:squery))
    assert_equal(0..2, PRUNE::Message.n_params(:stats))
    assert_equal(0..1, PRUNE::Message.n_params(:time))
    assert_equal(4, PRUNE::Message.n_params(:user))
    assert_equal(0..1, PRUNE::Message.n_params(:users))
    assert_equal(0..1, PRUNE::Message.n_params(:version))
    assert_equal(2, PRUNE::Message.n_params(:invite))
    assert_equal(1..15, PRUNE::Message.n_params(:ison))
    assert_equal(1..2, PRUNE::Message.n_params(:join))
    assert_equal(2..3, PRUNE::Message.n_params(:kick))
    assert_equal(2, PRUNE::Message.n_params(:kill))
    assert_equal(1..15, PRUNE::Message.n_params(:mode))
    assert_equal(0..2, PRUNE::Message.n_params(:names))
    assert_equal(1, PRUNE::Message.n_params(:nick))
    assert_equal(2, PRUNE::Message.n_params(:notice))
    assert_equal(1..2, PRUNE::Message.n_params(:part))
    assert_equal(2, PRUNE::Message.n_params(:privmsg))
    assert_equal(1..3, PRUNE::Message.n_params(:summon))
    assert_equal(1..2, PRUNE::Message.n_params(:topic))
    assert_equal(1..15, PRUNE::Message.n_params(:userhost))
    assert_equal(0..2, PRUNE::Message.n_params(:who))
    assert_equal(1..2, PRUNE::Message.n_params(:whois))
    assert_equal(1..3, PRUNE::Message.n_params(:whowas))
  end # def test_n_params

end # class TestPRUNE__Message

class TestPRUNE__Message__CONNECT < Test::Unit::TestCase

  def setup
    @cmd = :CONNECT
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__CONNECT < Test::Unit::TestCase

class TestPRUNE__Message__DIE < Test::Unit::TestCase

  def setup
    @cmd = "DIE"
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__DIE < Test::Unit::TestCase

class TestPRUNE__Message__ERROR < Test::Unit::TestCase

  def setup
    @cmd = 'ERROR'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__ERROR < Test::Unit::TestCase

class TestPRUNE__Message__PING < Test::Unit::TestCase

  def setup
    @cmd = 'PING'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__PING < Test::Unit::TestCase

class TestPRUNE__Message__PONG < Test::Unit::TestCase

  def setup
    @cmd = 'PONG'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__PONG < Test::Unit::TestCase

class TestPRUNE__Message__REHASH < Test::Unit::TestCase

  def setup
    @cmd = 'REHASH'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__REHASH < Test::Unit::TestCase

class TestPRUNE__Message__RESTART < Test::Unit::TestCase

  def setup
    @cmd = 'RESTART'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__RESTART < Test::Unit::TestCase

class TestPRUNE__Message__SERVICE < Test::Unit::TestCase

  def setup
    @cmd = 'SERVICE'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__SERVICE < Test::Unit::TestCase

class TestPRUNE__Message__SQUIT < Test::Unit::TestCase

  def setup
    @cmd = 'SQUIT'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__SQUIT < Test::Unit::TestCase

class TestPRUNE__Message__TRACE < Test::Unit::TestCase

  def setup
    @cmd = 'TRACE'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__TRACE < Test::Unit::TestCase

class TestPRUNE__Message__WALLOPS < Test::Unit::TestCase

  def setup
    @cmd = 'WALLOPS'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__WALLOPS < Test::Unit::TestCase

class TestPRUNE__Message__ADMIN < Test::Unit::TestCase

  def setup
    @cmd = 'ADMIN'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__ADMIN < Test::Unit::TestCase

class TestPRUNE__Message__AWAY < Test::Unit::TestCase

  def setup
    @cmd = 'AWAY'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__AWAY < Test::Unit::TestCase

class TestPRUNE__Message__INFO < Test::Unit::TestCase

  def setup
    @cmd = 'INFO'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__INFO < Test::Unit::TestCase

class TestPRUNE__Message__LINKS < Test::Unit::TestCase

  def setup
    @cmd = 'LINKS'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__LINKS < Test::Unit::TestCase

class TestPRUNE__Message__LIST < Test::Unit::TestCase

  def setup
    @cmd = 'LIST'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__LIST < Test::Unit::TestCase

class TestPRUNE__Message__LUSERS < Test::Unit::TestCase

  def setup
    @cmd = 'LUSERS'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__LUSERS < Test::Unit::TestCase

class TestPRUNE__Message__MOTD < Test::Unit::TestCase

  def setup
    @cmd = 'MOTD'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__MOTD < Test::Unit::TestCase

class TestPRUNE__Message__OPER < Test::Unit::TestCase

  def setup
    @cmd = 'OPER'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__OPER < Test::Unit::TestCase

class TestPRUNE__Message__PASS < Test::Unit::TestCase

  def setup
    @cmd = 'PASS'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__PASS < Test::Unit::TestCase

class TestPRUNE__Message__QUIT < Test::Unit::TestCase

  def setup
    @cmd = 'QUIT'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__QUIT < Test::Unit::TestCase

class TestPRUNE__Message__SERVLIST < Test::Unit::TestCase

  def setup
    @cmd = 'SERVLIST'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__SERVLIST < Test::Unit::TestCase

class TestPRUNE__Message__SQUERY < Test::Unit::TestCase

  def setup
    @cmd = 'SQUERY'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__SQUERY < Test::Unit::TestCase

class TestPRUNE__Message__STATS < Test::Unit::TestCase

  def setup
    @cmd = 'STATS'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__STATS < Test::Unit::TestCase

class TestPRUNE__Message__TIME < Test::Unit::TestCase

  def setup
    @cmd = 'TIME'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__TIME < Test::Unit::TestCase

class TestPRUNE__Message__USER < Test::Unit::TestCase

  def setup
    @cmd = 'USER'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__USER < Test::Unit::TestCase

class TestPRUNE__Message__USERS < Test::Unit::TestCase

  def setup
    @cmd = 'USERS'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__USERS < Test::Unit::TestCase

class TestPRUNE__Message__VERSION < Test::Unit::TestCase

  def setup
    @cmd = 'VERSION'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__VERSION < Test::Unit::TestCase

class TestPRUNE__Message__INVITE < Test::Unit::TestCase

  def setup
    @cmd = 'INVITE'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('foo', '#foo')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@suffix'}
    assert_equal('#foo', t.channel)
    assert_equal('#foo@suffix', t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('foo', '#foo')")
    assert_equal("#{@cmd} foo :#foo\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} foo :#foo\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} foo :#foo\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} foo :#foo\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} foo :#foo@blah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} foo :#foo@blah\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__INVITE < Test::Unit::TestCase

class TestPRUNE__Message__ISON < Test::Unit::TestCase

  def setup
    @cmd = 'ISON'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__ISON < Test::Unit::TestCase

class TestPRUNE__Message__JOIN < Test::Unit::TestCase

  def setup
    @cmd = 'JOIN'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('#foo')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    t = eval("PRUNE::Message::#{@cmd}.new('#foo,#bar,#baz')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_equal(['#foo', '#bar', '#baz'], t.channel({:array=>true}))
    assert_equal(['#foo', '#bar', '#baz'], t.channel({:suffix=>true, :array=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    assert_equal(['#foo', '#bar', '#baz'], t.channel({:array=>true}))
    assert_equal(['#foo@blah', '#bar@blah', '#baz@blah'], t.channel({:suffix=>true, :array=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('#foo')")
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} :#foo@blah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} :#foo@blah\r\n", t.to_s)
    t = eval("PRUNE::Message::#{@cmd}.new('#foo,#bar,#baz', '&blah')")
    assert_equal("#{@cmd} #foo,#bar,#baz :&blah\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} #foo,#bar,#baz :&blah\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} #foo,#bar,#baz :&blah\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} #foo,#bar,#baz :&blah\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} #foo@blah,#bar@blah,#baz@blah :&blah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} #foo@blah,#bar@blah,#baz@blah :&blah\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__JOIN < Test::Unit::TestCase

class TestPRUNE__Message__KICK < Test::Unit::TestCase

  def setup
    @cmd = 'KICK'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', 'foo')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    t = eval("PRUNE::Message::#{@cmd}.new('#foo,#bar', 'foo,bar')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:array=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:suffix=>true, :array=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:array=>true}))
    assert_equal(['#foo@blah', '#bar@blah'], t.channel({:suffix=>true, :array=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', 'foo')")
    assert_equal("#{@cmd} #foo :foo\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} #foo :foo\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} #foo :foo\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} #foo :foo\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} #foo@blah :foo\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} #foo@blah :foo\r\n", t.to_s)
    t = eval("PRUNE::Message::#{@cmd}.new('#foo,#bar', 'foo,bar')")
    assert_equal("#{@cmd} #foo,#bar :foo,bar\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} #foo,#bar :foo,bar\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} #foo,#bar :foo,bar\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} #foo,#bar :foo,bar\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} #foo@blah,#bar@blah :foo,bar\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} #foo@blah,#bar@blah :foo,bar\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__KICK < Test::Unit::TestCase

class TestPRUNE__Message__KILL < Test::Unit::TestCase

  def setup
    @cmd = 'KILL'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__KILL < Test::Unit::TestCase

class TestPRUNE__Message__MODE < Test::Unit::TestCase

  def setup
    @cmd = 'MODE'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', '+o', 'foo')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', '+o', 'foo')")
    assert_equal("#{@cmd} #foo +o :foo\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} #foo +o :foo\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} #foo +o :foo\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} #foo +o :foo\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} #foo@blah +o :foo\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} #foo@blah +o :foo\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__MODE < Test::Unit::TestCase

class TestPRUNE__Message__NAMES < Test::Unit::TestCase

  def setup
    @cmd = 'NAMES'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('#foo')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    t = eval("PRUNE::Message::#{@cmd}.new('#foo,#bar')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:array=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:suffix=>true, :array=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:array=>true}))
    assert_equal(['#foo@blah', '#bar@blah'], t.channel({:suffix=>true, :array=>true}))
    t = eval("PRUNE::Message::#{@cmd}.new('')")
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('#foo')")
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} :#foo@blah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} :#foo@blah\r\n", t.to_s)
    t = eval("PRUNE::Message::#{@cmd}.new('#foo,#bar')")
    assert_equal("#{@cmd} :#foo,#bar\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} :#foo,#bar\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} :#foo,#bar\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} :#foo,#bar\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} :#foo@blah,#bar@blah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} :#foo@blah,#bar@blah\r\n", t.to_s)
    t = eval("PRUNE::Message::#{@cmd}.new('')")
    assert_equal("#{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} \r\n", t.to_s)
    t = eval("PRUNE::Message::#{@cmd}.new(nil)")
    assert_equal("#{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} \r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} \r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__NAMES < Test::Unit::TestCase

class TestPRUNE__Message__NICK < Test::Unit::TestCase

  def setup
    @cmd = 'NICK'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__NICK < Test::Unit::TestCase

class TestPRUNE__Message__NOTICE < Test::Unit::TestCase

  def setup
    @cmd = 'NOTICE'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', 'blahblahblah')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    t = eval("PRUNE::Message::#{@cmd}.new('foo', 'blahblahblah')")
    assert_equal('foo', t.channel)
    assert_equal('foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('foo', t.channel)
    assert_equal('foo@blah', t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', 'blahblahblah')")
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} #foo@blah :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} #foo@blah :blahblahblah\r\n", t.to_s)
    t = eval("PRUNE::Message::#{@cmd}.new('foo', 'blahblahblah')")
    assert_equal("#{@cmd} foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo@blah!foo@example.com #{@cmd} foo@blah :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} foo@blah :blahblahblah\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__NOTICE < Test::Unit::TestCase

class TestPRUNE__Message__PART < Test::Unit::TestCase

  def setup
    @cmd = 'PART'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('#foo')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    t = eval("PRUNE::Message::#{@cmd}.new('#foo,#bar')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:array=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:suffix=>true,:array=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    assert_equal(['#foo', '#bar'], t.channel({:array=>true}))
    assert_equal(['#foo@blah', '#bar@blah'], t.channel({:suffix=>true,:array=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('#foo')")
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} :#foo\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} :#foo@blah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} :#foo@blah\r\n", t.to_s)
    t = eval("PRUNE::Message::#{@cmd}.new('#foo,#bar')")
    assert_equal("#{@cmd} :#foo,#bar\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} :#foo,#bar\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} :#foo,#bar\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} :#foo,#bar\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} :#foo@blah,#bar@blah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} :#foo@blah,#bar@blah\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__PART < Test::Unit::TestCase

class TestPRUNE__Message__PRIVMSG < Test::Unit::TestCase

  def setup
    @cmd = 'PRIVMSG'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', 'blahblahblah')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
    t = eval("PRUNE::Message::#{@cmd}.new('foo', 'blahblahblah')")
    assert_equal('foo', t.channel)
    assert_equal('foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('foo', t.channel)
    assert_equal('foo@blah', t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', 'blahblahblah')")
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} #foo@blah :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} #foo@blah :blahblahblah\r\n", t.to_s)
    t = eval("PRUNE::Message::#{@cmd}.new('foo', 'blahblahblah')")
    assert_equal("#{@cmd} foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo@blah!foo@example.com #{@cmd} foo@blah :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} foo@blah :blahblahblah\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__PRIVMSG < Test::Unit::TestCase

class TestPRUNE__Message__SUMMON < Test::Unit::TestCase

  def setup
    @cmd = 'SUMMON'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__SUMMON < Test::Unit::TestCase

class TestPRUNE__Message__TOPIC < Test::Unit::TestCase

  def setup
    @cmd = 'TOPIC'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', 'blahblahblah')")
    assert_equal('#foo', t.channel)
    assert_equal('#foo', t.channel({:suffix=>true}))
    assert_nothing_raised {t.suffix = '@blah'}
    assert_equal('@blah', t.suffix)
    assert_equal('#foo', t.channel)
    assert_equal('#foo@blah', t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('#foo', 'blahblahblah')")
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} #foo :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} #foo@blah :blahblahblah\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} #foo@blah :blahblahblah\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__TOPIC < Test::Unit::TestCase

class TestPRUNE__Message__USERHOST < Test::Unit::TestCase

  def setup
    @cmd = 'USERHOST'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__USERHOST < Test::Unit::TestCase

class TestPRUNE__Message__WHO < Test::Unit::TestCase

  def setup
    @cmd = 'WHO'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('jto*', 'o')")
    assert_equal('jto*', t.channel)
    assert_equal('jto*', t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('jto*', 'o')")
    assert_equal("#{@cmd} jto* :o\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} jto* :o\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} jto* :o\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} jto* :o\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "@blah"}
    assert_equal("@blah", t.suffix)
    assert_equal(":foo@blah!foo@example.com #{@cmd} jto*@blah :o\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} jto*@blah :o\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__WHO < Test::Unit::TestCase

class TestPRUNE__Message__WHOIS < Test::Unit::TestCase

  def setup
    @cmd = 'WHOIS'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__WHOIS < Test::Unit::TestCase

class TestPRUNE__Message__WHOWAS < Test::Unit::TestCase

  def setup
    @cmd = 'WHOWAS'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(PRUNE::Error::InvalidMessage) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal(PRUNE::Message.normalize(@cmd), t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(["foo"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("foo", t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(["foo", "bar", "baz"], t.params)
    assert_equal("foo", t.params(0))
    assert_equal("baz", t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(["foo", "bar", "baz"], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal(":foo!foo@example.com #{@cmd} irc.example.com :6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("#{@cmd} irc.example.com :6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6667)")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new('irc.example.com', 6668)")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__WHOWAS < Test::Unit::TestCase

class TestPRUNE__Message__RAW < Test::Unit::TestCase

  def setup
    @cmd = 'RAW'
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    assert_raises(TypeError) {t = eval("PRUNE::Message::#{@cmd}.new(:foo)")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new(\"foo\")")}
    assert_nothing_raised {eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'FOO', :params=>['foo', 'bar']})")}
  end # def test_functional

  def test_enforced_delivery=
    # should be done in test_is_enforced_delivery
  end # def test_enforced_delivery=

  def test_is_enforced_delivery
    t = eval("PRUNE::Message::#{@cmd}.new('foo')")
    assert_equal(false, t.is_enforced_delivery?)
    assert_nothing_raised {t.enforced_delivery = true}
    assert_equal(true, t.is_enforced_delivery?)
  end # def test_is_enforced_delivery

  def test_nick
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.nick)
    assert_nothing_raised {t.nick = "bar"}
    assert_equal("bar", t.nick)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'FOO', :params=>['foo', 'bar']})")
    assert_equal("foo", t.nick)
    assert_nothing_raised {t.nick = "baz"}
    assert_equal("baz", t.nick)
  end # def test_nick

  def test_nick_
    # should be done in test_nick
  end # def test_nick_

  def test_user
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.user)
    assert_nothing_raised {t.user = "bar"}
    assert_equal("bar", t.user)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'FOO', :command=>'FOO', :params=>['foo', 'bar']})")
    assert_equal("foo", t.user)
    assert_nothing_raised {t.user = "baz"}
    assert_equal("baz", t.user)
  end # def test_user

  def test_user_
    # should be done in test_user
  end # def test_user_

  def test_host
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.host)
    assert_nothing_raised {t.host = "bar"}
    assert_equal("bar", t.host)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'FOO', :params=>['foo', 'bar']})")
    assert_equal("example.com", t.host)
    assert_nothing_raised {t.host = "baz"}
    assert_equal("baz", t.host)
  end # def test_host

  def test_host_
    # should be done in test_host
  end # def test_host_

  def test_time
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(true, t.time.kind_of?(Time))
    x = Time.now
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'FOO', :params=>['foo', 'bar'], :time=>x})")
    assert_equal(x, t.time)
  end # def test_time

  def test_command
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal(nil, t.command)
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar']})")
    assert_equal('foo', t.command)
  end # def test_command

  def test_params
    t = eval("PRUNE::Message::#{@cmd}.new(\"foo\")")
    assert_equal([], t.params)
    assert_equal(nil, t.params(0))
    assert_equal(nil, t.params(-1))
    assert_equal(nil, t.params(1))
    t = eval("PRUNE::Message::#{@cmd}.new({:prefix=>{:nick=>'foo',:user=>'foo',:host=>'example.com'}, :command=>'foo', :params=>['foo', 'bar', 'baz']})")
    assert_equal(['foo', 'bar', 'baz'], t.params)
    assert_equal('foo', t.params(0))
    assert_equal('baz', t.params(-1))
    assert_equal(nil, t.params(3))
    assert_equal(['foo', 'bar', 'baz'], t.params(nil))
    assert_raises(TypeError) {t.params(:foo)}
    assert_raises(TypeError) {t.params("0")}
  end # def test_params

  def test_channel
    t = eval("PRUNE::Message::#{@cmd}.new(\"FOO irc.example.com 6667\\r\\n\")")
    # no channel required in this command
    assert_equal(nil, t.channel)
    assert_equal(nil, t.channel({:suffix=>true}))
  end # def test_channel

  def test_to_s
    t = eval("PRUNE::Message::#{@cmd}.new(\"FOO irc.example.com 6667\\r\\n\")")
    assert_equal("FOO irc.example.com 6667\r\n", t.to_s)
    assert_nothing_raised {t.nick = "foo"}
    assert_equal("FOO irc.example.com 6667\r\n", t.to_s)
    assert_nothing_raised {t.user = "foo"}
    assert_equal("FOO irc.example.com 6667\r\n", t.to_s)
    assert_nothing_raised {t.host = "example.com"}
    assert_equal("FOO irc.example.com 6667\r\n", t.to_s)
    assert_nothing_raised {t.suffix = "blah"}
    assert_equal("blah", t.suffix)
    assert_equal("FOO irc.example.com 6667\r\n", t.to_s)
    assert_nothing_raised {t.user = nil}
    assert_equal("FOO irc.example.com 6667\r\n", t.to_s)
  end # def test_to_s

  def test_suffix
    # should be done in test_to_s
  end # def test_suffix

  def test_suffix_
    # should be done in test_to_s
  end # def test_suffix_

  def test_equal
    t = eval("PRUNE::Message::#{@cmd}.new(\"FOO irc.example.com :6667\\r\\n\")")
    t2 = eval("PRUNE::Message::#{@cmd}.new(\"FOO irc.example.com :6667\\r\\n\")")
    assert_equal(true, (t == t2))
    assert_equal(false, (t != t2))
    t2 = eval("PRUNE::Message::#{@cmd}.new(\"FOO irc.example.com :6668\\r\\n\")")
    assert_equal(false, (t == t2))
    assert_equal(true, (t != t2))
  end # def test_equal

end # class TestPRUNE__Message__RAW < Test::Unit::TestCase


if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
