# debug.rb
# Copyright (C) 2005-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'prune/debug'

class Validator
  include PRUNE::Debug

  def PRUNE._output(msg)
    return msg
  end # def _output

  alias :debug2 :debug

  def debug(cat, fmt, *msg)
    debug2(cat, fmt, *msg)
  end # def debug

  alias :info2 :info

  def info(fmt, *msg)
    info2(fmt, *msg)
  end # def info

  alias :warning2 :warning

  def warning(fmt, *msg)
    warning2(fmt, *msg)
  end # def warning

  alias :bug2 :bug

  def bug(fmt, *msg)
    bug2(fmt, *msg)
  end # def bug

  alias :FIXME2 :FIXME

  def FIXME(fmt, *msg)
    FIXME2(fmt, *msg)
  end # def FIXME

  alias :debug_sprintf2 :debug_sprintf

  def debug_sprintf(fmt, *args)
    debug_sprintf2(fmt, *args)
  end # def debug_sprintf

end # class Validator


class TestPRUNE__Debug < Test::Unit::TestCase

  def setup
    @t = Validator.new
  end # def setup

  def teardown
    
  end # def teardown

  def test_info
    assert_equal("INFO ***: foo", @t.info("foo"))
    assert_equal("INFO ***: int 10", @t.info("int %d", 10))
    assert_equal("INFO ***: str \"foo\"", @t.info("str %s", "foo"))
    assert_equal("INFO ***: array [10]", @t.info("array %s", [10]))
    assert_equal("INFO ***: hash {\"foo\"=>10}", @t.info("hash %s", {'foo'=>10}))
    assert_equal("INFO ***: nil NIL", @t.info("nil %s", nil))
  end # def test_info

  def test_warning
    assert_equal("WARNING ***: foo", @t.warning("foo"))
    assert_equal("WARNING ***: int 10", @t.warning("int %d", 10))
    assert_equal("WARNING ***: str \"foo\"", @t.warning("str %s", "foo"))
    assert_equal("WARNING ***: array [10]", @t.warning("array %s", [10]))
    assert_equal("WARNING ***: hash {\"foo\"=>10}", @t.warning("hash %s", {'foo'=>10}))
    assert_equal("WARNING ***: nil NIL", @t.warning("nil %s", nil))
  end # def test_warning

  def test_bug
    assert_equal("[BUG] foo", @t.bug("foo"))
    assert_equal("[BUG] int 10", @t.bug("int %d", 10))
    assert_equal("[BUG] str \"foo\"", @t.bug("str %s", "foo"))
    assert_equal("[BUG] array [10]", @t.bug("array %s", [10]))
    assert_equal("[BUG] hash {\"foo\"=>10}", @t.bug("hash %s", {'foo'=>10}))
    assert_equal("[BUG] nil NIL", @t.bug("nil %s", nil))
  end # def test_bug

  def test_FIXME
    assert_equal("FIXME!!! foo", @t.FIXME("foo"))
    assert_equal("FIXME!!! int 10", @t.FIXME("int %d", 10))
    assert_equal("FIXME!!! str \"foo\"", @t.FIXME("str %s", "foo"))
    assert_equal("FIXME!!! array [10]", @t.FIXME("array %s", [10]))
    assert_equal("FIXME!!! hash {\"foo\"=>10}", @t.FIXME("hash %s", {'foo'=>10}))
    assert_equal("FIXME!!! nil NIL", @t.FIXME("nil %s", nil))
  end # def test_FIXME

  def test_debug_sprintf
    assert_equal("foo", @t.debug_sprintf("foo"))
    assert_equal("int 10", @t.debug_sprintf("int %d", 10))
    assert_equal("str \"foo\"", @t.debug_sprintf("str %s", "foo"))
    assert_equal("array [10]", @t.debug_sprintf("array %s", [10]))
    assert_equal("hash {\"foo\"=>10}", @t.debug_sprintf("hash %s", {'foo'=>10}))
    assert_equal("nil NIL", @t.debug_sprintf("nil %s", nil))
  end # def test_debug_sprintf

end # class TestPRUNE__Debug

if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
