# irc.rb<2>
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'timeout'
require 'prune/irc'

class IO

  class << self

    def select(rq, wq, eq, to)
      retval = [[], [], []]

      begin
        x = Proc.new do |q, ret, no_loop|
          loop do
            q.each do |x|
              unless x.queue.empty? then
                ret << x unless ret.include?(x)
              end
            end
            break if !ret.empty? || no_loop
          end
        end
        Timeout.timeout(to) do
          r = Thread.new(rq, retval[0]) do |rrq, ret|
            x.call(rrq, ret, to == 0) unless rrq.nil?
          end
          w = Thread.new(wq, retval[1]) do |wwq, ret|
            x.call(wwq, ret, to == 0) unless wwq.nil?
          end
          e = Thread.new(eq, retval[2]) do |eeq, ret|
            x.call(eeq, ret, to == 0) unless eeq.nil?
          end
          r.join
          w.join
          e.join
        end
      rescue Timeout::Error
        retval = nil
        r.exit
        w.exit
        e.exit
      end

      retval
    end # def IO.select

  end

end

class PRUNE::TCPSocket

  def initialize(host, port, prefix = nil)
    @host = host
    @port = port
    @prefix = prefix
    @stop = false
    @floodcontrol = true
    @status = []
    @status.extend(PRUNE::Lock_m)
    @iq = PRUNE::Queue.new
    @oq = PRUNE::Queue.new
    @eof = false
    @closed = false
    sleep 1
  end

  def closed?
    @closed
  end # def closed?

  def close
    @eof = true
    self.stop
    @closed = true
  end

  def addr
    ["AF_INET", @port, @host, IPSocket.getaddress(@host)]
  end

  def eof?
    @eof
  end # def 

  def gets(*args)
    # XXX
    read
  end

  def read(length = nil, buffer = nil)
    x = @iq.shift
    unless x.nil? then
      unless length.nil? then
        xx = x.split(//)
        x = xx[0..length-1].join
        @iq.unshift(xx[length-1..-1].join)
      end
    end
    unless buffer.nil? then
      buffer = x
    end

    x
  end

  def write(string)
    @oq << string
  end

  def push(*args)
    @iq.push(*args)
  end # def push

  def queue
    @iq
  end

  def result
    @oq
  end # def result

  def clear
    @iq.clear
    @oq.clear
  end # def clear

end

class TestPRUNE__IRC < Test::Unit::TestCase

  def setup
    
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    
  end # def test_functional

end # class TestPRUNE__IRC

class TestPRUNE__IRCConnector < Test::Unit::TestCase

  def setup
  end # def setup

  def teardown
    
  end # def teardown

  def test_functional
    
  end # def test_functional

  def test_353_received
    t = PRUNE::IRCConnector.new
    t.open(PRUNE::TYPE::HostInfoStruct.new('localhost', 6667, 'foo', 'foo', 'foo',{},nil))
    sd = t.instance_variable_get("@sdelegate")
    until sd.result.length >= 2 do
      sleep 1
    end

    sd.push("001 blahblah blahblah\r\n")
    sd.push("JOIN #fedora-devel\r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :tagoh mintos fraggle_ musolinoa zer0c00l jaswinder_ dtardon _Hicham_ paragan mbacovsk RodrigoPadula danielbruno sgallagh Shrink than_home ehabkost dvlasenk mherman Sonar_Guy nhorman jnovy biertie _tydeas_ hachiman atkac jmoskovc bochecha sc0field marcus_ akurtakov jankratochvil thomastp ndrs__ AndyP hughsie ssp diegobz che RaoulDuke SMParrish nphilipp bpepple thomasj Pikachu_2015 KageSenshi kdudka mefoster npmccallum jreznik_afk liquidat \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :tim4dev alatyr fab rakesh rvokal slaine_ maploin schlobinux eseyman Panu wolfy npatil jsafrane aarapov c4chris giallu thoger jhorak beekhof zodbot_ zwu holger_ Urs_ShPo kashyapc Cheshirc kaitlin blitzkrieg3 aholler_ sudhir pcfe itamarjp ffesti_ Sonar_Gal stu|laptop sonar_logger2 ReneP cebbert stickster_afk N3LRX kaio fnasser_ hpa_at_work LinDon|afk dmaphy donavan lkundrak mitr uwog DrNick jmatthews_ abartlet warren talcite nb_ jhunt \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :Pikachu_2014 nokia3510 dazo mhlavink plautrba ivana pp_ BoChao|AFK sandeen furai Nushio theskunk wwoods Dodji jlaska josef sadmac rprice` mikem23 jon_vanalten fraggle_laptop Dp67 ricky ijuma_ ubijtsa jnettlet_ kasal than NotTheAdmin pknirsch generalBordeaux gregdek mbonnet_ delhage cyberpear jonmasters killefiz pink_trout londo jwb davej kanarip jlb|away nb cjb cpg geppetto Sir_Gallantmon ctyler smooge Tuju dgregor dwalluck Kick_ \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :spevack tuatha sl33v3 rsquared lxo twoerner muep_ jdav_gone hpa sadmac_home |Frederik LetoTo1 sandeen_ jlindsay vpv ijuma bernie kalev [0x100] nim-nim SirThomas llaumgui maxamillion Ac-town ajami Harley-D stiv2k ebassi tmz rwmjones opsec gbeshers_home trashy adamw londo_ eponyme jmp Ryan52 ianweller ajax dnsn lon j-rod daemoen1 xover_ mezcalero shortstack ndim eddyp_work ndevos_ lmacken kylem ensc poelcat giesen jds2001 juhp kylev \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :Plouj saispo nwnk nirik hno Sparks dbhole mnagy torshido pjones Risar paulbsch mjg59 fabbione pgf jbowes Edgan Cope paragn thm mebus teprrr mmcgrath clumens silfreed UdontKnow bugbot_ daniel_hozac pingou awjb fenrus02 mharris skvidal ensc|w Viking-Ice goncalo_ dledford_ timlau pcheung_ dmlloyd Oxf13 pixelbeat dgilmore abadger1999 tibbs|h ke4qqq jforbes quaid bress wwwnick th0br0 nosnilmot MerlinTHP silug z00dax yaneti Guest99296 Lalufu \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :ingvarha zykes- tagoh3 chii tomaw_ Jeff_S thomasvs albiorix @ChanServ EvilBob darktama Nrg_ whot simo jiteshs neurocyte knurd daq4th felipe_ Zerberus dwmw2 AndyCap agk SmootherFrOgZ vanaltj blackops G_work rdieter_work airlied daju adrianr tonyb joe sharkcz johe|work bmr spot sm|CPU mchua_afk wakko666 ixs rsc halfline \r\n")
    sleep 5
    assert_equal(["#fedora-devel"], t.state.channels)
    assert_equal("foo tagoh mintos fraggle_ musolinoa zer0c00l jaswinder_ dtardon _Hicham_ paragan mbacovsk RodrigoPadula danielbruno sgallagh Shrink than_home ehabkost dvlasenk mherman Sonar_Guy nhorman jnovy biertie _tydeas_ hachiman atkac jmoskovc bochecha sc0field marcus_ akurtakov jankratochvil thomastp ndrs__ AndyP hughsie ssp diegobz che RaoulDuke SMParrish nphilipp bpepple thomasj Pikachu_2015 KageSenshi kdudka mefoster npmccallum jreznik_afk liquidat tim4dev alatyr fab rakesh rvokal slaine_ maploin schlobinux eseyman Panu wolfy npatil jsafrane aarapov c4chris giallu thoger jhorak beekhof zodbot_ zwu holger_ Urs_ShPo kashyapc Cheshirc kaitlin blitzkrieg3 aholler_ sudhir pcfe itamarjp ffesti_ Sonar_Gal stu|laptop sonar_logger2 ReneP cebbert stickster_afk N3LRX kaio fnasser_ hpa_at_work LinDon|afk dmaphy donavan lkundrak mitr uwog DrNick jmatthews_ abartlet warren talcite nb_ jhunt Pikachu_2014 nokia3510 dazo mhlavink plautrba ivana pp_ BoChao|AFK sandeen furai Nushio theskunk wwoods Dodji jlaska josef sadmac rprice` mikem23 jon_vanalten fraggle_laptop Dp67 ricky ijuma_ ubijtsa jnettlet_ kasal than NotTheAdmin pknirsch generalBordeaux gregdek mbonnet_ delhage cyberpear jonmasters killefiz pink_trout londo jwb davej kanarip jlb|away nb cjb cpg geppetto Sir_Gallantmon ctyler smooge Tuju dgregor dwalluck Kick_ spevack tuatha sl33v3 rsquared lxo twoerner muep_ jdav_gone hpa sadmac_home |Frederik LetoTo1 sandeen_ jlindsay vpv ijuma bernie kalev [0x100] nim-nim SirThomas llaumgui maxamillion Ac-town ajami Harley-D stiv2k ebassi tmz rwmjones opsec gbeshers_home trashy adamw londo_ eponyme jmp Ryan52 ianweller ajax dnsn lon j-rod daemoen1 xover_ mezcalero shortstack ndim eddyp_work ndevos_ lmacken kylem ensc poelcat giesen jds2001 juhp kylev Plouj saispo nwnk nirik hno Sparks dbhole mnagy torshido pjones Risar paulbsch mjg59 fabbione pgf jbowes Edgan Cope paragn thm mebus teprrr mmcgrath clumens silfreed UdontKnow bugbot_ daniel_hozac pingou awjb fenrus02 mharris skvidal ensc|w Viking-Ice goncalo_ dledford_ timlau pcheung_ dmlloyd Oxf13 pixelbeat dgilmore abadger1999 tibbs|h ke4qqq jforbes quaid bress wwwnick th0br0 nosnilmot MerlinTHP silug z00dax yaneti Guest99296 Lalufu ingvarha zykes- tagoh3 chii tomaw_ Jeff_S thomasvs albiorix @ChanServ EvilBob darktama Nrg_ whot simo jiteshs neurocyte knurd daq4th felipe_ Zerberus dwmw2 AndyCap agk SmootherFrOgZ vanaltj blackops G_work rdieter_work airlied daju adrianr tonyb joe sharkcz johe|work bmr spot sm|CPU mchua_afk wakko666 ixs rsc halfline".gsub(/[@+]/,'').split(/ /).sort, t.state.channel("#fedora-devel").nicks.sort)
    sd.push("PART #fedora-devel\r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :tagoh mintos fraggle_ musolinoa zer0c00l jaswinder_ dtardon _Hicham_ paragan mbacovsk RodrigoPadula danielbruno sgallagh Shrink than_home ehabkost dvlasenk mherman Sonar_Guy nhorman jnovy biertie _tydeas_ hachiman atkac jmoskovc bochecha sc0field marcus_ akurtakov jankratochvil thomastp ndrs__ AndyP hughsie ssp diegobz che RaoulDuke SMParrish nphilipp bpepple thomasj Pikachu_2015 KageSenshi kdudka mefoster npmccallum jreznik_afk liquidat \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :tim4dev alatyr fab rakesh rvokal slaine_ maploin schlobinux eseyman Panu wolfy npatil jsafrane aarapov c4chris giallu thoger jhorak beekhof zodbot_ zwu holger_ Urs_ShPo kashyapc Cheshirc kaitlin blitzkrieg3 aholler_ sudhir pcfe itamarjp ffesti_ Sonar_Gal stu|laptop sonar_logger2 ReneP cebbert stickster_afk N3LRX kaio fnasser_ hpa_at_work LinDon|afk dmaphy donavan lkundrak mitr uwog DrNick jmatthews_ abartlet warren talcite nb_ jhunt \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :Pikachu_2014 nokia3510 dazo mhlavink plautrba ivana pp_ BoChao|AFK sandeen furai Nushio theskunk wwoods Dodji jlaska josef sadmac rprice` mikem23 jon_vanalten fraggle_laptop Dp67 ricky ijuma_ ubijtsa jnettlet_ kasal than NotTheAdmin pknirsch generalBordeaux gregdek mbonnet_ delhage cyberpear jonmasters killefiz pink_trout londo jwb davej kanarip jlb|away nb cjb cpg geppetto Sir_Gallantmon ctyler smooge Tuju dgregor dwalluck Kick_ \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :spevack tuatha sl33v3 rsquared lxo twoerner muep_ jdav_gone hpa sadmac_home |Frederik LetoTo1 sandeen_ jlindsay vpv ijuma bernie kalev [0x100] nim-nim SirThomas llaumgui maxamillion Ac-town ajami Harley-D stiv2k ebassi tmz rwmjones opsec gbeshers_home trashy adamw londo_ eponyme jmp Ryan52 ianweller ajax dnsn lon j-rod daemoen1 xover_ mezcalero shortstack ndim eddyp_work ndevos_ lmacken kylem ensc poelcat giesen jds2001 juhp kylev \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :Plouj saispo nwnk nirik hno Sparks dbhole mnagy torshido pjones Risar paulbsch mjg59 fabbione pgf jbowes Edgan Cope paragn thm mebus teprrr mmcgrath clumens silfreed UdontKnow bugbot_ daniel_hozac pingou awjb fenrus02 mharris skvidal ensc|w Viking-Ice goncalo_ dledford_ timlau pcheung_ dmlloyd Oxf13 pixelbeat dgilmore abadger1999 tibbs|h ke4qqq jforbes quaid bress wwwnick th0br0 nosnilmot MerlinTHP silug z00dax yaneti Guest99296 Lalufu \r\n")
    sd.push(":calvino.freenode.net 353 tagoh = #Fedora-Devel :ingvarha zykes- tagoh3 chii tomaw_ Jeff_S thomasvs albiorix @ChanServ EvilBob darktama Nrg_ whot simo jiteshs neurocyte knurd daq4th felipe_ Zerberus dwmw2 AndyCap agk SmootherFrOgZ vanaltj blackops G_work rdieter_work airlied daju adrianr tonyb joe sharkcz johe|work bmr spot sm|CPU mchua_afk wakko666 ixs rsc halfline \r\n")
    sleep 5
    assert_equal([], t.state.channels)
    assert_equal(false, t.state.joined?("#fedora-devel", "foo"))

    t.close
  end # def test_353_received

end # class TestPRUNE__IRCConnector


if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
