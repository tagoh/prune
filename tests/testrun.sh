#! /bin/sh

ARGS=

if [ "x$1" = "x" ]; then
    echo "Usage: $0 <filename>"
    exit 1
fi

while [ "x$1" != "x" ]; do
    ARGS="$ARGS $1"
    shift
done

echo "ruby $ARGS"
RUBYLIB=.. ruby $ARGS
