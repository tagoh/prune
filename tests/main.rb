# main.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'test/unit/ui/console/testrunner'

suite = Test::Unit::TestSuite.new

ObjectSpace.each_object(Class) do |klass|
  if klass.ancestors.include?(Test::Unit::TestCase) && klass != Test::Unit::TestCase then
    suite << klass.suite
  end
end

result = Test::Unit::UI::Console::TestRunner.run(suite)
if result.failure_count > 0 || result.error_count > 0 then
  exit 1
end
