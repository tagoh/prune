# -*- encoding: utf-8 -*-
# parser.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <tagoh@redhat.com>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'rubygems'
gem 'test-unit'
require 'test/unit/testcase'
require 'prune/parser'

class TestPRUNE__Parser < Test::Unit::TestCase

  def setup
    @t = PRUNE::Parser.new
    def @t.raw_parse(data)
      msg = parse(data)
      raw = msg.__send__(:instance_variable_get, "@message")
      raw.delete(:time)
      raw
    end
  end # def setup

  def teardown

  end # def teardown

  def test_functional
    assert_nothing_raised {t = PRUNE::Parser.new}
  end # def test_functional

  def test_parse
    assert_nothing_raised {@t.raw_parse(":freenode-connect!freenode@freenode/bot/connect PRIVMSG prunetest :VERSION\r\n")}
    assert_equal({
                   :prefix=>{
                     :nick=>'freenode-connect',
                     :user=>'freenode',
                     :host=>'freenode/bot/connect'},
                   :command=>'PRIVMSG',
                   :params=>["prunetest", "VERSION"],
                   :suffix=>''},
                 @t.raw_parse(":freenode-connect!freenode@freenode/bot/connect PRIVMSG prunetest :VERSION\r\n"))
    assert_nothing_raised {@t.raw_parse(":minori.linux.or.jp 252 prunetest 1 :flagged staff members\r\n")}
    assert_equal({
                   :prefix=>{
                     :nick=>nil,
                     :user=>nil,
                     :host=>'minori.linux.or.jp'},
                   :command=>'252',
                   :params=>["prunetest", "1", "flagged staff members"],
                   :suffix=>''},
                 @t.raw_parse(":minori.linux.or.jp 252 prunetest 1 :flagged staff members\r\n"))
    assert_nothing_raised {@t.raw_parse(":christel!i=christel@freenode/staff/exherbo.christel NOTICE $* :[Global Notice] Hi all, unfortunately one of our former developers has left behind a memory leak in our ircd software which means we'll need to restart several ircds over the next few days. We're going to stagger it to reduce disruption, and the first round will be happening very shortly. Affected users for now will be about 1300. Apologies for the inconvenience!\r\n")}
    assert_equal({
                   :prefix=>{
                     :nick=>'christel',
                     :user=>'christel',
                     :host=>'freenode/staff/exherbo.christel'},
                   :command=>'NOTICE',
                   :params=>["$*", "[Global Notice] Hi all, unfortunately one of our former developers has left behind a memory leak in our ircd software which means we'll need to restart several ircds over the next few days. We're going to stagger it to reduce disruption, and the first round will be happening very shortly. Affected users for now will be about 1300. Apologies for the inconvenience!"],
                   :suffix=>''},
                 @t.raw_parse(":christel!i=christel@freenode/staff/exherbo.christel NOTICE $* :[Global Notice] Hi all, unfortunately one of our former developers has left behind a memory leak in our ircd software which means we'll need to restart several ircds over the next few days. We're going to stagger it to reduce disruption, and the first round will be happening very shortly. Affected users for now will be about 1300. Apologies for the inconvenience!\r\n"))
    assert_nothing_raised {@t.raw_parse(":timmy!~tboisver@dhcp-193-203.nrt.redhat.com PRIVMSG #japan : :)\r\n")}
    assert_equal({
                   :prefix=>{
                     :nick=>'timmy',
                     :user=>'~tboisver',
                     :host=>'dhcp-193-203.nrt.redhat.com'},
                   :command=>'PRIVMSG',
                   :params=>["#japan", " :)"],
                   :suffix=>''},
                 @t.raw_parse(":timmy!~tboisver@dhcp-193-203.nrt.redhat.com PRIVMSG #japan : :)\r\n"))
    assert_nothing_raised {@t.raw_parse("PRIVMSG #foo ::の\r\n")}
    assert_equal({
                   :prefix=>{
                     :nick=>nil,
                     :user=>nil,
                     :host=>nil},
                   :command=>'PRIVMSG',
                   :params=>["#foo", ":の"],
                   :suffix=>''},
                 @t.raw_parse("PRIVMSG #foo ::の\r\n"))
    assert_nothing_raised {@t.raw_parse(":buggbot!n=bugbot@landfill.bugzilla.org PRIVMSG #fedora-devel :Bug https://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=527712 low, low, ---, sandmann, NEW, unnecessary dependency on pcsc-lite?\r\n")}
    assert_nothing_raised {@t.raw_parse(":BenjaminKreuter!n=Benjamin@c-24-125-42-219.hsdl.va.comcast.net QUIT :Read error: 110 (Connection timed out)\r\n")}
    assert_nothing_raised {@t.raw_parse(":npmccallum!n=npmccall@cpe-76-177-118-80.natcky.res.rr.com JOIN :#fedora-devel\r\n")}
    assert_nothing_raised {@t.raw_parse(":takuo!~takuo@irc6.airs.net PRIVMSG #Northeye :もぺ\r\n")}
    assert_raises {@t.raw_parse(":foo!foo@localhost.localdomain PRIVMSG #foo : \r\n")}
    assert_nothing_raised {@t.raw_parse(":ratbox.services MODE #eng-services +o CHANSERV   \r\n")}
    assert_nothing_raised {@t.raw_parse(":apt!~apt@irc6.airs.net NOTICE #せつない :7832 (株)バンダイナムコホールディングス 905 03+27(03+3.08%) 639,200 (11:00)\r\n")}
    assert_nothing_raised {@t.raw_parse(":mdomsch!~mdomsch@2001:1938:16a::2 JOIN :#fedora-meeting\r\n")}
    assert_nothing_raised {@t.raw_parse(":40FAAYSLG!~jsafrane@ip-89-103-131-92.karneval.cz JOIN :#fedora-devel\r\n")}

    @t.enable_identification(nil)
    assert_equal({
                   :prefix=>{
                     :nick=>'foo',
                     :user=>'foo',
                     :host=>'localhost'},
                   :command=>'PRIVMSG',
                   :params=>["#foo", "blahblahblah"],
                   :identified=>false,
                   :suffix=>''},
                 @t.raw_parse(":foo!foo@localhost PRIVMSG #foo :-blahblahblah\r\n"))
    assert_equal({
                   :prefix=>{
                     :nick=>'foo',
                     :user=>'foo',
                     :host=>'localhost'},
                   :command=>'PRIVMSG',
                   :params=>["#foo", "-blahblahblah"],
                   :identified=>false,
                   :suffix=>''},
                 @t.raw_parse(":foo!foo@localhost PRIVMSG #foo :--blahblahblah\r\n"))
    assert_equal({
                   :prefix=>{
                     :nick=>'foo',
                     :user=>'foo',
                     :host=>'localhost'},
                   :command=>'PRIVMSG',
                   :params=>["#foo", "blahblahblah"],
                   :identified=>true,
                   :suffix=>''},
                 @t.raw_parse(":foo!foo@localhost PRIVMSG #foo :+blahblahblah\r\n"))
  end # def test_parse

end # class TestPRUNE__Parser

if $0 == __FILE__ then
  begin
    require 'main'
  rescue LoadError
    require 'tests/main'
  end
end
