#! /usr/bin/env ruby
# prunexyd.rb - IRC Proxy daemon
# Copyright (C) 2005-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'etc'
require 'fileutils'
require 'optparse'
require 'yaml'
require 'prune/debug'
require 'prune/irc'
require 'prune/key'
require 'prune/message'
require 'prune/parser'
require 'prune/socket'
require 'prune/version'


module PRUNE

  class IRCProxy < PRUNE::IRC

    def initialize
      super

      def @state.dup
        retval = super

        [:@state].each do |v|
          retval.instance_variable_set(v, eval(sprintf("%s.dup", v)))
        end

        retval
      end # def @state.dup

      @listeninfo = nil
      @proxysigids = {}
      @connectors = {}
      @targets = {}
      @conninfo = {}
      @connq = PRUNE::Queue.new
      @lstate = {}
      @lsockets = {}
      @suffixcount = 0
      @suffixreversemap = {}
      @suffixmap = {}
      @disconntimer = 60
    end # def initialize

    attr_reader :connectors

    def open(conninfo, listeninfo)
      raise TypeError, sprintf("Can't convert %s into Array", conninfo.class) unless conninfo.kind_of?(Array)

      begin
        info("Listening on %s/%s", listeninfo.host, listeninfo.port)
        socket = PRUNE::TCPServer.new(listeninfo.host, listeninfo.port)
        @listeninfo = listeninfo
        ltarget = PRUNE::TargetKey.new("listen", listeninfo.host, listeninfo.port, self.object_id, PRUNE::TargetKey::TYPE_LISTENER)
        @sdelegate = PRUNE::ServerSocketDelegator.new(self, socket, ltarget)
      rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
        info("Failed to listen on %s/%s", listeninfo.host, listeninfo.port)
        return false
      end

      @state.nick = @listeninfo.nick
      @state.user = @listeninfo.user
      @state.name = @listeninfo.name
      @state.host = @sdelegate.addr[2]
      @value[:listener_timer] = {}
      @value[:ping] = {}
      @proxysigids[:Listen] = @eventmgr.auto_registration(@sdelegate.target, self, '_proxy_listen', PRUNE::EventManager::PRIOR_SYS_SYNC_HIGH)

      super(@sdelegate, @sdelegate.target, PRUNE::IRC::MODE_LISTEN, @listeninfo.options)
      # basically it's useless. so unregistering
      @ids[:Async].delete_if do |k, v|
        unless k.nil? then
          @eventmgr.unregister(v)
          true
        else
          false
        end
      end

      @state.listened = true
      @connq.push(*conninfo)

      until @sdelegate.closed? do
        if @state.listen? then
          unless @connq.empty? then
            ci = @connq.shift
            if !ci[0].options.has_key?('Identity') || ci[0].options['Identity'].nil? then
              ci[0].options['Identity'] = "#{@suffixcount}"
              @suffixcount += 1
            end
            suffix = sprintf("@%s", ci[0].options['Identity'])

            # FIXME: may need better lock mechanism here.
            unless @suffixmap.values.include?(suffix) then
              Thread.new(suffix, ci) do |sfx, cinfo|
                c = PRUNE::IRCConnector.new

                target = PRUNE::TargetKey.new(cinfo[0].options['Identity'], cinfo[0].host, cinfo[0].port, c.object_id)
                ids = @eventmgr.auto_registration(target, self, '_proxy_conn', PRUNE::EventManager::PRIOR_SYS_SYNC_NORMAL)
                ids[:state] = @eventmgr.auto_registration(target, self, '_proxy_conn_state', PRUNE::EventManager::PRIOR_SYS_SYNC_LOW)
                begin
                  v = c.open(cinfo, target)
                end while v == false

                Thread.exclusive do
                  @proxysigids[target.key] = ids
                  @conninfo[target.key] = cinfo
                  @connectors[target.key] = c
                  @targets[target.key] = target
                  @suffixmap[target.key] = sfx
                  @suffixreversemap[sfx] = target
                  @parser.suffix = @suffixmap.values.join('|')
                end
                Thread.exit
              end
            end
          end
          @connectors.each do |ctarget, value|
            unless @targets.has_key?(ctarget) then
              bug("Invalid target: %s", ctarget)
              raise
            end
            value.emit(@targets[ctarget].unicast, :Async, nil)
          end
          @eventmgr.emit(@sdelegate.target.unicast, :Async, nil)
          yield @eventmgr, @state, @sdelegate.target
        else
          # nothing to do
        end
        sleep 1
      end # until @sdelegate.closed?
      close
      true
    end # def open

    def close
      return false unless @state.listen?

      @eventmgr.auto_unregistration(@proxysigids)
      @proxysigids = {}
      @connectors.each do |k, v|
        info("Closing the connection `%s'", k)
        v.close
      end
      @lsockets.each do |k, v|
        info("Closing the connection `%s'", k)
        v.close
      end
      @connectors.clear
      @conninfo.clear
      @connq.clear
      @lstate.clear
      @lsockets.clear
      @suffixcount = 0
      @suffixreversemap.clear
      @suffixmap.clear

      super
    end # def close

    def is_connector?(target)
      @connectors.has_key?(target.unicast.key)
    end # def is_connector?

    def is_listener?(target)
      @sdelegate.target == target || @lstate.has_key?(target.unicast.key)
    end # def is_listener?

    def synchronize(mode = true)
      @eventmgr.synchronize(mode) do |x|
        yield self
      end
    end # def synchronize

    def emit(target, signal, subsignal, *data)
      if (signal == :Received || signal == :Sent) && data[0].kind_of?(PRUNE::Message::Core) then
        emit_message(target, signal, *data)
      else
        if is_listener?(target) then
          @eventmgr.emit(target.nil? ? @sdelegate.target.unicast : target,
                         signal, subsignal,
                         *data)
        else
          super
        end
      end
    end # def emit

    def emit_message(target, signal, message)
      raise TypeError, sprintf("Can't convert %s into PRUNE::Message::Core", message.class) unless message.kind_of?(PRUNE::Message::Core)

      state = target.nil? || target == @sdelegate.target ? @state : @lstate[target]

      if message.command =~ /[0-9]+/ then #
        message.host = state.host if message.host.nil?
      else
        if message.host.nil? then
          message.nick = state.nick if message.nick.nil?
          message.user = state.user if message.user.nil?
          message.host = state.host
        end
      end

      if is_listener?(target) then
        @eventmgr.emit(target.nil? ? @sdelegate.target.unicast : target,
                       signal, message.command,
                       message)
      else
        super
      end
    end # def emit_message

    protected

    def _proxy_conn_disconnected(ret, target, *args)
      # don't kill this thread when it finished.
      Thread.current[:keepalive] = true
      cinfo = nil
      key = target.unicast.key
      ci = @connectors[key]
      # send PART message to clients
      sfx = @suffixmap[key]
      cis = @conninfo[key]
      cinfo = ci.instance_variable_get("@conninfo").dup
      ci.state.channels.each do |ch|
        @state.leave("#{ch}#{sfx}", @state.nick)
        ret.direct_message(method(:_proxy_conn_received), target,
                           PRUNE::Message::PART.new(ch,
                                                    sprintf("Disconnected from %s:%s", cinfo.host, cinfo.port)))
      end
      flag = ci.is_reconnectable?
      @connectors.delete(key)
      @targets.delete(key)
      @eventmgr.auto_unregistration(@proxysigids[key])
      @proxysigids.delete(key)
      @suffixmap.delete(key)
      @suffixreversemap.delete(sfx)
      @conninfo.delete(key)

      if flag then
        raise RuntimeError, sprintf("No connection information available for %s", key) if cinfo.nil?
        @connq << cis
      end

      false
    end # def _proxy_conn_disconnected

    def _proxy_conn_received(ret, target, *args)
      return false if args[0].kind_of?(String)

      msg = args[0]
      key = target.unicast.key

      if self.is_connector?(target) then
        unless @suffixmap.has_key?(key) then
          bug("no suffixmap registered somehow: %s, map: %s", key, @suffixmap)
          return false
        end
        # update nick name as needed
        if msg.nick == @connectors[key].state.nick then
          msg.nick = @state.nick
        end
        # add the suffix
        msg.suffix = @suffixmap[key]
        # send the message to the master listener to take care of plugins.
        # FIXME
        ret.emit_message(@sdelegate.target.unicast, :Sent, msg)
        # delegate messages to clients
        @lstate.dup.each do |k, v|
          unless @targets.has_key?(k) then
            bug("Invalid target: %s", k)
            raise
          end
          ret.emit_message(@targets[k].unicast, :Sent, msg)
        end
      else
        bug("Got :Received signal from the unknown origin: %s", key)
      end

      false
    end # def _proxy_conn_received

    # ERR_INVALIDUSERNAME
    def _proxy_conn_468_received(ret, target, *args)
      FIXME("ERR_INVALIDUSERNAME")

      false
    end # def _proxy_conn_468_received

    # RPL_CLIENTCAPAB
    def _proxy_conn_state_290_received(ret, target, *args)
      key = target.unicast.key
      suffix = @suffixmap[key]
      msg = args[0]

      case msg.params(-1)
      when 'IDENTIFY-MSG'
        parser = @connectors[key].instance_variable_get("@parser")
        parser.enable_identification(nil)
      else
        warning("Unknown capabilities: %s", msg.params(-1))
      end

      false
    end # def _proxy_conn_state_290_received

    # RPL_CHANNELMODEIS
    def _proxy_conn_state_324_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at RPL_CHANNELMODEIS: %s", msg)
      elsif !@state.channels.include?(msg.channel({:suffix=>true})) then
        bug("Unknown channel at RPL_CHANNELMODEIS: %s, but %s", msg.channel({:suffix=>true}), @state.channels)
      else
        cs = @state.channel(msg.channel({:suffix=>true}))
        cs.clear_mode
        cs.set_mode(*msg.params[2..-1])
      end

      false
    end # def _proxy_conn_state_324_received

    # RPL_TOPIC
    def _proxy_conn_state_332_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at RPL_TOPIC: %s", msg)
      elsif !@state.channels.include?(msg.channel({:suffix=>true})) then
        bug("Unknown channel at RPL_TOPIC: %s, but %s", msg.channel({:suffix=>true}), @state.channels)
      else
        @state.channel(msg.channel({:suffix=>true})).topic = msg.params(2)
      end

      false
    end # def _proxy_conn_state_332_received

    # RPL_NAMREPLY
    def _proxy_conn_state_353_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at RPL_NAMREPLY: %s", msg)
      elsif !@state.channels.include?(msg.channel({:suffix=>true})) then
        # this may be the result of NAMES. and not relevant to you at all.
      else
        cs = @state.channel(msg.channel({:suffix=>true}))

        if msg.params(1) == '@' then
          cs.set_mode('+s')
        elsif msg.params(1) == '*' then
          cs.set_mode('+p')
        end

        cs.clear_oper
        cs.clear_voice

        msg.params(-1).split(' ').each do |n|
          nick = n.sub(/^[@+]/, '')
          unless cs.joined?(nick) then
            cs.join(nick)
          end
          if n=~ /\A@/ then #
            cs.set_oper(nick, true)
          end
          if n=~ /\A\+/ then #
            cs.set_voice(nick, true)
          end
        end
      end

      false
    end # def _proxy_conn_state_353_received

    # RPL_BANLIST
    def _proxy_conn_state_367_received(ret, target, *args)
      msg = args[0]
      mask = msg.params(2)
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at RPL_BANLIST: %s", msg)
      elsif !@state.channels.include?(msg.channel({:suffix=>true})) then
        bug("Unknown channel at RPL_BANLIST: %s, but %s", msg.channel({:suffix=>true}), @state.channels)
      else
        cs = @state.channel(msg.channel({:suffix=>true}))
        cs.set_mode('+b', mask)
      end

      false
    end # def _proxy_conn_state_367_received

    def _proxy_conn_state_join_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at JOIN: %s", msg)
      elsif msg.nick.nil? then
        bug("Empty nick name in the message at JOIN: %s", msg)
      else
        @state.join(msg.channel({:suffix=>true}), msg.nick)
      end

      false
    end # def _proxy_conn_state_join_received

    def _proxy_conn_state_kick_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at KICK: %s", msg)
      elsif !@state.channels.include?(msg.channel({:suffix=>true})) then
        bug("Unknown channel at KICK: %s, but %s", msg.channel({:suffix=>true}), @state.channels)
      elsif msg.nick.nil? then
        bug("Empty nick name in the message at KICK: %s", msg)
      else
        @state.leave(msg.channel({:suffix=>true}), msg.params(1))
      end

      false
    end # def _proxy_conn_state_kick_received

    def _proxy_conn_state_mode_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at MODE: %s", msg)
      elsif msg.channel !~ /#{PRUNE::PATTERN::CHANNEL}/ then
        if msg.channel == @state.nick then
          @state.set_mode(*msg.params[1..-1])
        else
          FIXME("MODE to nick: %s", msg.to_s)
        end
      elsif !@state.channels.include?(msg.channel({:suffix=>true})) then
        bug("Unknown channel at MODE: %s, but %s", msg.channel({:suffix=>true}), @state.channels)
      else
        cs = @state.channel(msg.channel({:suffix=>true}))
        cs.set_mode(*msg.params[1..-1])
      end

      false
    end # def _proxy_conn_state_mode_received

    def _proxy_conn_state_nick_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      @state.channels.each do |ch|
        cs = @state.channel(ch)
        if ch =~ /#{msg.suffix}\Z/ && cs.joined?(msg.nick) then
          cs.change_nick(msg.nick, msg.params(0))
        end
      end

      false
    end # def _proxy_conn_state_nick_received

    def _proxy_conn_state_part_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at PART: %s", msg)
      elsif msg.nick.nil? then
        bug("Empty nick name in the message at PART: %s", msg)
      else
        @state.leave(msg.channel({:suffix=>true}), msg.nick)
      end

      false
    end # def _proxy_conn_state_part_received

    def _proxy_conn_state_quit_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.nick.nil? then
        bug("Empty nick name in the message at QUIT: %s", msg)
      else
        @state.quit(msg.nick, msg.suffix)
      end

      false
    end # def _proxy_conn_state_quit_received

    def _proxy_conn_state_topic_received(ret, target, *args)
      msg = args[0]
      key = target.unicast.key
      msg.suffix = @suffixmap[key]

      if msg.channel.nil? then
        bug("Empty channel at TOPIC: %s", msg)
      else
        @state.channel(msg.channel({:suffix=>true})).topic = msg.params(2)
      end

      false
    end # def _proxy_conn_state_topic_received

    def _proxy_listen_ping_async(ret, target, *args)
      ctime = Time.now

      @value[:listener_timer].dup.each do |k, v|
        if ctime - v > @disconntimer then
          # must be deleted first from the list, because 'close' may takes a while.
          @value[:listener_timer].delete(k)
          begin
            @lsockets[k].close
          rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
            # just ignore it.
          end
        end
      end
      @lstate.dup.each do |k, v|
        unless @targets.has_key?(k) then
          bug("Invalid target: %s", k)
          raise
        end
        if v.loggedin? then
          t = Time.now

          unless @value.has_key?(:ping) then
            @value[:ping] = {}
          end
          unless @value[:ping].has_key?(k) then
            @value[:ping][k] = PRUNE::TYPE::PingInfoStruct.new(0, t - 301, nil)
          end
          if @value[:ping][k].pinged_time.kind_of?(Time) then
            if t - @value[:ping][k].pinged_time > 180 then
              ret.emit_message(@targets[k].unicast, :Sent,
                               PRUNE::Message::ERROR.new(sprintf("Ping timeout: %d seconds", t - @value[:ping][k].pinged_time)))
              # to avoid flooding.
              @value[:ping][k].pinged_time = Time.now
            end
          else
            if t - @value[:ping][k].timer > 300 then
              msg = PRUNE::Message::PING.new(v.nick)
              ret.emit_message(@targets[k].unicast, :Sent, msg)
              @value[:ping][k].pinged_time = t
              @value[:ping][k].signature = v.nick
            end
          end
        end
      end

      false
    end # def _proxy_listen_ping_async

    def _proxy_listen_connected(ret, target, *args)
      debug('proxy/debug', "Disabling the flood control for %s", target.key)
      args[0].floodcontrol = false
      ltarget = args[0].target
      lkey = ltarget.unicast.key

      @proxysigids[lkey] = @eventmgr.auto_registration(ltarget, self, '_proxy_listen', PRUNE::EventManager::PRIOR_SYS_SYNC_NORMAL)
      @proxysigids["#{lkey}:parent"] = @eventmgr.register(ltarget, :Received, PRUNE::EventManager::PRIOR_SYS_SYNC_HIGH, self, :_received)
      @lstate[lkey] = @state.dup
      @eventmgr.statemap[lkey] = @lstate[lkey]
      # once reset all states
      @lstate[lkey].connected = false
      @lstate[lkey].connected = true
      @lstate[lkey].authenticated = (@listeninfo.options.has_key?('Password') && !@listeninfo.options['Password'].nil? ? false : true)
      begin
        @lstate[lkey].host = args[0].peeraddr[2]
      rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
        # just ignore it. the disconnected signal will be raised later.
      end
      @value[:listener_timer][lkey] = Time.now
      @lsockets[lkey] = args[0]
      @targets[lkey] = ltarget
      sockman = PRUNE::SocketManager.instance
      # push the socket to the read queue.
      sockman.readq << args[0]

      false
    end # def _proxy_listen_connected

    def _proxy_listen_disconnected(ret, target, *args)
      # don't kill this thread when it finished.
      Thread.current[:keepalive] = true

      key = target.unicast.key
      @lstate[key].connected = false
      @lstate.delete(key)
      @value[:listener_timer].delete(key)
      @value[:ping].delete(key)
      @lsockets.delete(key)
      @targets.delete(key)
      @eventmgr.auto_unregistration(@proxysigids[key])
      @eventmgr.unregister(@proxysigids["#{key}:parent"])
      @proxysigids.delete(key)
      @proxysigids.delete("#{key}:parent")

      false
    end # def _proxy_listen_disconnected

    def _proxy_listen_lloggedin(ret, target, *args)
      starget = args[0]
      skey = starget.unicast.key

      @value[:listener_timer].delete(skey)
      @lstate[skey].loggedin = true
      q = []
      q << PRUNE::Message::RPL_WELCOME.new(@lstate[skey].nick,
                                           sprintf("Welcome to the Internet Relay Network, %s!%s@%s",
                                                   @lstate[skey].nick, @lstate[skey].user, @lstate[skey].host))
      q << PRUNE::Message::RPL_YOURHOST.new(@lstate[skey].nick,
                                            sprintf("Your host is %s, running version %s",
                                                    @lstate[skey].host, PRUNE.version))
      q << PRUNE::Message::RPL_CREATED.new(@lstate[skey].nick,
                                           sprintf("This server was created %s", PRUNE.date))
      q << PRUNE::Message::RPL_MYINFO.new(@lstate[skey].nick, @lstate[skey].host, PRUNE.dotversion,
                                          PRUNE::ModeList.availmode.split(//).sort.join,
                                          PRUNE::ChannelModeList.availmode.split(//).sort.join,
                                          nil)
      q << PRUNE::Message::RPL_MOTDSTART.new(@lstate[skey].nick,
                                             sprintf("- %s Message of the day -", @lstate[skey].host))
      q << PRUNE::Message::RPL_ENDOFMOTD.new(@lstate[skey].nick, "End of MOTD command")
      if @state.nick != @lstate[skey].nick then
        q << PRUNE::Message::NICK.new(@state.nick)
      end
      # flush the queue once to ensure the nick change.
      q.each do |m|
        # set nickname here to ensure the old nickname until changing it with NICK.
        m.nick = @lstate[skey].nick
        # XXX: dunno if there are any clients that mind to see possibly different username and hostname.
        #      but guess it may be good to keep consistency even after changing the nickname.
        m.user = @state.user
        m.host = @state.host
        ret.synchronize do |r|
          r.emit_message(starget.unicast, :Sent, m)
        end
      end
      q = []
      scraper = Proc.new do |channel, suffix|
        retval = nil
        @suffixmap.each_value do |x|
          if channel =~ /#{x}\Z/ then
            suffix << x
            retval = channel.sub(/#{x}\Z/, '')
            break
          end
        end
        retval
      end
      @state.channels.each do |ch|
        sfx = ""
        chan = scraper.call(ch, sfx)
        q << PRUNE::Message::JOIN.new(chan, nil)
        q[-1].suffix = sfx
        cs = @state.channel(ch)
        if !cs.topic.nil? && !cs.topic.empty? then
          q << PRUNE::Message::RPL_TOPIC.new(@state.nick, chan, cs.topic)
          q[-1].suffix = sfx
        end
        type = '='
        type = '@' if cs.mode(PRUNE::ChannelModeList::MODE_SECRET) == true
        type = '*' if cs.mode(PRUNE::ChannelModeList::MODE_PRIVATE) == true
        nicks = cs.opers.map {|n| "@#{n}"}
        nonopers = cs.nicks.reject {|n| cs.opers.include?(n)}
        nicks.push(*nonopers)
        unless nicks.empty? then
          q << PRUNE::Message::RPL_NAMREPLY.new(@state.nick, type, chan, nicks.join(' '))
          q[-1].suffix = sfx
        end
        q << PRUNE::Message::RPL_ENDOFNAMES.new(@state.nick, chan, "End of NAMES list")
        q[-1].suffix = sfx
        mode = cs.mode_string.sub(/\A\+/, '').split(' ')
        unless mode.empty? then
          ms = mode.shift
          ms = ms.split(//).delete_if do |x|
            if x == 'o' || x == 'v' then
              mode.shift
              true
            else
              false
            end
          end.join
          mode.unshift(ms)
        end
        q << PRUNE::Message::RPL_CHANNELMODEIS.new(@state.nick, chan, "+" << mode.join(' '), nil)
        q[-1].suffix = sfx
      end

      q.each do |m|
        ret.synchronize do |r|
          r.emit_message(starget.unicast, :Sent, m)
        end
      end

      false
    end # def _proxy_listen_lloggedin

    def _proxy_listen_reloaded(ret, target, *args)
      cc, cl = args

      cikeys = []
      reloaded = []
      cc.each do |x|
        @connectors.each do |k, v|
          unless @targets.has_key?(k) then
            bug("Invalid target: %s", k)
            raise
          end
          catch(:found_connection) do
            x.each do |ci|
              @conninfo[k].each do |y|
                if y.host == ci.host &&
                    y.port == ci.port then
                  cikeys << k
                  v.emit(@targets[k].unicast, :Reloaded, nil, x)
                  @conninfo[k] = x
                  reloaded << x
                  throw :found_connection
                end
              end
            end
          end # catch
        end # @connectors.each
      end # cc.each

      newconn = cc - reloaded
      @connq.push(*newconn) unless newconn.empty?

      @connectors.keys.reject {|v| cikeys.include?(v)}.each do |k|
        info("Server configuration for %s is no longer available. disconnecting...", k)
        @connectors[k].synchronize do |x|
          x.emit_message(k.unicast, :Sent,
                         PRUNE::Message::QUIT.new(sprintf("Prune %s", PRUNE.version)))
          x.emit_message(k.unicast, :Sent,
                         PRUNE::Message::PING.new("prune"))
        end
      end

      if cl.host != @listeninfo.host ||
        cl.port != @listeninfo.port ||
          cl.nick != @listeninfo.nick ||
          cl.user != @listeninfo.user ||
          cl.name != @listeninfo.name then
        warning("Changing the host name, the port and the account informations for listener will requires restarting.")
      end

      @listeninfo.options = cl.options
      reload(target, @listeninfo.options)

      info("Configuration file has been updated.")
      @lstate.each_key do |k|
        unless @targets.has_key?(k) then
          bug("Invalid target: %s", k)
          raise
        end
        ret.emit_message(@targets[k].unicast, :Sent,
                         PRUNE::Message::NOTICE.new(@lstate[k].nick, 'Configuration file has been updated.'))
      end

      false
    end # def _proxy_listen_reloaded

    def _proxy_listen_received(ret, target, *args)
      return false if target == @sdelegate.target
      return false unless args[0].kind_of?(PRUNE::Message::Core)

      if self.is_listener?(target) then
        msg = args[0]

        # send a message to the master listener instance to take care of plugins.
        ret.synchronize do |x|
          v = ret.emit_message(@sdelegate.target.unicast, :Received, msg, target)
          if v[0] == true then
            return true
          end
        end

        if @lstate[target.unicast.key].loggedin? then
          ctarget = @suffixreversemap[msg.suffix]
          # if only one destination is avilable, send it there without any notice.
          ctarget = @suffixreversemap.values[0] if @suffixreversemap.length == 1

          # reset prefix to have the certain prefix when forwarding.
          msg.nick = nil
          msg.user = nil
          msg.host = nil
          msg.suffix = nil

          if ctarget.nil? then
            if msg.channel.nil? then
              # broadcast a message to all servers if the message is unrelated to any channels.
              @connectors.each do |k, v|
                unless @targets.has_key?(k) then
                  bug("Invalid target: %s", k)
                  raise
                end
                v.emit_message(@targets[k].unicast, :Sent, msg)
              end
            else
              warning("detected to send a message to the unknown destination: %s, map: %s, rmap: %s", msg.inspect, @suffixmap, @suffixreversemap)
            end
          else
            @connectors[ctarget.unicast.key].emit_message(ctarget.unicast, :Sent, msg)
          end
        end

        false
      else
        bug("Got :Received signal from the unknown origin: %s", target.unicast.key)
        true
      end
    end # def _proxy_listen_received

    def _proxy_listen_nick_received(ret, target, *args)
      return false if target == @sdelegate.target

      msg = args[0]
      retval = false
      key = target.unicast.key

      unless @lstate[key].loggedin? then
        retval = true
        if msg.params.empty? || msg.params(0).nil? then
          m = PRUNE::Message::ERR_NEEDMOREPARAMS.new(sprintf("%s :Not enough parameters", msg.command))
          m.enforced_delivery = true
          ret.emit_message(target.unicast, :Sent, m)

          return true
        else
          @lstate[key].nick = msg.params(0)
        end
        unless @lstate[key].user.nil? then
          unless @lstate[key].authenticated? then
            q = []
            q << PRUNE::Message::ERR_PASSWDMISMATCH.new('Password incorrect')
            q << PRUNE::Message::ERROR.new(sprintf("Closing Link: %s!%s@%s (Bad Password)", @lstate[key].nick, @lstate[key].user, @lstate[key].host))
            q.each do |m|
              m.enforced_delivery = true
              ret.emit_message(target.unicast, :Sent, m)
            end
          else
            # Since the plugins doesn't have any way of connecting to each sessions,
            # all of instances are being connected to the master instance.
            # So the event has to be sent out there but not each sessions.
            ret.emit(@sdelegate.target.unicast, :ListenerLoggedin, nil, target)
          end
        end
      end

      retval
    end # def _proxy_listen_nick_received

    def _proxy_listen_notice_received(ret, target, *args)
      return false if target == @sdelegate.target

      @lstate.dup.each do |k, v|
        unless @targets.has_key?(k) then
          bug("Invalid target: %s", k)
          raise
        end
        if target != @targets[k] && v.loggedin? then
          ret.emit_message(@targets[k].unicast, :Sent, args[0])
        end
      end

      false
    end # def _proxy_listen_notice_received

    def _proxy_listen_ping_received(ret, target, *args)
      return false if target == @sdelegate.target

      msg = args[0]
      t = Time.now
      key = target.unicast.key

      unless @value.has_key?(:ping) then
        @value[:ping] = {}
      end
      unless @value[:ping].has_key?(key) then
        @value[:ping][key] = PRUNE::TYPE::PingInfoStruct.new(0, t - 301, nil)
      end
      if !msg.params(1).nil? && !msg.params(1).empty? then
        if msg.params(1) == @state.nick then
          ret.emit_message(target.unicast, :Sent, PRUNE::Message::PONG.new(msg.params(1), msg.params(0)))
          @value[:ping][key].pinged_time = nil
          @value[:ping][key].timer = t
          @value[:ping][key].signature = nil
          true
        else
          false
        end
      else
        ret.emit_message(target.unicast, :Sent, PRUNE::Message::PONG.new(msg.params(0)))
        @value[:ping][key].pinged_time = nil
        @value[:ping][key].timer = t
        @value[:ping][key].signature = nil
        true
      end
    end # def _proxy_listen_ping_received

    def _proxy_listen_pong_received(ret, target, *args)
      return false if target == @sdelegate.target

      key = target.unicast.key
      msg = args[0]
      msg.suffix = @suffixmap[key]

      t = Time.now

      unless @value.has_key?(:ping) then
        @value[:ping] = {}
      end
      unless @value[:ping].has_key?(key) then
        @value[:ping][key] = PRUNE::TYPE::PingInfoStruct.new(0, t - 301, nil)
      end
      if !msg.params(1).nil? && !msg.params(1).empty? then
        if msg.params(1) == @value[:ping][key].signature then
          @value[:ping][key].pinged_time = nil
          @value[:ping][key].timer = t
          @value[:ping][key].signature = nil
          true
        else
          false
        end
      else
        if msg.params(0) == @value[:ping][key].signature then
          @value[:ping][key].pinged_time = nil
          @value[:ping][key].timer = t
          @value[:ping][key].signature = nil
          true
        else
          false
        end
      end
    end # def _proxy_listen_pong_received

    def _proxy_listen_privmsg_received(ret, target, *args)
      return false if target == @sdelegate.target

      @lstate.dup.each do |k, v|
        unless @targets.has_key?(k) then
          bug("Invalid target: %s", k)
          raise
        end
        if target != @targets[k] && v.loggedin? then
          ret.emit_message(@targets[k].unicast, :Sent, args[0])
        end
      end

      false
    end # def _proxy_listen_privmsg_received

    def _proxy_listen_pass_received(ret, target, *args)
      return false if target == @sdelegate.target

      msg = args[0]
      key = target.unicast.key

      unless @lstate[key].loggedin? then
        if @listeninfo.options['Password'].nil? then
          debug('proxy/listener', "No Password section registered. just authenticate the connection from %s", key)
          @lstate[key].authenticated = true
        else
          if msg.params.empty? then
            m = PRUNE::Message::ERR_NEEDMOREPARAMS.new(sprintf("%s :Not enough parameters", msg.command))
            m.enforced_delivery = true
            ret.emit_message(target.unicast, :Sent, m)

            return true
          elsif @lstate[key].authenticated? then
            m = PRUNE::Message::ERR_ALREADYREGISTERED.new('Already registered')
            m.enforced_delivery = true
            ret.emit_message(target.unicast, :Sent, m)

            return true
          elsif msg.params(0) == @listeninfo.options['Password'] then
            debug('proxy/listener', "authenticated the connection from %s", key)
            @lstate[key].authenticated = true
          end
        end
      else
        ret.emit_message(target.unicast, :Sent,
                         PRUNE::Message::ERR_ALREADYREGISTERED.new('Already registered'))
        return true
      end

      false
    end # def _proxy_listen_pass_received

    def _proxy_listen_quit_received(ret, target, *args)
      if target == @sdelegate.target then
        bug("Server socket received QUIT message")
      else
        @value[:listener_timer][target.unicast.key] = Time.now - @disconntimer
      end

      true
    end # def _proxy_listen_quit_received

    def _proxy_listen_user_received(ret, target, *args)
      return false if target == @sdelegate.target

      msg = args[0]
      key = target.unicast.key

      unless @lstate[key].loggedin? then
        retval = true
        if msg.params.length < 4 then
          m = PRUNE::Message::ERR_NEEDMOREPARAMS.new(sprintf("%s :Not enough parameters", msg.command))
          m.enforced_delivery = true
          ret.emit_message(target.unicast, :Sent, m)

          return true
        end
        if @lstate[key].user.nil? then
          @lstate[key].user = msg.params(0)
          FIXME("user mode: %s", msg.params(1))
          @lstate[key].name = msg.params(3)
        end
        unless @lstate[key].nick.nil? then
          unless @lstate[key].authenticated? then
            q = []
            q << PRUNE::Message::ERR_PASSWDMISMATCH.new('Password incorrect')
            q << PRUNE::Message::ERROR.new(sprintf("Closing Link: %s!%s@%s (Bad Password)", @lstate[key].nick, @lstate[key].user, @lstate[key].host))
            q.each do |m|
              m.enforced_delivery = true
              ret.emit_message(target.unicast, :Sent, m)
            end
          else
            # Since the plugins doesn't have any way of connecting to each sessions,
            # all of instances are being connected to the master instance.
            # So the event has to be sent out there but not each sessions.
            ret.emit(@sdelegate.target.unicast, :ListenerLoggedin, nil, target)
          end
        end
      else
        ret.emit_message(target.unicast, :Sent,
                         PRUNE::Message::ERR_ALREADYREGISTERED.new('Already registered'))
      end

      true
    end # def _proxy_listen_user_received

    def _proxy_listen_sent(ret, target, *args)
      return false if target == @sdelegate.target

      msg = args[0]
      key = target.unicast.key

      return false if !@lstate[key].loggedin? && !msg.is_enforced_delivery?
      # get rid of the suffix if only one connection is established.
      # though this may behaves wrongly at the client when you add new
      # connection on the fly.
      msg.suffix = nil if @suffixmap.length == 1

      convert_from_utf8(msg) do |m|
        begin
          if target.is_broadcasted? then
            @lsockets.each do |k, v|
              v.puts(m.to_s)
            end
          elsif @lsockets[key].nil? then
            bug("No such socket available: %s", key)
          else
            @lsockets[key].puts(m.to_s)
          end
        rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
          ret.emit(target.unicast, :Disconnected, nil, @lsockets[key])
        end
      end

      false
    end # def _proxy_listen_sent

    def _proxy_listen_error_sent(ret, target, *args)
      return false if target == @sdelegate.target

      @value[:listener_timer][target.unicast.key] = Time.now - @disconntimer

      false
    end # def _proxy_listen_error_sent

    def _proxy_listen_nick_sent(ret, target, *args)
      return false if target == @sdelegate.target

      msg = args[0]
      key = target.unicast.key

      if msg.nick == @lstate[key].nick then
        @lstate[key].nick = msg.params(0)
      end

      false
    end # def _proxy_listen_nick_sent

  end # class IRCProxy

  class ProxyConfig
    include PRUNE::Debug

    ConfigInfoStruct = Struct.new(:filename, :mtime, :size, :data)

    UPDATE_MTIME = 0
    UPDATE_SIZE = 1
    UPDATE_MANUAL = 2

    def initialize
      @debug = false
      @daemonize = true
      @kill = false
      @config = nil
      @updatemode = UPDATE_MTIME
    end # def initialize

    attr_accessor :updatemode, :kill

    def debug=(flag)
      $DEBUG = flag
      @debug = flag

      Thread.abort_on_exception = (flag == true)
      PRUNE::Parser.ignore_invalid_message = (flag == true)
    end # def debug=

    def is_debug_enabled?
      @debug
    end # def is_debug_enabled?

    def daemonize=(flag)
      @daemonize = flag == true
    end # def daemonize=

    def is_daemonized?
      @daemonize
    end # def is_daemonized?


    def load(file)
      @config = PRUNE::ProxyConfig::ConfigInfoStruct.new(file)

      unless File.exist?(@config.filename) then
        @config = nil
        raise ArgumentError, sprintf("No such configuration file available: %s", file)
      else
        @config.mtime = File.mtime(file)
        @config.size = File.size(file)
        File.open(file) do |f|
          @config.data = YAML.load(f.read)
        end
      end
    end # def load

    def reload
      self.load(@config.filename)
      yield self.get_connectors_config, self.get_listener_config
    end # def reload

    def is_updated?
      if @updatemode == UPDATE_MTIME then
        mtime = nil
        begin
          mtime = File.mtime(@config.filename)
        rescue => e
          warning("%s", e.message)
        end
        if !mtime.nil? && mtime > @config.mtime then
          info("detected the configuration file `%s' updated (from the updated mtime)", @config.filename)
          true
        else
          false
        end
      elsif @updatemode == UPDATE_SIZE then
        size = nil
        begin
          size = File.size(@config.filename)
        rescue => e
          warning("%s", e.message)
        end
        if !size.nil? && size != @config.size then
          info("detected the configuration file `%s' updated (from the different file size)", @config.filename)
          true
        else
          false
        end
      else
        false
      end
    end # def is_updated?

    def get_connectors_config
      retval = []

      if @config.nil? then
        raise RuntimeError, "Must load the configuration file first."
      end
      pwent = Etc.getpwuid
      server_list.each do |k|
        next if @config.data[k].has_key?('Listen') && @config.data[k]['Listen'] == true

        args = []
        unless @config.data[k].has_key?('Nick') then
          raise RuntimeError, sprintf("No nickname defined in Nick section for the connector `%s'", k)
        end
        args << @config.data[k]['Nick']
        unless @config.data[k].has_key?('User') then
          user = pwent.name
          if @config.data[k].has_key?('UseCryptedName') &&
              @config.data[k]['UseCryptedName'] then
            user = user.crypt(user.object_id)
          end
        else
          user = @config.data[k]['User']
        end
        args << user
        unless @config.data[k].has_key?('Name') then
          n = pwent.gecos.sub(/,.*/, '')
          if n.nil? || n.empty? then
            name = "Prune User"
          else
            name = n
          end
        else
          name = @config.data[k]['Name']
        end
        args << name
        args << @config.data[k]
        args << @config.data[k]['Identity'] if @config.data[k].has_key?('Identity')

        data = []
        @config.data['ServerList'][k].each do |n|
          host, ports = n.split('/')
          ports.split(',').each do |port|
            data << PRUNE::TYPE::HostInfoStruct.new(host, port.to_i, *args)
          end
        end
        if data.empty? then
          raise RuntimeError, sprintf("No host/port information for `%s'", k)
        end

        retval << data
      end

      retval
    end # def get_connectors_config

    def get_listener_config
      data = nil
      name = nil

      if @config.nil? then
        raise RuntimeError, "Must load a configuration file first."
      end
      server_list.each do |k|
        if @config.data[k].has_key?('Listen') && @config.data[k]['Listen'] == true then
          name = k
          data = @config.data[k]
          break
        end
      end
      if data.nil? then
        raise RuntimeError, sprintf("No Listener configuration available in `%s'", @config.filename)
      end

      n = @config.data['ServerList'][name][0]
      host, port = n.split('/')
      args = [host, port.to_i]

      unless @config.data[name].has_key?('Nick') then
        raise RuntimeError, sprintf("No nickname defined in Nick section for the listener.")
      else
        args << @config.data[name]['Nick']
      end
      pwent = Etc.getpwuid
      unless @config.data[name].has_key?('User') then
        user = pwent.name
        args << user.crypt(user.object_id.to_s)
      else
        args << @config.data[name]['User']
      end
      unless @config.data[name].has_key?('Name') then
        n = pwent.gecos.sub(/,.*/, '') 
        if n.nil? || n.empty? then
          args << 'Prune User'
        else
          args << n
        end
      else
        args << @config.data[name]['Name']
      end
      args << @config.data[name]

      return PRUNE::TYPE::HostInfoStruct.new(*args)
    end # def get_listener_config

    private

    def server_list
      retval = []

      if @config.nil? then
        raise RuntimeError, "Must load the configuration file first."
      end

      if @config.data.has_key?('ServerList') then
        @config.data['ServerList'].each_key do |k|
          if @config.data.has_key?(k) then
            retval << k
          end
        end
      end

      retval
    end # def server_list

  end # class ProxyConfig

end # module PRUNE

if $0 == __FILE__ then
  smgr = PRUNE::SocketManager.instance
  if ENV.has_key?('PRUNE_DEBUG') then
    ENV['PRUNE_DEBUG'].split(',').each do |v|
      smgr.category_list.add(v)
    end
  end

  conf = PRUNE::ProxyConfig.new

  begin
    ARGV.options do |opt|
      opt.on('--config=FILE', 'read FILE as the configuration file.') {|v| conf.load(v)}
      opt.on('-d', '--debug', 'turn on the debugging mode') {|v| conf.debug = true}
      opt.on('--[no-]daemon', 'daemonize the process') {|v| conf.daemonize = v}
      opt.on('-k', '--kill', 'kill the running process.') {|v| conf.kill = true}

      opt.parse!
    end
  rescue => e
    p e
    exit
  end

  path = File.join(ENV['HOME'], '.prune')
  unless File.exist?(path) then
    FileUtils.mkdir(path)
  end
  def PRUNE.fixme_output(msg)
    path = File.join(ENV['HOME'], '.prune', Time.now.strftime("prune_fixme_%Y%m%d.log"))
    File.open(path, "a") do |x|
      x.write(sprintf("%s\n", msg))
    end
  end # def 
  def PRUNE.bug_output(msg)
    path = File.join(ENV['HOME'], '.prune', Time.now.strftime("prune_bug_%Y%m%d.log"))
    File.open(path, "a") do |x|
      x.write(sprintf("%s\n  %s\n", msg, caller.join("\n  ")))
    end
  end # def 
  def PRUNE.warning_output(msg)
    path = File.join(ENV['HOME'], '.prune', Time.now.strftime("prune_warning_%Y%m%d.log"))
    File.open(path, "a") do |x|
      x.write(sprintf("%s\n", msg))
    end
  end # def 

  cl = conf.get_listener_config
  cc = conf.get_connectors_config

  if cc.empty? || cl.nil? then
    printf("No connection informations available\n");
    exit
  end

  pidfile = File.join(ENV['HOME'], '.prune', sprintf("%s.pid", cl.port))
  if File.exist?(pidfile) then
    pid = File.open(pidfile).read.to_i
    unless pid > 1 then
      printf("Invalid pidfile detected. ignoring...\n")
    else
      begin
        Process.kill(0, pid)
        if conf.kill then
          Process.kill("TERM", pid)
          begin
            Process.wait(pid)
          rescue Errno::ECHILD
          end
          printf("Pid %d has been terminated successfully.\n", pid)
          File.delete(pidfile)
        else
          printf("The process has already been running for %s/%s\n", cl.host, cl.port)
        end
        exit
      rescue Errno::ESRCH
        printf("detected the garbage of the pidfile. ignoring...\n")
      end
    end
  elsif conf.kill then
    printf("No processes running for %s/%s.\n", cl.host, cl.port)
    exit
  end

  if conf.is_daemonized? then
    exit! if fork
    Process.setsid
    pid = fork
    if pid then
      File.open(pidfile, "w") do |f|
        f.write(pid)
      end
      exit!
    end
    STDIN.reopen('/dev/null')
    STDOUT.reopen('/dev/null', 'w')
    STDERR.reopen('/dev/null', 'w')
  end

  irc = PRUNE::IRCProxy.new

  trap("SIGINT") do
    class ErrorSIGINT < StandardError; end

    printf("%s\n", caller.join("\n"))
    exit! if Thread.current[:sigtrap]
    Thread.current[:sigtrap] = true

    irc.synchronize do |x|
      x.connectors.each do |k, v|
        v.reconnectable = false
        v.emit_message(nil, :Sent, PRUNE::Message::QUIT.new(sprintf("Prune %s", PRUNE.version)))
        v.emit_message(nil, :Sent, PRUNE::Message::PING.new("prune"))
      end
    end
    irc.close
    printf("Successfully closed.\n")
    exit!
  end
  trap("SIGTERM") do
    exit! if Thread.current[:sigtrap]
    Thread.current[:sigtrap] = true
    irc.synchronize do |x|
      x.connectors.each do |k, v|
        v.reconnectable = false
        v.emit_message(nil, :Sent, PRUNE::Message::QUIT.new(sprintf("Prune %s", PRUNE.version)))
        v.emit_message(nil, :Sent, PRUNE::Message::PING.new("prune"))
      end
    end
    irc.close
    printf("Successfully closed.\n")
    exit!
  end
  trap("SIGUSR1") do
    pm = PRUNE::PluginManager.instance
    pm.update_plugins(PRUNE::PluginManager::UPDATE_MANUAL)
  end

  irc.open(cc, cl) do |ev, stat, target|
    if conf.is_updated? then
      conf.reload do |cc, cl|
        ev.emit(target.unicast, :Reloaded, nil, cc, cl)
      end
    end
  end

  File.delete(pidfile) if File.exist?(pidfile)
end
