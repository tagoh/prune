# queue.rb
# Copyright (C) 2006-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'thread'
require 'prune/error'


module PRUNE

=begin rdoc

== PRUNE::Queue

=end

  class Queue

=begin rdoc

=== PRUNE::Queue#new(*val)

=end

    def initialize(*val)
      @queue = val
      @mutex = Mutex.new
    end # def initialize

=begin rdoc

=== PRUNE::Queue#inspect

=end

    def inspect
      @queue.inspect
    end # def inspect

=begin rdoc

=== PRUNE::Queue#to_a

=end

    def to_a
      @queue.dup
    end # def to_a

    def method_missing(*args, &block)
      retval = nil

      @mutex.synchronize do
        retval = @queue.__send__(*args, &block)
      end

      if retval == @queue then
        self
      else
        case args[0].to_sym
        when :clear, :collect, :compact, :flatten, :map, :reject, :reverse, :shuffle, :slice, :sort, :uniq
          PRUNE::Queue.new(*retval)
        else
          retval
        end
      end
    end # def method_missing

  end # class Queue

end # module PRUNE
