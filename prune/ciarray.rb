# ciarray.rb - Case Insensitive Array class
# Copyright (C) 2005-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'delegate'
require 'prune/compat'


module PRUNE

=begin rdoc

== PRUNE::CiArray

PRUNE::CiArray class is the Array class extended to deal with
the value case-insensitively.  you can use this class as well
as Array class, except some methods extended to take care of
the case-insensitive values.

=end

  class CiArray < DelegateClass(Array)

=begin rdoc

=== PRUNE::CiArray#new

Returns a new array. any of ways to create an Array instance is available.
but it will create PRUNE::CiArray instance.

=end

    def initialize(*args)
      super(Array.new(*args))
    end # def initialize

=begin rdoc

=== PRUNE::CiArray#include?(val)

Works similarly as +Array::include?+ but comparing the value case-insensitively.

=end

    def include?(val)
      __getobj__.map {|n| (n.downcase rescue n)}.include?((val.downcase rescue val))
    end # def include?

    alias :member? :include?

=begin rdoc

=== PRUNE::CiArray#delete(val)

Works similarly as +Array::delete+ but comparing the value case-insensitively.

=end

    def delete(val)
      __getobj__.delete_if do |v|
        (v.downcase rescue v) == (val.downcase rescue val)
      end
    end # def delete

  end # class CiArray

end # module PRUNE
