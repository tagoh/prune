# charset.rb
# Copyright (C) 2009-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'iconv'
require 'prune/cihash'
require 'prune/error'


module PRUNE

=begin rdoc

== PRUNE::Charset

PRUNE::Charset class provides facilities to easy-access for encodings/character sets.
This class is supposed to be used per the connection or so. you can set
a default encoding for the connection, being possible to set different
encodings for some channels say.

=end

  class Charset < PRUNE::CiHash

=begin rdoc

=== PRUNE::Charset#default=(val)

Sets the default encoding. _val_ has to be the encoding name available in +Iconv+ class.

=end

    def default=(val)
      validate_encoding(val)
      super
    end # def default=

=begin rdoc

=== PRUNE::Charset#[]=(key, val)

Sets _val_ as the encoding for _key_. _val_ has to be the encoding name available in +Iconv+ class.

=end

    def []=(key, val)
      validate_encoding(val)
      super
    end # def []=

    private

    def validate_encoding(value)
      begin
        Iconv.conv(value, value, "foo")
      rescue Errno::EINVAL, Iconv::InvalidEncoding
        PRUNE.Fail(PRUNE::Error::InvalidCharset, value)
      end
    end # def validate_encoding

  end # class Charset

end # module PRUNE
