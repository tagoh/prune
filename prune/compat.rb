# compat.rb - small wrapper to keep up backward-compatibility.
# Copyright (C) 2009-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

# no need to generate documents.
# :enddoc:

class Symbol

  unless Symbol.public_methods.include?(:downcase) then

    module_eval <<-EOS, __FILE__, __LINE__+1
    def downcase
      self.to_s.downcase.to_sym
    end # def downcase
    EOS
    
  end

end # class Symbol

class Fixnum

  unless Fixnum.public_methods.include?(:to_sym) then

    module_eval <<-EOS, __FILE__, __LINE__+1
    def to_sym
      nil
    end # def to_sym
    EOS
    
  end

end # class Fixnum

class Regexp

  class << self

    def unescape(string)
      string.gsub(/\\([\[\]\{\}\(\)\|\-\*\.\\\?\+\^\$\# fnrt])/) do |s|
        i = $1
        i =~ /[fnrt]/ ? eval('"' + "\\#{i}" + '"') : i
      end
    end # def unescape

  end

end # class Regexp
