# types.rb
# Copyright (C) 2007-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.


module PRUNE

=begin rdoc

== PRUNE::TYPE

=end

  module TYPE

    # composer.rb
    ComposeInfoStruct = Struct.new('ComposeInfoStruct', :composetype, :instance, :method, :args)
    # event.rb
    EventQueueStruct = Struct.new('EventQueueStruct', :calltrace, :target, :signal, :subsignal, :data, :synchronous)
    DispatchStruct = Struct.new('DispatchStruct', :target, :handler_id, :signal, :priority, :instance, :function, :args)
    # irc.rb
    HostInfoStruct = Struct.new('HostInfoStruct', :host, :port, :nick, :user, :name, :options, :suffix)
    PingInfoStruct = Struct.new('PingInfoStruct', :pinged_time, :timer, :signature)
    # message.rb
    ChannelLocationInfo = Struct.new('ChannelLocationInfo', :location, :index)
    # plugin.rb
    PluginInfoStruct = Struct.new('PluginInfoStruct', :filename, :target, :channel_options, :plugin_options, :instance, :interface, :mtime, :size)
    # socket.rb
    SendMessageStruct = Struct.new('SendMessageStruct', :string, :thread)
    # state.rb
    NickInfoStruct = Struct.new('NickInfoStruct', :nick, :oper, :voice)

    CommandStruct = Struct.new('CommandStruct', :nick, :user, :host, :command, :params, :time, :suffix)

  end # module TYPE

end # module PRUNE
