#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <unistd.h>
#include <fcntl.h>
/* prevent warnings about redefining of gettext things */
#ifdef PACKAGE_NAME
#define backup_PACKAGE_NAME PACKAGE_NAME
#undef PACKAGE_NAME
#endif
#ifdef PACKAGE_TARNAME
#define backup_PACKAGE_TARNAME PACKAGE_TARNAME
#undef PACKAGE_TARNAME
#endif
#ifdef PACKAGE_VERSION
#define backup_PACKAGE_VERSION PACKAGE_VERSION
#undef PACKAGE_VERSION
#endif
#ifdef PACKAGE_STRING
#define backup_PACKAGE_STRING PACKAGE_STRING
#undef PACKAGE_STRING
#endif
#ifdef PACKAGE_BUGREPORT
#define backup_PACKAGE_BUGREPORT PACKAGE_BUGREPORT
#undef PACKAGE_BUGREPORT
#endif

#ifdef HAVE_RUBY_H
#include <ruby.h>
#endif
#ifdef HAVE_RUBYIO_H
#include <rubyio.h>
#endif

/* restore gettext related definitions */
#ifdef backup_PACKAGE_NAME
#undef PACKAGE_NAME
#define PACKAGE_NAME backup_PACKAGE_NAME
#undef backup_PACKAGE_NAME
#endif
#ifdef backup_PACKAGE_TARNAME
#undef PACKAGE_TARNAME
#define PACKAGE_TARNAME backup_PACKAGE_TARNAME
#undef backup_PACKAGE_TARNAME
#endif
#ifdef backup_PACKAGE_VERSION
#undef PACKAGE_VERSION
#define PACKAGE_VERSION backup_PACKAGE_VERSION
#undef backup_PACKAGE_VERSION
#endif
#ifdef backup_PACKAGE_STRING
#undef PACKAGE_STRING
#define PACKAGE_STRING backup_PACKAGE_STRING
#undef backup_PACKAGE_STRING
#endif
#ifdef backup_PACKAGE_BUGREPORT
#undef PACKAGE_BUGREPORT
#define PACKAGE_BUGREPORT backup_PACKAGE_BUGREPORT
#undef backup_PACKAGE_BUGREPORT
#endif

enum IOType {
	TYPE_IO,
	TYPE_STRINGIO,
	TYPE_ARRAY
};

static int
pseudoio_create(VALUE  self,
		int    mode)
{
	VALUE tmpfile = rb_eval_string("File.join(Dir.tmpdir, \"pseudoioXXXXXXXX\")");
	VALUE io, type;
	char *ctmpfile;
	int fd;
#ifdef HAVE_RUBYIO_H
	OpenFile *rfp;
#endif

	if (mode & FMODE_READABLE) {
		io = rb_iv_get(self, "__input_obj__");
	} else if (mode & FMODE_WRITABLE) {
		io = rb_iv_get(self, "__output_obj__");
	} else {
		rb_raise(rb_eArgError, "creating IO for neither of reading or writing");
		/* shouldn't be reached */
		return -1;
	}
	type = rb_iv_get(self, "__type__");
	if (FIX2INT (type) == TYPE_IO) {
		GetOpenFile(io, rfp);
		return fileno(rfp->f);
	}
	SafeStringValue(tmpfile);
	ctmpfile = StringValueCStr(tmpfile);
	if ((fd = mkstemp(ctmpfile)) < 0) {
		rb_raise(rb_eIOError, "Unable to create pseudo IO.");
		/* shouldn't be reached */
		return -1;
	}
	if (unlink(ctmpfile) < 0) {
		rb_raise(rb_eIOError, "Unable to create pseudo IO.");
		/* shouldn't be reached */
		return -1;
	}

	return fd;
}

static int
pseudoio_read(VALUE self,
	      int   fd,
	      VALUE io,
	      VALUE type)
{
	FILE *fp;
	VALUE val, t;
	char *cval;
	VALUE mode;
	int retval;

	if (FIX2INT (type) == TYPE_IO) {
		/* nothing to do here */
		return fd;
	}
	mode = rb_iv_get(self, "__io_mode__");
	if (!(FIX2INT (mode) & FMODE_READABLE)) {
		rb_raise(rb_eIOError, "not opened for reading");
		/* shouldn't be reached */
		return -1;
	}
	retval = dup(fd);
	if ((fp = rb_fdopen(retval, "w")) == NULL) {
		rb_raise(rb_eIOError, "Unable to create pseudo IO.");
		/* shouldn't be reached */
		return -1;
	}
	switch (FIX2INT (type)) {
	    case TYPE_ARRAY:
		    while (RARRAY_LEN (io) > 0) {
			    t = rb_ary_shift(io);
			    if (t == Qnil) {
				    /* ignore */
				    continue;
			    }
			    val = rb_convert_type(t, T_STRING, "String", "to_s");
			    SafeStringValue(val);
			    cval = StringValueCStr(val);
			    fprintf(fp, "%s\n", cval);
		    }
		    break;
	    case TYPE_STRINGIO:
		    while (rb_funcall(io, rb_intern("eof?"), 0) != Qtrue) {
			    val = rb_funcall(io, rb_intern("readline"), 0);
			    SafeStringValue(val);
			    cval = StringValueCStr(val);
			    fprintf(fp, "%s", cval);
		    }
		    break;
	    default:
		    rb_raise(rb_eTypeError, "neither IO nor Array");
		    /* shouldn't be reached */
		    return -1;
	}
	fseek(fp, 0, SEEK_SET);
	fclose(fp);

	return fd;
}

static void
pseudoio_write(VALUE  self,
	       FILE  *fp,
	       VALUE  io,
	       VALUE  type)
{
	VALUE mode, val;
	struct flock lock;
	fpos_t pos;
	int newfd, oldfd;
	FILE *tfp;
	char buffer[256];

	mode = rb_iv_get(self, "__io_mode__");
	if (!(FIX2INT (mode) & FMODE_WRITABLE)) {
		rb_raise(rb_eIOError, "not opened for writing");
		/* shouldn't be reached */
		return;
	}
	fgetpos(fp, &pos);
	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 0;
	oldfd = fileno(fp);
	fcntl(oldfd, F_SETLKW, &lock);
	fseek(fp, 0, SEEK_SET);
	newfd = dup(oldfd);
	tfp = rb_fdopen(newfd, "r");
	if (tfp == NULL) {
		rb_raise(rb_eIOError, "Unable to write data into PseudoIO.");
		return;
	}
	while (!feof(tfp)) {
		if (fgets(buffer, 255, tfp) == NULL)
			break;
		val = rb_str_new2(buffer);
		switch (FIX2INT (type)) {
		    case TYPE_IO:
			    rb_io_write(io, val);
			    break;
		    case TYPE_ARRAY:
			    rb_ary_push(io, val);
			    break;
		    case TYPE_STRINGIO:
			    rb_funcall(io, rb_intern("write"), 1, val);
			    break;
		    default:
			    rb_raise(rb_eTypeError, "neither IO nor Array");
			    break;
		}
	}
	fclose(tfp);
	fcntl(oldfd, F_UNLCK);
	fsetpos(fp, &pos);
}

static void
pseudoio_init(VALUE self)
{
	int ifd, ofd;
	VALUE type = rb_iv_get(self, "__type__");
	VALUE mode = rb_iv_get(self, "__io_mode__");
	VALUE obj = rb_iv_get(self, "__input_obj__");
#ifdef HAVE_RUBYIO_H
	OpenFile *fptr;
#endif

	ifd = pseudoio_create(self, FMODE_READABLE);
	ofd = pseudoio_create(self, FMODE_WRITABLE);
	if (FIX2INT (mode) & FMODE_READABLE) {
		ifd = pseudoio_read(self, ifd, obj, type);
	}
	MakeOpenFile(self, fptr);
	if ((FIX2INT (mode) & FMODE_READWRITE) == FMODE_READWRITE) {
		fptr->f = rb_fdopen(ifd, "r");
		fptr->f2 = rb_fdopen(ofd, "w");
	} else if (FIX2INT (mode) & FMODE_READABLE) {
		fptr->f = rb_fdopen(ifd, "r");
		fptr->f2 = NULL;
	} else if (FIX2INT (mode) & FMODE_WRITABLE) {
		fptr->f = rb_fdopen(ofd, "w");
		fptr->f2 = NULL;
	} else {
		rb_raise(rb_eIOError, "Unknown I/O type.");
	}
	fptr->mode = FIX2INT (mode);
}

static VALUE
pseudoio_call_close(VALUE self)
{
	return rb_funcall(self, rb_intern("close"), 0, 0);
}

static VALUE
pseudoio_close(VALUE self)
{
	return rb_rescue(pseudoio_call_close, self, 0, 0);
}

static VALUE
rb_pseudoio_s_open(int    argc,
		    VALUE *argv,
		    VALUE  klass)
{
	VALUE self = rb_class_new_instance(argc, argv, klass);

	if (rb_block_given_p()) {
		return rb_ensure(rb_yield, self, pseudoio_close, self);
	}

	return self;
}

static VALUE
rb_pseudoio_flush(VALUE self)
{
	VALUE mode = rb_iv_get(self, "__io_mode__");
	VALUE type = rb_iv_get(self, "__type__");
	VALUE io = rb_iv_get(self, "__output_obj__");
	FILE *fp;
#if defined(HAVE_RUBYIO_H)
	OpenFile *rfp;
#else
#error "This class won't work due to no way of initializing IO"
#endif

	if (FIX2INT (type) == TYPE_IO) {
		/* nothing to do here */
		return Qfalse;
	}
	GetOpenFile(self, rfp);
	if ((FIX2INT (mode) & FMODE_READWRITE) == FMODE_READWRITE) {
		fp = rfp->f2;
	} else {
		fp = rfp->f;
	}
	if (FIX2INT (mode) & FMODE_WRITABLE) {
		pseudoio_write(self, fp, io, type);
	}
	pseudoio_init(self);

	return self;
}

static VALUE
rb_pseudoio_initialize(VALUE obj,
			VALUE io,
			VALUE mode)
{
	char *classname = rb_obj_classname(io);
	int flags;
	VALUE input, output, type;

	if (FIXNUM_P (mode)) {
		flags = FIX2LONG (mode);
	} else {
		SafeStringValue(mode);
		flags = rb_io_mode_flags(StringValueCStr(mode));
	}
	rb_iv_set(obj, "__io_mode__", INT2FIX (flags));
	if (rb_obj_is_kind_of(io, rb_cIO)) {
		rb_iv_set(obj, "__type__", (type = INT2FIX (TYPE_IO)));
		input = output = io;
	} else if (rb_obj_is_kind_of(io, rb_cArray)) {
		rb_iv_set(obj, "__type__", (type = INT2FIX (TYPE_ARRAY)));
		if ((flags & FMODE_READWRITE) == FMODE_READWRITE) {
			if (RARRAY_LEN (io) != 2) {
				rb_raise(rb_eArgError, "No I/O queues in Array [%d of 2]", RARRAY_LEN (io));
				/* shouldn't be reached */
				return Qnil;
			} else {
				char *c1, *c2;

				c1 = rb_obj_classname(RARRAY_PTR (io)[0]);
				c2 = rb_obj_classname(RARRAY_PTR (io)[1]);
				if (rb_obj_is_kind_of(RARRAY_PTR (io)[0], rb_cIO) &&
				    rb_obj_is_kind_of(RARRAY_PTR (io)[1], rb_cIO)) {
					rb_iv_set(obj, "__type__", (type = INT2FIX (TYPE_IO)));
				} else if (strcmp(c1, "StringIO") == 0 &&
				    strcmp(c2, "StringIO") == 0) {
					rb_iv_set(obj, "__type__", (type = INT2FIX (TYPE_STRINGIO)));
				} else if (!rb_obj_is_kind_of(RARRAY_PTR (io)[0], rb_cArray) ||
					   !rb_obj_is_kind_of(RARRAY_PTR (io)[1], rb_cArray)) {
					rb_raise(rb_eArgError, "I/O queues aren't Array");
					/* shouldn't be reached */
					return Qnil;
				}
			}
			input = RARRAY_PTR (io)[0];
			output = RARRAY_PTR (io)[1];
		} else {
			input = output = io;
		}
	} else if (strcmp(classname, "StringIO") == 0) {
		if ((flags & FMODE_READWRITE) == FMODE_READWRITE) {
			rb_raise(rb_eArgError, "need both StringIO for Input and Output");
			/* shouldn't be reached */
			return Qnil;
		}
		rb_iv_set(obj, "__type__", (type = INT2FIX (TYPE_STRINGIO)));
		input = output = io;
	} else {
		rb_raise(rb_eTypeError, "neither IO nor Array");
	}
	rb_iv_set(obj, "__input_obj__", input);
	rb_iv_set(obj, "__output_obj__", output);
	pseudoio_init(obj);

	return obj;
}

void
Init_pseudoio(void)
{
	VALUE mPRUNE, cPseudoIO;

	mPRUNE = rb_define_module("PRUNE");
	cPseudoIO = rb_define_class_under(mPRUNE, "PseudoIO", rb_cIO);
	rb_define_method(cPseudoIO, "initialize", rb_pseudoio_initialize, 2);
	rb_define_singleton_method(cPseudoIO, "open", rb_pseudoio_s_open, -1);
	rb_define_method(cPseudoIO, "flush", rb_pseudoio_flush, 0);
}
