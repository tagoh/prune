# key.rb
# Copyright (C) 2009-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'prune/pattern'


module PRUNE

  class TargetKey
    TYPE_CONNECTOR = 0
    TYPE_LISTENER = 1

    class << self

      def parse(string)
        broadcast = false
        if string =~ /:\[broadcast\]\Z/ then
          broadcast = true
          string.sub!(/:\[broadcast\]\Z/, '')
        end
        if string =~ /\A([^:]+):(#{PATTERN::SHORTNAME}(?:\.#{PATTERN::SHORTNAME}|\.)*):(\d+):([0-9a-f]+)/ then
          id = $1
          host = $2
          port = $3.to_i
          uid = $4.hex
          type = string =~ /:\[c\]\Z/ ? PRUNE::TargetKey::TYPE_CONNECTOR : PRUNE::TargetKey::TYPE_LISTENER
          PRUNE::TargetKey.new(id, host, port, uid, type, broadcast)
        else
          raise ArgumentError, sprintf("Invalid syntax: %s", string)
        end
      end # def parse

    end

    def initialize(identity, host, port, unique_id, type = TYPE_CONNECTOR, broadcast = false)
      raise TypeError, sprintf("Can't convert %s into String", identity.class) unless identity.kind_of?(String)
      raise TypeError, sprintf("Can't convert %s into String", host.class) unless host.kind_of?(String)
      raise TypeError, sprintf("Can't convert %s into Integer", port.class) unless port.kind_of?(Integer)
      raise TypeError, sprintf("Can't convert %s into Integer", unique_id.class) unless unique_id.kind_of?(Integer)

      @host = host
      @port = port
      @identity = identity
      @unique_id = unique_id
      @type = type
      @broadcast = broadcast
      @time = Time.now
    end # def initialize

    attr_reader :host, :port, :time, :identity

    def to_s
      sprintf("%s:%s:%s:%x%s%s", @identity, @host, @port, @unique_id,
              @type == TYPE_CONNECTOR ? ":[c]" : "",
              @broadcast ? ":[broadcast]" : "")
    end # def to_s

    alias :key :to_s

    def is_connector?
      @type == TYPE_CONNECTOR
    end # def is_connector?

    def is_broadcasted?
      @broadcast == true
    end # def is_broadcasted?

    def broadcast
      PRUNE::TargetKey.new(@identity, @host, @port, @unique_id, @type, true)
    end # def broadcast

    def unicast
      PRUNE::TargetKey.new(@identity, @host, @port, @unique_id, @type, false)
    end # def unicast

    def ==(instance)
      instance == self.unicast.key
    end # def ==

  end # class TargetKey

end # module PRUNE
