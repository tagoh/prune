# plugin.rb
# Copyright (C) 2004-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'mutex_m'
require 'thread'
require 'prune/event'
require 'prune/debug'
##require 'prune/kernel'
require 'prune/message'
require 'prune/prconfig'
require 'prune/version'


module PRUNE

=begin rdoc

== PRUNE::PluginManager

=end

  class PluginManager
    include Singleton
    include PRUNE::Debug

    UPDATE_MTIME = 0
    UPDATE_SIZE = 1
    UPDATE_MANUAL = 2

    MINIMUM_REQUIRED_REVISION = 291

=begin rdoc

=== PRUNE::PluginManager#instance

=end

    def initialize
      @eventmgr = PRUNE::EventManager.instance
      @pluginlist = {}
      @pluginlist.extend Mutex_m
      def @pluginlist.[]=(key, val)
        super(key.to_s, val)
      end
      def @pluginlist.[](key)
        super(key.to_s)
      end
      @pluginsigids = {}
      @pluginsigids.extend Mutex_m
      @mutex = Mutex.new
      @loading = nil
      @loaded = []
      @loaded.extend Mutex_m
      @plugindir = nil
      self.plugindir = PRUNE::Config::DefaultPluginDir
      @updatemode = UPDATE_MTIME
    end # def initialize

    attr_reader :loading, :plugindir
    attr_accessor :updatemode

    def inspect
      sprintf("#<%s:0x%x @pluginlist=%s, @pluginsigids=%s, @loaded=%s>", self.class, self.object_id, @pluginlist.inspect, @pluginsigids.inspect, @loaded.inspect)
    end # def inspect

=begin rdoc

=== PRUNE::PluginManager#plugindir=(val)

=end

    def plugindir=(val)
      if $:.include?(@plugindir) then
        $:.delete(@plugindir)
      end
      unless $:.include?(@plugindir) then
        $:.unshift(val)
      end
      @plugindir = val
    end # def plugindir

=begin rdoc

=== PRUNE::PluginManager#load_plugin(file, target, copts, popts)

=end

    def load_plugin(file, target, copts, popts)
      retval = true
      info("Loading the plugin `%s' for `%s'...", file, target)
      o = PRUNE::TYPE::PluginInfoStruct.new(file, target, copts, popts)
      begin
        o.mtime = File.mtime(File.join(@plugindir, file))
        o.size = File.size(File.join(@plugindir, file))
      rescue => e
        warning("%s", e.message)
      end
      ret = nil
      @mutex.synchronize do
        @loading = o
        begin
          ret = load(file, true)
        rescue SecurityError, TypeError, LoadError, SyntaxError, NameError => e
          warning("Unable to load the plugin file `%s'\n  result: %s\n%s", file, e.message, e.backtrace.join("\n"))
          retval = false
        ensure
          @loading = nil
        end
      end

      if ret == false then
        # the plugin has already been loaded.
        obj = nil
        catch(:found_plugin) do
          @pluginlist.synchronize do
            @pluginlist.each do |t, v|
              v.each do |ob|
                if ob.filename == file then
                  obj = ob
                  throw :found_plugin
                end
              end
            end # @pluginlist.each
          end # @pluginlist.synchronize
        end # catch
        if obj.nil?  then
          warning("Unable to load the plugin `%s'", file)
        else
          i = obj.instance.class.new
          ifs = PRUNE::PluginInterface.new(i, o.target, o.channel_options, o.plugin_options)
          o.instance = i
          o.interface = ifs
          add(o)
        end
      end

      broken = []
      @loaded.synchronize do
        @loaded.each do |x|
          if broken.include?(x.filename) then
            warning("Some instance of the plugin `%s' may be fully/partially broken. the instance of %s won't be activated.",
                    broken[broken.index(x.filename)], x.instance.class)
          else
            if !x.instance.respond_to?(:plugin_required_revision) then
              warning("Loading the plugin `%s' failed: no %s method available in %s.", x.filename, :plugin_required_revision, x.instance.class)
              broken << x.filename
              next
            end
            rev = x.instance.__send__(:plugin_required_revision)
            if !rev.kind_of?(Fixnum) || rev < MINIMUM_REQUIRED_REVISION then
              warning("Unable to satisfy the minimum requirements to work the plugin: the plugin `%s' has to be compatible with the revision %d", x.filename, MINIMUM_REQUIRED_REVISION)
            end
            if !rev.kind_of?(Fixnum) || PRUNE.revision < rev then
              warning("Unable to satisfy the requirements of %s in %s: required revision %s but %s", x.instance.class, x.filename, rev, PRUNE.revision)
              broken << x.filename
              next
            end
            if !x.instance.respond_to?(:plugin_type) then
              warning("Loading the plugin `%s' failed: no %s method available in %s.", x.filename, :plugin_type, x.instance.class)
              broken << x.filename
              next
            end
            ptype = x.instance.__send__(:plugin_type)
            if ptype == PRUNE::Plugin::TYPE_CONNECTOR && !x.target.is_connector? then
              warning("Loading the plugin `%s' failed: this plugin is supposed to work at the server-side plugin, but loading at the client-side.", x.filename)
              broken << x.filename
              next
            end
            if ptype == PRUNE::Plugin::TYPE_LISTENER && x.target.is_connector? then
              warning("Loading the plugin `%s' failed: this plugin is supposed to work at the client-side plugin, but loading at the server-side.", x.filename)
              broken << x.filename
              next
            end
            if x.instance.respond_to?(:plugin_register) then
              sigids = x.instance.__send__(:plugin_register, x.interface)
            else
              prefix = ''
              if x.instance.respond_to?(:plugin_prefix) then
                prefix = x.instance.__send__(:plugin_prefix)
              end
              priority = PRUNE::Plugin::PRIOR_SYNC_NORMAL
              if x.instance.respond_to?(:plugin_priority) then
                priority = x.instance.__send__(:plugin_priority)
              end
              sigids = x.interface.auto_registration(prefix, priority)
              checker = Proc.new do |_self, val|
                val.values.map do |v|
                  if v.kind_of?(Hash) then
                    _self.call(_self, v)
                  else
                    v
                  end
                end
              end
              if checker.call(checker, sigids).flatten.compact.empty? then
                warning("No callbacks are registered for %s in `%s'. is it expected thing?", x.instance.class, x.filename) # "
              end
            end
            @pluginsigids.synchronize do
              @pluginsigids["#{x.target}:#{x.filename}:#{x.instance.class}"] = sigids
            end
            if x.instance.respond_to?(:plugin_init) then
              x.instance.__send__(:plugin_init, x.plugin_options)
            end
            @eventmgr.emit(x.target.unicast, :LoadedPlugin, nil, x.filename, x.channel_options, x.plugin_options)
          end # if broken.include?(x.filename)
        end # @loaded.each
        @loaded.clear
      end # @loaded.synchronize
      broken.each do |f|
        unload_plugin(f, target)
        retval = false
      end

      retval
    end # def load_plugin

=begin rdoc

=== PRUNE::PluginManager#unload_plugin(file, target)

=end

    def unload_plugin(file, target)
      @pluginlist.synchronize do
        @pluginlist[target].delete_if do |x|
          if x.filename == file then
            info("Unloading the plugin `%s' for `%s'", file, target)
            key = sprintf("%s:%s:%s", target, file, x.instance.class)
            @pluginsigids.synchronize do
              unless @pluginsigids[key].nil? then
                @eventmgr.auto_unregistration(@pluginsigids[key])
                @pluginsigids.delete(key)
              end
            end
            if x.instance.respond_to?(:plugin_finalize) then
              x.instance.__send__(:plugin_finalize, x.interface, x.plugin_options)
            end
##            ObjectCache.delete(file)
            true
          else
            false
          end
        end
        if @pluginlist[target].empty? then
          @pluginlist.delete(target)
        end
      end
    end # def unload_plugin

=begin rdoc

=== PRUNE::PluginManager#reload_plugin(file, target, copts, popts)

=end

    def reload_plugin(file, target, copts, popts)
      retval = false

      @pluginlist.synchronize do
        @pluginlist[target].each do |o|
          if o.filename == file then
            info("Reloading the plugin configuration for `%s' on `%s'...", file, target)
            ifs = o.interface
            ifs.__send__(:instance_variable_set, "@channel_options", copts)
            ifs.__send__(:instance_variable_set, "@plugin_options", popts)
            o.channel_options = copts
            o.plugin_options = popts
            retval = true
            break
          end
        end
      end

      unless retval then
        retval = load_plugin(file, target, copts, popts)
      end

      retval
    end # def reload_plugin

=begin rdoc

=== PRUNE::PluginManager#update_plugins(mode = nil)

=end

    def update_plugins(mode = nil)
      mode = @updatemode if mode.nil?
      plist = {}
      pupdated = []
      orig_pluginlist = {}
      @pluginlist.synchronize do
        @pluginlist.each do |starget, v|
          target = PRUNE::TargetKey.parse(starget)
          orig_pluginlist[target] = []
          v.each do |obj|
            orig_pluginlist[target] << obj
            plist[obj.filename] ||= obj
          end
        end
      end
      plist.each do |f, obj|
        if mode == UPDATE_MTIME then
          mtime = nil
          begin
            mtime = File.mtime(File.join(@plugindir, f))
          rescue => e
            warning("%s", e.message)
          end
          if !mtime.nil? && mtime > obj.mtime then
            info("detected the plugin `%s' updated (from the updated mtime)", f)
            pupdated << f
          end
        elsif mode == UPDATE_SIZE then
          size = nil
          begin
            size = File.size(@plugindir, f)
          rescue => e
            warning("%s", e.message)
          end
          if !size.nil? && size != obj.size then
            info("detected the plugin `%s' updated (from the different file size)", f)
            pupdated << f
          end
        elsif mode == UPDATE_MANUAL then
          info("Forcibly updating the plugin '%s'", f)
          pupdated << f
        end
      end
      reloaded = []
      orig_pluginlist.each do |target, v|
        v.each do |obj|
          if pupdated.include?(obj.filename) then
            unless reloaded.include?(obj.filename) then
              unload_plugin(obj.filename, target)
              reloaded << obj.filename
            end
            load_plugin(obj.filename, target, obj.channel_options, obj.plugin_options)
          end
        end
      end
    end # def update_plugins

=begin rdoc

=== PRUNE::PluginManager#add(obj)

=end

    def add(obj)
      reload = []

      @pluginlist.synchronize do
        @pluginlist[obj.target] ||= []
        @pluginlist[obj.target].each do |o|
          if obj.filename == o.filename then
            reload << o
          end
        end
        @pluginlist[obj.target] << obj
      end

      unless reload.empty? then
        reload.each do |o|
          info("Replacing the instance of the plugin `%s' from `%s'...",
               o.instance.class, o.filename)
          unload_plugin(o.filename, o.target)
        end
      end

      @loaded.synchronize do
        @loaded << obj
      end
    end # def add

  end # class PluginManager

=begin rdoc

== PRUNE::PluginInterface

=end

  class PluginInterface
    include PRUNE::Debug

=begin rdoc

=== PRUNE::PluginInterface#new(instance, target, copts, popts)

=end

    def initialize(instance, target, copts, popts)
      raise ArgumentError, sprintf("Object isn't a plugin instance, but %s", instance.class) unless instance.kind_of?(PRUNE::Plugin)

      @eventmgr = PRUNE::EventManager.instance
      @instance = instance
      @target = target
      @state = nil
      @channel_options = copts.nil? ? nil : copts.dup.freeze
      @plugin_options = popts.nil? ? nil : popts.dup.freeze
      @result = nil
    end # def initialize

    attr_accessor :result
    attr_reader :channel_options, :plugin_options, :state, :target

    def inspect
      sprintf("#<%s:0x%x @instance=%s, @target=%s, @state=%s, @channel_options=%s, @plugin_options=%s, @result=%s>", self.class, self.object_id, @instance.inspect, @target.inspect, @state.inspect, @channel_options.inspect, @plugin_options.inspect, @result.inspect)
    end # def inspect

=begin rdoc

=== PRUNE::PluginInterface#auto_registration(prefix, priority = PRUNE::Plugin::PRIOR_SYNC_NORMAL)

=end

    def auto_registration(prefix, priority = PRUNE::Plugin::PRIOR_SYNC_NORMAL)
      if priority < PRUNE::Plugin::PRIOR_SYNC_HIGH ||
          (priority > PRUNE::Plugin::PRIOR_SYNC_LAST &&
           priority < PRUNE::Plugin::PRIOR_ASYNC_HIGH) then
        PRUNE.Fail(PRUNE::Error::InvalidPriority, signal, @instance.class)
      end
      @eventmgr.auto_registration(@target, @instance, prefix, priority, self)
    end # def auto_registration

=begin rdoc

=== PRUNE::PluginInterface#register(signal, priority, function, *args)

=end

    def register(signal, priority, function, *args)
      if priority < PRUNE::Plugin::PRIOR_SYNC_HIGH ||
          (priority > PRUNE::Plugin::PRIOR_SYNC_LAST &&
           priority < PRUNE::Plugin::PRIOR_ASYNC_HIGH) then
        PRUNE.Fail(PRUNE::Error::InvalidPriority, signal, @instance.class)
      end
      @eventmgr.register(@target, signal, priority, @instance, function, self, *args)
    end # def register

=begin rdoc

=== PRUNE::PluginInterface#auto_unregistration(hash)

=end

    def auto_unregistration(hash)
      @eventmgr.auto_unregistration(hash)
    end # def auto_unregistration

=begin rdoc

=== PRUNE::PluginInterface#unregister(handler_id)

=end

    def unregister(handler_id)
      @eventmgr.unregister(handler_id)
    end # def unregister

=begin rdoc

=== PRUNE::PluginInterface#synchronize(mode = true)

=end

    def synchronize(mode = true)
      @eventmgr.synchronize(mode) do
        yield self
      end
    end # def synchronize

=begin rdoc

=== PRUNE::PluginInterface#emit(signal, subsignal, *data)

=end

    def emit(signal, subsignal, *data)
      if (signal == :Received || signal == :Sent) && data[0].kind_of?(PRUNE::Message::Core) then
        emit_message(signal, *data)
      else
        @eventmgr.emit(@target.unicast, signal, subsignal, *data)
      end
    end # def emit

=begin rdoc

=== PRUNE::PluginInterface#emit_message(signal, message, target = @target)

=end

    def emit_message(signal, message, target = @target.unicast)
      raise TypeError, sprintf("Can't convert %s into PRUNE::Message::Core", message.class) unless message.kind_of?(PRUNE::Message::Core)

      if message.nick.nil? && message.user.nil? && message.host.nil? then
        message.nick = @state.nick
        message.user = @state.user
        message.host = @state.host
      end

      @eventmgr.emit(target, signal, message.command, message)
    end # def emit_message

=begin rdoc

=== PRUNE::PluginInterface#get_target(channel)

=end

    def get_target(channel)
      retval = nil

      targets = @eventmgr.targets
      targets.each do |x, y|
        # XXX: it may be a bad idea to assume @blahblahblah is added by prune. there may be possibilities that the channel name contains it
        if channel =~ /#{y.identity}\Z/ then
          retval = y
          break
        end
      end

      retval
    end # def get_target

  end # class PluginInterface

=begin rdoc

== PRUNE::Plugin

=end

  class Plugin
    include PRUNE::Debug

    PRIOR_SYNC_HIGH = PRUNE::EventManager::PRIOR_USR_SYNC_HIGH
    PRIOR_SYNC_NORMAL = PRUNE::EventManager::PRIOR_USR_SYNC_NORMAL
    PRIOR_SYNC_LOW = PRUNE::EventManager::PRIOR_USR_SYNC_LOW
    PRIOR_SYNC_LAST = PRUNE::EventManager::PRIOR_USR_SYNC_LAST
    PRIOR_ASYNC_HIGH = PRUNE::EventManager::PRIOR_USR_ASYNC_HIGH
    PRIOR_ASYNC_NORMAL = PRUNE::EventManager::PRIOR_USR_ASYNC_NORMAL
    PRIOR_ASYNC_LOW = PRUNE::EventManager::PRIOR_USR_ASYNC_LOW
    PRIOR_ASYNC_LAST = PRUNE::EventManager::PRIOR_USR_ASYNC_LAST

    TYPE_CONNECTOR = 1
    TYPE_LISTENER = 2

=begin rdoc

=== PRUNE::Plugin#inherited(subclass)

=end

    def Plugin.inherited(subclass)
      p = PRUNE::PluginManager.instance
      obj = p.loading
      if obj.nil? then
        PRUNE::Debug.warning("loaded %s with the unexpected way. unable to activate this plugin.", subclass)
      else
        i = subclass.new
        ifs = PRUNE::PluginInterface.new(i, obj.target, obj.channel_options, obj.plugin_options)
        obj.instance = i
        obj.interface = ifs
        p.add(obj)
      end
    end # def Plugin.inherited

=begin rdoc

=== PRUNE::Plugin#plugin_prefix

=end

    def plugin_prefix
      ''
    end # def pluginprefix

=begin rdoc

=== PRUNE::Plugin#plugin_type

=end

    def plugin_type
      TYPE_CONNECTOR
    end # def plugin_type

  end # class Plugin

end # module PRUNE
