# lock_m.rb
# Copyright (C) 2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'monitor'


module PRUNE

  module Lock_m

=begin rdoc

=== PRUNE::Lock_m#extend_object(obj)

=end

    def self.extend_object(obj)
      obj.extend(MonitorMixin)
      obj.class.instance_methods(false).each do |m|

        module_eval <<-EOS,__FILE__,__LINE__+1
        class << obj
          alias :#{m.to_s.split(//).map {|x| sprintf("_%s", x.unpack("H*")[0])}.join} #{m}
          undef #{m}

          def method_missing(*args, &block)
            escaped = args[0].to_s.split(//).map {|x| sprintf("_%s", x.unpack("H*")[0])}.join
            if self.respond_to?(escaped) then
              self.synchronize do
                self.__send__(escaped, *args[1..-1], &block)
              end
            else
              raise NoMethodError, sprintf("undefined method %s for %s", args[0], self.class)
            end
          end # def method_missing
        end
        EOS
      end
    end # def extend_object

  end # module Lock_m

end # module PRUNE
