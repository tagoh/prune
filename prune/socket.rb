# socket.rb
# Copyright (C) 2004-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'delegate'
require 'singleton'
require 'socket'
require 'thread'
require 'prune/error'
require 'prune/debug'
require 'prune/key'
require 'prune/lock_m'
require 'prune/queue'

Thread.abort_on_exception = true if $DEBUG


module PRUNE

=begin rdoc

== PRUNE::SocketManager

PRUNE::SocketManager is a class to manage the sockets.

=end

  class SocketManager
    include Singleton
    include PRUNE::Debug

=begin rdoc

=== PRUNE::SocketManager#new

=end

    def initialize
      @readq = []
      @writeq = []
      @errorq = []
      @sendq = {}
      @lasttime = {}
      @is_running = false
      @thread = nil
      @thgrp = ThreadGroup.new
      @threadmap = {}
      @abort_on_parser_exception = false
    end # def initialize

    attr_reader :readq, :writeq, :errorq, :sendq
    attr_accessor :abort_on_parser_exception

=begin rdoc

=== PRUNE::SocketManager#is_running?

=end

    def is_running?
      return @is_running && !@thread.nil?
    end # def is_running?

=begin rdoc

=== PRUNE::SocketManager#start

=end

    def start
      return true if is_running?
      @is_running = true

      info("Starting socket manager...")
      @thread = Thread.new do
        while @is_running do
          sleep 0.1
          begin
            retval = IO.select(@readq, @writeq, @errorq, 0)
            unless retval.nil? then
              debug('socket/debug', "State changed in sockets.")
              # for read
              length = retval[0].length
              while length > 0 do
                rs = retval[0][retval[0].length - length]
                if @threadmap.has_key?(rs) && !@threadmap[rs].nil? && @threadmap[rs].alive? then
                  # postpone to deal with this to next IO.select
                  debug('socket/debug', "Waiting for finishing a job in %s", rs.target.key)
                else
                  @threadmap[rs] = Thread.new(rs) do |rrs|
                    if rrs.class.to_s =~ /Server/ then
                      debug('socket/manager', "Connecting to %s", rrs.target.key)
                      s = rrs.accept
                      debug('socket/manager', "Connected from %s", s.target.key)

                      # create an instance for listener
                      if s.respond_to?(:handler) &&
                          s.handler.respond_to?(:emit) then
                        s.handler.emit(rrs.target.unicast, :Connected, nil, s)
                      end
                    else
                      flag = false
                      begin
                        flag = rrs.eof?
                      rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
                        flag = true
                      end
                      if flag then
                        debug('socket/manager', "%s was closed.", rrs.target.key)
                        rrs.close
                        @readq.delete(rrs)
                        # destroy an instance for listener
                        if rrs.respond_to?(:handler) &&
                            rrs.handler.respond_to?(:emit) then
                          begin
                            rrs.handler.emit(rrs.target.unicast, :Disconnected, nil, rrs)
                          rescue PRUNE::Error::EventServiceNotAvailable
                            # ignore it.
                          end
                        end
                      else
                        str = rrs.gets
                        debug('socket/manager', "Received[%s]: %s", rrs.target.key, str.chomp("\r\n"))

                        if str.respond_to?(:force_encoding) then
                          # for Ruby 1.9, the string has to be ASCII-8BIT to avoid RegexpError.
                          str.force_encoding('ASCII-8BIT') if str.encoding.dummy?
                        end

                        if rrs.respond_to?(:handler) &&
                            rrs.handler.respond_to?(:emit) then
                          rrs.handler.emit(rrs.target.unicast, :Received, nil, str)
                        end
                      end
                    end
                    @threadmap.delete(rrs)
                    Thread.exit
                  end
                end
                length -= 1
              end
              # for write
              retval[1].each do |ws|
                FIXME("need to take care of write sockets here.")
                p ws
              end
              # for error
              retval[2].each do |es|
                FIXME("need to take care of error sockets here.")
                p es
              end
            end
          rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
            # check in the read queue
            @readq.dup.each do |rs|
              if rs.closed? then
                # destroy an instance for listener
                if rs.respond_to?(:handler) &&
                    rs.handler.respond_to?(:emit) then
                  rs.handler.emit(rs.target.unicast, :Disconnected, nil, rs)
                end
                @readq.delete(rs)
              end
            end
            # check in the write queue
            # check in the error queue
          end
          # check the sender threads
          @sendq.each do |k, v|
            unless v.empty? then
              th = v.shift
              now = Time.now
              flag = (@lasttime[k].nil? || (now - @lasttime[k]) >= 2) ? true : false

              if flag then
                @lasttime[k] = now
                th.wakeup
              else
                v.unshift(th)
              end
            end
          end
        end
        debug('socket/manager', "Stopping...")
        # clean up to finalize.
        @readq.delete_if do |rs|
          debug('socket/manager', "Closing %s", rs)
          rs.stop if rs.respond_to?(:stop)
          rs.close
          true
        end
        @writeq.delete_if do |ws|
          debug('socket/manager', "Closing %s", ws)
          ws.stop if ws.respond_to?(:stop)
          ws.close
          true
        end
        @errorq.delete_if do |es|
          debug('socket/manager', "Closing %s", es)
          es.stop if es.respond_to?(:stop)
          es.close
          true
        end
        debug('socket/manager', 'Stopped')
        Thread.exit
      end
    end # def start

=begin rdoc

=== PRUNE::SocketManager#stop

=end

    def stop
      @is_running = false
      @thread.join
      @thread = nil
    end # def stop

  end # class SocketManager

=begin rdoc

== PRUNE::SocketDelegator

=end

  class SocketDelegator < SimpleDelegator

=begin rdoc

=== PRUNE::SocketDelegator#new(handler, obj, target)

=end

    def initialize(handler, obj, target)
      @handler = handler
      @target = target

      super(obj)
    end # def initialize

    attr_reader :handler

    def inspect
      retval = sprintf("#<%s:0x%x %s handler:%s>",
                       self.class, self.object_id, __getobj__.inspect, @handler.inspect)

      return retval
    end # def inspect

=begin rdoc

=== PRUNE::SocketDelegator#target

=end

    def target
      @target
    end # def target

  end # class SocketDelegator

=begin rdoc

== PRUNE::ServerSocketDelegator

=end

  class ServerSocketDelegator < PRUNE::SocketDelegator

=begin rdoc

=== PRUNE::ServerSocketDelegator#accept

=end

    def accept
      s = __getobj__.__send__(:accept)
      s.extend(PRUNE::Debug)

      def s.__initvalue__
        @stop = false unless defined?(@stop)
        @floodcontrol = true unless defined?(@floodcontrol)
        unless defined?(@status) then
          @status = []
          @status.extend(PRUNE::Lock_m)
        end
      end # def __initvalue__
      def s.floodcontrol=(flag)
        @floodcontrol = flag
        unless @floodcontrol then
          self.stop
        end
      end # def floodcontrol=
      def s.status
        __initvalue__
        return @status
      end # def status
      def s.is_failed?
        !self.status.empty?
      end # def is_failed?

      def s.stop
        __initvalue__
        sockman = PRUNE::SocketManager.instance
        if sockman.sendq.has_key?(self.inspect) then
          @stop = true
          until sockman.sendq[self.inspect].empty? do
            sleep 0.1
          end
          @status.clear
          @stop = false
        end
      end # def stop
      def s.close
        self.stop
        begin
          super
        rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
        end
      end # def close
      def s.eof?
        begin
          super
        rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
          true
        end
      end # def eof?
      def s.puts(line)
        __initvalue__
        unless @floodcontrol then
          m = line.chomp("\r\n")
          debug('socket/flood', "Real-sending[no-flood]: %s", m)
          _m = sprintf("%s\r\n", m)
          super(_m)
        else
          raise PRUNE::Error::SocketServiceNotAvailable if @stop == true
          raise self.status.shift if self.is_failed?

          _s = PRUNE::TYPE::SendMessageStruct.new(line, Thread.current)

          Thread.new(_s) do |sms|
            Thread.exclusive do
              sockman = PRUNE::SocketManager.instance
              grp = sockman.instance_variable_get("@thgrp")
              grp.add(Thread.current)
              Thread.current[:info] = sms
              sockman.sendq[self.inspect] ||= PRUNE::Queue.new
              sockman.sendq[self.inspect] << Thread.current
            end
            Thread.stop

            begin
              m = sms.string.chomp("\r\n")
              debug('socket/flood', "Real-sending: %s", m)
              _m = sprintf("%s\r\n", m)
              super(_m)
            rescue => e
              @status << e
            end
            Thread.exit
          end # Thread.new
        end # unless @floodcontrol
      end # def puts

      target = PRUNE::TargetKey.new("client", @target.host, @target.port, s.object_id, PRUNE::TargetKey::TYPE_LISTENER)
      PRUNE::SocketDelegator.new(@handler, s, target)
    end # def accept

  end # class ServerSocketDelegator

=begin rdoc

== PRUNE::TCPSocket

=end

  class TCPSocket < TCPSocket
    include PRUNE::Debug

=begin rdoc

=== PRUNE::TCPSocket#new(host, port, prefix = nil)

=end

    def initialize(host, port, prefix = nil)
      @host = host
      @port = port
      @prefix = prefix
      @stop = false
      @floodcontrol = true
      @status = []
      @status.extend(PRUNE::Lock_m)

      super(host, port)
    end # def initialize

    attr_reader :prefix

    def inspect
      retval = sprintf("#<%s:0x%x host:%s port:%s prefix:%s>",
                       self.class, self.object_id, @host, @port, @prefix)

      return retval
    end # def inspect

=begin rdoc

=== PRUNE::TCPSocket#floodcontrol=

=end

    def floodcontrol=(flag)
      @floodcontrol = flag
      unless @floodcontrol then
        self.stop
      end
    end # def floodcontrol=

=begin rdoc

=== PRUNE::TCPSocket#status

=end

    def status
      return @status
    end # def status

=begin rdoc

=== PRUNE::TCPSocket#is_failed?

=end

    def is_failed?
      !self.status.empty?
    end # def is_failed?

=begin rdoc

=== PRUNE::TCPSocket#stop

=end

    def stop
      sockman = PRUNE::SocketManager.instance
      if sockman.sendq.has_key?(self.inspect) then
        @stop = true
        until sockman.sendq[self.inspect].empty? do
          sleep 0.1
        end
      end
      @status.clear
      @stop = false
    end # def stop

=begin rdoc

=== PRUNE::TCPSocket#close

=end

    def close
      self.stop
      begin
        super
      rescue IOError, SocketError, Errno::EPIPE, Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::ETIMEDOUT, Errno::EHOSTUNREACH
      end
    end # def close

=begin rdoc

=== PRUNE::TCPSocket#puts(line)

=end

    def puts(line)
      unless @floodcontrol then
        m = line.chomp("\r\n")
        debug('socket/flood', "Real-sending[no-flood]: %s", m)
        _m = sprintf("%s\r\n", m)
        super(_m)
      else
        raise PRUNE::Error::SocketServiceNotAvailable if @stop == true
        raise self.status.shift if self.is_failed?

        s = PRUNE::TYPE::SendMessageStruct.new(line, Thread.current)

        Thread.new(s) do |sms|
          sockman = PRUNE::SocketManager.instance
          grp = sockman.instance_variable_get("@thgrp")
          grp.add(Thread.current)
          Thread.current[:info] = sms
          sockman.sendq[self.inspect] ||= PRUNE::Queue.new
          sockman.sendq[self.inspect] << Thread.current
          Thread.stop

          begin
            m = sms.string.chomp("\r\n")
            debug('socket/flood', "Real-sending: %s", m)
            _m = sprintf("%s\r\n", m)
            super(_m)
          rescue => e
            @status << e
          end
          Thread.exit
        end # Thread.new
      end # unless @floodcontrol
    end # def puts

  end # class TCPSocket

=begin rdoc

== PRUNE::TCPServer

=end

  class TCPServer < TCPServer

=begin rdoc

=== PRUNE::TCPServer#new(host, port, prefix = nil)

=end

    def initialize(host, port, prefix = nil)
      @host = host
      @port = port
      @prefix = prefix

      super(host, port)
    end # def initialize

    def inspect
      retval = sprintf("#<%s:0x%x host:%s port:%s prefix:%s>",
                       self.class, self.object_id, @host, @port, @prefix)

      return retval
    end # def inspect

  end # class TCPServer

end # module PRUNE
