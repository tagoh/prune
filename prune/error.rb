# error.rb
# Copyright (C) 2006-2009 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'e2mmap'


module PRUNE

  class << self

    def Fail(*args)
      PRUNE::Error::Fail(*args)
    end # def Fail

  end

  module Error
    extend Exception2MessageMapper

    # for composer.rb
    def_exception(:InvalidMessageType, "`%s' isn't a message class.", ArgumentError)
    def_exception(:InvalidComposeTable, "Invalid compose table `%s' is given.", ArgumentError)
    # for event.rb
    def_exception(:InvalidSignal, "Invalid signal `%s' is given.", ArgumentError)
    def_exception(:NoCallBackFunction, "No method of `%s' in %s.", NoMethodError)
    def_exception(:InvalidSubSignal, "Invalid sub signal `%s' is given.", ArgumentError)
    def_exception(:EventServiceNotAvailable, "Event thread is going to shutdown now.", RuntimeError)
    def_exception(:InvalidTarget, "Invalid target `%s' is given.", ArgumentError)
    def_exception(:OutdateTarget, "Given target is outdated (%s >= %s). please use PRUNE::TargetKey#unicast or #broadcast to get new one.", ArgumentError)
    # for irc.rb
    def_exception(:AlreadyOpened, "Already opened.", RuntimeError)
    # for message.rb
    def_exception(:InvalidMessage, "Invalid message is given: %s", ArgumentError)
    def_exception(:LessParamsInMessage, "wrong number of parameters %d of %d: %s", ArgumentError)
    def_exception(:MoreParamsInMessage, "too much parameters %d of %d: %s", ArgumentError)
    def_exception(:InvalidCharset, "Invalid charset: %s", ArgumentError)
    # for parser.rb
    def_exception(:UnknownToken, "Unknown token `%s' found in `%s'", NameError)
    # for plugin.rb
    def_exception(:InvalidPlugin, "The instance of `%s' in `%s' isn't a valid plugin.", RuntimeError)
    def_exception(:NewerRevisionRequired, "plugin `%s' requires Prune rev%s, but the running revision is %s", RuntimeError)
    def_exception(:InvalidPriority, "Detected the invalid priority given to the signal %s in %s", RuntimeError)
    def_exception(:InvalidPluginType, "The plugin `%s' was designed to use at the %s-side, but it's not loaded there.", RuntimeError)
    # for queue.rb
    def_exception(:UnknownQueueFunction, "Unknown queue function `%s'", ArgumentError)
    # for socket.rb
    def_exception(:SocketServiceNotAvailable, "Socket `%s' is going to shutdown now.", RuntimeError)
    # for state.rb
    def_exception(:NotYetConnected, "Connection hasn't been established yet", RuntimeError)
    def_exception(:AlreadyLoggedIn, "You have already logged in without any authentications.", RuntimeError)
    def_exception(:UnableToLogOut, "Can't log out without disconnection.", RuntimeError)

  end # module Error
    
end # module PRUNE
