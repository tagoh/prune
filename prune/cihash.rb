# cihash.rb - Case Insensitive Hash class
# Copyright (C) 2004-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'delegate'
require 'prune/compat'


module PRUNE

=begin rdoc

== PRUNE::CiHash

PRUNE::CiHash class is the Hash class extended to deal with
the keys case-insensitively.  you can use this class as well
as Hash class  except some methods extended to take care of
the case-insensitive keys.

=end

  class CiHash < DelegateClass(Hash)

=begin rdoc

=== PRUNE::CiHash#new(default = nil)

Returns a new hash. any of ways to create an Hash instance is available.
but it will create PRUNE::CiHash instance. (see +Hash::new+ for more details).

=end

    def initialize(default = nil)
      @keymaps = {}

      h = super({})
      h.default = default
      h
    end # def initialize

=begin rdoc

=== PRUNE::CiHash#[](key)

Element Reference---Retrieves the _value_ object corresponding to the _key_
object, but case-insensitively. If not found, returns the a default value
(see +Hash::new+ for details).

=end

    def [](key)
      __getobj__[(key.downcase rescue key)]
    end # def []

=begin rdoc

=== PRUNE::CiHash#[]=(key, val)

Element Assignment---Associates the value given by _value_ with the key given
by _key_, but case-insensitively.

=end

    def []=(key, val)
      newkey = (key.downcase rescue key)
      @keymaps[newkey] = key unless @keymaps.has_key?(newkey)
      __getobj__[newkey] = val
    end # def []=

=begin rdoc

=== PRUNE::CiHash#clear

Removes all key-value pairs from the instance.

=end

    def clear
      @keymaps.clear

      __getobj__.clear
    end # def clear

=begin rdoc

=== PRUNE::CiHash#delete(key)

Deletes and returns a key-value pair from the instance whose key is equal
to _key_ case-insensitively. If the key is not found, returns
the _default value_. If the optional code block is given and the key is
not found, pass in the key and return the result of _block_.

=end

    def delete(key)
      __getobj__.delete((key.downcase rescue key))
    end # def delete

=begin rdoc

=== PRUNE::CiHash#fetch(key, *arg, &block)

Returns a value from the hash from the given key.
This method behaves similarly to Hash built-in class, except
the key is evaluated case-insensitively.

=end

    def fetch(key, *arg, &block)
      __getobj__.fetch((key.downcase rescue key), *arg, &block)
    end # def fetch

=begin rdoc

=== PRUNE::CiHash#values_at(*keys)

Return an array containing the values associated with the given keys.
This method behaves similarly to Hash built-in class, except
the key is evaluated case-insensitively.

=end

    def values_at(*keys)
      newkeys = keys.map {|x| (x.downcase rescue x)}
      __getobj__.values_at(*newkeys)
    end # def values_at

    alias :indices :values_at
    alias :indexes :values_at

=begin rdoc

=== PRUNE::CiHash#has_key?(key)

Returns +true+ if the given key is present in the instance.
This method behaves similarly to Hash built-in class, except
the key is evaluated case-insensitively.

=end

    def has_key?(key)
      __getobj__.has_key?((key.downcase rescue key))
    end # def has_key?

    alias :key? :has_key?
    alias :include? :has_key?
    alias :member? :has_key?

=begin rdoc

=== PRUNE::CiHash#to_origkey(key)

Returns the original key that is given when associating the value with the key.

=end

    def to_origkey(key)
      @keymaps[(key.downcase rescue key)]
    end # def to_origkey

=begin rdoc

=== PRUNE::CiHash#each_origkey(&block)

call-seq:each_origkey {|key| block } -> self

Calls _block_ once for each key in the instance, passing the key as a parameter.
This works as well as Hash#each_key.

=end

  def each_origkey(&block)
    @keymaps.each_value(&block)
  end # def each_origkey

=begin rdoc

=== PRUNE::CiHash#each_origpair(&block)

call-seq:orig_each {|key, value| block} -> self
each_origpair {|key, value| block} -> self

Calls _block_ once for each key in the instance, passing the key-value pair as parameters.
This works as well as Hash#each and Hash#each_pair.

=end

  def each_origpair(&block)
    __getobj__.each_pair do |k,v|
      yield @keymaps[k], v
    end
  end # def each_origpair

  alias :orig_each :each_origpair

=begin rdoc

=== PRUNE::CiHash#origkeys

Returns a new array populated with the keys from this hash.
This works as well as Hash#keys.

=end

  def origkeys
    __getobj__.keys.map {|x| @keymaps[x]}
  end # def origkeys

  end # class CiHash

end # module PRUNE
