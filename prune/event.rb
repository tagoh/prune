# event.rb - Event handler class
# Copyright (C) 2009-2010 Akira TAGOH

# Authors:
#   Akira TAGOH  <akira@tagoh.org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

require 'mutex_m'
require 'pp'
require 'singleton'
require 'stringio'
require 'thread'
require 'prune/debug'
require 'prune/error'
require 'prune/message'
require 'prune/queue'
require 'prune/pattern'
require 'prune/types'


Thread.abort_on_exception = true if $DEBUG


module PRUNE

=begin rdoc

== PRUNE::EventManager

PRUNE::EventManager is a sigleton class to handle a event.

=end

  class EventManager
    include Singleton
    include PRUNE::Debug

    Signals = {
      :Async => [nil],
      :Connected => nil,
      :Disconnected => nil,
      :Loggedin => nil,
      :ListenerLoggedin => nil,
      :Received => PRUNE::PATTERN::Commands.map {|n| n.split('|')}.flatten.push(*PRUNE::PATTERN::Responses.map {|n| n.split(',')[0]}),
      :Sent => PRUNE::PATTERN::Commands.map {|n| n.split('|')}.flatten.push(*PRUNE::PATTERN::Responses.map {|n| n.split(',')[0]}),
      :LoadPlugin => nil,
      :Reloaded => nil,
      :LoadedPlugin => nil,
      :UserDefined => [],
    }

    PRIOR_SYS_SYNC_EMERGENCY = 0
    PRIOR_SYS_SYNC_HIGH = 1
    PRIOR_SYS_SYNC_NORMAL = 5
    PRIOR_SYS_SYNC_LOW = 7
    PRIOR_SYS_SYNC_LAST = 9
    PRIOR_USR_SYNC_HIGH = 10
    PRIOR_USR_SYNC_NORMAL = 30
    PRIOR_USR_SYNC_LOW = 40
    PRIOR_USR_SYNC_LAST = 49
    PRIOR_SYS_ASYNC_HIGH = 50
    PRIOR_SYS_ASYNC_NORMAL = 55
    PRIOR_SYS_ASYNC_LOW = 60
    PRIOR_SYS_ASYNC_LAST = 79
    PRIOR_USR_ASYNC_HIGH = 80
    PRIOR_USR_ASYNC_NORMAL = 85
    PRIOR_USR_ASYNC_LOW = 90
    PRIOR_USR_ASYNC_LAST = 100

    class << self

      def is_valid_signal?(sig)
        Signals.has_key?(sig)
      end # def is_valid_signal?(sig)

    end # class 

=begin rdoc

=== PRUNE::EventManager#new

=end

    def initialize
      @dispatchlist = {}
      @dispatchlist.extend Mutex_m
      @registerer = nil
      @registerer.extend Mutex_m
      @signal2hid = {}
      @eventq = PRUNE::Queue.new
      @thqgrp = {}
      @thaqgrp = {}
      @max_thread = 20
      @is_running = false
      @thread = nil
      @statemap = {}
      @lastkey = nil

      self.start
    end # def initialize

    attr_accessor :max_thread
    attr_reader :statemap

=begin rdoc

=== PRUNE::EventManager#is_running?

=end

    def is_running?
      return @is_running && !@thread.nil?
    end # def is_running?

=begin rdoc

=== PRUNE::EventManager#dispatchlist(target, signal, subsignal, &block)

=end

    def dispatchlist(target, signal, subsignal, &block) # :yields: queue
      PRUNE.Fail(PRUNE::Error::InvalidSignal, signal) unless EventManager.is_valid_signal?(signal)
      PRUNE.Fail(PRUNE::Error::InvalidSubSignal, subsignal) if !subsignal.nil? && !Signals[signal].nil? && Signals[signal].class == Array && !Signals[signal].compact.empty? && !Signals[signal].include?(subsignal.upcase) && !Signals[signal].empty?

      retval = nil
      list = nil
      @dispatchlist.synchronize do
        key = nil
        if Signals[signal].class == Array && !subsignal.nil? then
          if Signals[signal].compact.empty? then
            # the special case for :Async.
            key = subsignal.downcase
          elsif Signals[signal].include?(subsignal.upcase) then
            key = subsignal.upcase
          end
        end
        if @signal2hid.include?(signal) && @signal2hid[signal].has_key?(key) then
          list = @signal2hid[signal][key]
        end
        unless list.nil? then
          list.sort! do |x, y|
            if !@dispatchlist.has_key?(x) then
              bug("Unknown handler id `%s' on %s", x, self)
              -1
            elsif !@dispatchlist.has_key?(y) then
              bug("Unknown handler id `%s' on %s", y, self)
            else
              dx = @dispatchlist[x]
              dy = @dispatchlist[y]
              dx.priority <=> dy.priority
            end
          end # list.sort!
          retval = list.to_a
          retval.delete_if do |x|
            y = @dispatchlist[x]
            target != y.target
          end
        else
          retval = []
        end # unless list.nil?
      end
      if !subsignal.nil? && !Signals[signal].compact.empty? then
        retval << dispatchlist(target, signal, nil)
        retval.flatten!
      end

      if block_given? then
        retval.each(&block)
      end

      retval
    end # def dispatchlist

=begin rdoc

=== PRUNE::EventManager#subsignals(target, signal)

=end

    def subsignals(target, signal)
      return [] if Signals[signal].class != Array

      retval = []

      @dispatchlist.synchronize do
        if @signal2hid.has_key?(signal) && !@signal2hid[signal].nil? then
          @signal2hid[signal].each do |k, hids|
            hids.each do |hid|
              v = @dispatchlist[hid]
              if v.target == target then
                retval << k
              end
            end
          end
        end
      end

      retval
    end # def subsignals

=begin rdoc

=== PRUNE::EventManager#print_dispatchlist(ios, target, signal, subsignal)

=end

    def print_dispatchlist(ios, target, signal, subsignal)
      i = 1
      ios.printf("%s(%s): target: %s\n", signal, (subsignal.nil? ? "NONE" : subsignal), target.unicast.key)
      dispatchlist(target, signal, subsignal) do |handler_id|
        ios.printf("  %d. %s#%s %s [%X]\n", i,
               @dispatchlist[handler_id].instance.class,
               @dispatchlist[handler_id].function,
               (@dispatchlist[handler_id].priority < PRIOR_SYS_ASYNC_HIGH ? "SYNC" : "ASYNC"),
               handler_id)
        i += 1
      end
    end # def print_dispatchlist

=begin rdoc

=== PRUNE::EventManager#targets

=end

    def targets
      retval = {}

      @dispatchlist.synchronize do
        @dispatchlist.each do |x, y|
          retval[y.target.to_s] = y.target unless retval.include?(y.target.to_s)
        end
      end

      retval
    end # def targets

=begin rdoc

=== PRUNE::EventManager#start

=end

    def start
      return true if is_running?
      @is_running = true

      @thread = Thread.new do
        while @is_running do
          unless @eventq.empty? then
            ev = @eventq.shift
            thg = nil
            maxthread = nil
            mode = :wait
            unless @statemap.include?(ev.target.unicast.key) then
              warning("No state object is registered for %s. discarding the event %s", ev.target.key, ev.inspect)
              next
            end

            key = ev.target.unicast.key
            if ev.signal == :Async then
              @thaqgrp[key] ||= {}
              @thaqgrp[key][ev.subsignal] ||= ThreadGroup.new
              thg = @thaqgrp[key][ev.subsignal]
              maxthread = 1
              mode = :discard
            else
              @thqgrp[key] ||= ThreadGroup.new
              thg = @thqgrp[key]
              maxthread = @max_thread
            end
            if thg.list.length > maxthread then
              thg.list[-1][:blame_taking_too_much_time] += 1
              if ev.signal == :Async then
                debug('event/dispatch/async', 'too many events are now proceeding. [%d/%d]', thg.list.length, maxthread)
              else
                debug('event/dispatch', 'too many events are now proceeding. [%d/%d]', thg.list.length, maxthread)
              end
              if thg.list[-1][:blame_taking_too_much_time] > thg.list[-1][:blame_threshold] then
                warning("Thread %s has been brought up at %s but still running. this may means dead-locking or may need to improve the footprint etc.", thg.list[-1].inspect, thg.list[-1][:started_time])
                thg.list[-1].keys.each do |k|
                  debug('event/debug', sprintf("  %s: %s", k, thg.list[-1][k].inspect))
                end
                debug('event/debug', "  %s", @dispatchlist[thg.list[-1][:handler_id]].inspect)
                thg.list[-1][:blame_taking_too_much_time] = 0
                thg.list[-1][:blame_threshold] += thg.list[-1][:blame_threshold]
              end

              if mode != :discard then
                @eventq.unshift(ev)
              else
                warning("Discarding the event %s", ev.inspect)
              end
            else
              Thread.new(ev, thg) do |event, thgrp|
                Thread.current[:started_time] = Time.now
                Thread.current[:blame_taking_too_much_time] = 0
                Thread.current[:blame_threshold] = @max_thread
                eid = Thread.current[:started_time].object_id
                if ev.signal == :Async then
                  debug('event/dispatch/async', "Signal `%s:%s' is emitting with %s as `%d'", event.signal, event.subsignal, event.data, eid)
                else
                  debug('event/dispatch', "Signal `%s:%s' is emitting with %s as `%d'", event.signal, event.subsignal, event.data, eid)
                end
                # show the order of the dispatches for the debugging purpose.
                if $DEBUG then
                  cat = self.category_list
                  if cat.list.include?('event/debug') && event.signal != :Async ||
                      cat.list.include?('event/async/debug') then
                    self.print_dispatchlist(STDOUT, event.target, event.signal, event.subsignal)
                  end
                end

                list = self.dispatchlist(event.target, event.signal, event.subsignal)
                dplist = @dispatchlist.dup
                until @thread.stop? do
                  sleep 0.01
                end
                thgrp.add(Thread.current)
                Thread.current[:handler_id] = 0
                Thread.current[:id] = eid
                Thread.current[:data] = event.data
                Thread.current[:discontinued] = false
                Thread.current[:caller_count] = 0
                Thread.current[:call_trace] = event.calltrace

                unless event.synchronous.nil? then
                  event.synchronous[:event_thread] = Thread.current
                  event.synchronous.wakeup
                end

                h = PRUNE::EventHandler.new(@statemap[event.target.unicast.key])
                @lastkey = event.target if @lastkey.nil? || @lastkey.time < event.target.time

                list.each do |handler_id|
                  Thread.current[:handler_id] = handler_id
                  Thread.current[:caller_count] += 1
                  detail = dplist[handler_id].dup
                  hh = h

                  if detail.priority >= PRIOR_SYS_ASYNC_HIGH then
                    @thread.wakeup
                  end

                  if event.signal == :Async then
                    debug('event/signal/async', "dispatch id `%d' for a handler id `%d'[%s::%s] of %s is being brought up", eid, handler_id, detail.instance, detail.function, self)
                  else
                    debug('event/signal', "dispatch id `%d' for a handler id `%d'[%s::%s] of %s is being brought up", eid, handler_id, detail.instance, detail.function, self)
                  end

                  dupdata = []
                  if event.signal == :Async then
                    dupdata << event.subsignal
                  end
                  if detail.args.kind_of?(Array) && defined?(PRUNE::PluginInterface) && detail.args[0].kind_of?(PRUNE::PluginInterface) then
                    hh = detail.args[0]
                    dupdata.push(*Marshal.load(Marshal.dump(detail.args[1..-1])))
                    hh.__send__(:instance_variable_set, "@state", @statemap[event.target.unicast.key])
                  else
                    dupdata.push(*Marshal.load(Marshal.dump(detail.args)))
                  end
                  dupdata.compact!
                  event.data.each do |x|
                    y = nil
                    begin
                      y = Marshal.load(Marshal.dump(x))
                    rescue TypeError
                      y = x
                    ensure
                      dupdata << y
                    end
                  end
                  begin
                    val = detail.instance.__send__(detail.function, hh, event.target, *dupdata)
                  rescue => e
                    msgs = ""
                    StringIO.open(msgs, "w") do |io|
                      io.printf("Exception raised: %s\n", e.exception)
                      io.printf("  Details: %s", detail.pretty_inspect)
                      io.printf("  Event: %s", event.pretty_inspect)
                      io.printf("  Trace: %s\n", e.backtrace.join("\n         "))
                      io.printf("  Callbacks:\n")
                      self.print_dispatchlist(io, event.target, event.signal, event.subsignal)
                    end
                    STDOUT.puts(msgs)
                    bug("%s", msgs)
                    emit_notice(hh.state.nick, sprintf("E: %s. see prune_bug_*.log for more details.", e.exception))
                  end
                  if event.signal == :Async then
                    debug('event/signal/async', "dispatch id `%d' for a handler id `%d' of %s was done with %s [result:%s]", eid, handler_id, self, val, hh.result)
                  else
                    debug('event/signal', "dispatch id `%d' for a handler id `%d' of %s was done with %s [result:%s]", eid, handler_id, self, val, hh.result)
                  end

                  h.result = hh.result if hh != h

                  if val == true then
                    Thread.current[:discontinued] = true
                    break
                  end
                end # list.each

                @thread.wakeup

                unless event.synchronous.nil? then
                  event.synchronous[:result] = h.result
                  event.synchronous[:discontinued] = Thread.current[:discontinued]
                end

                # XXX: a hack to keep the memory leaks minimum by unreferencing
                #      objects no longer used.
                Thread.current.keys.each do |k|
                  Thread.current[k] = nil
                end

                Thread.exit
              end # Thread.new
              Thread.stop
            end # if thg.list.length > maxthread
          end # unless @eventq.empty?
          sleep 0.01 if @eventq.empty?
        end
        Thread.exit
      end
    end # def start

=begin rdoc

=== PRUNE::EventManager#stop

=end

    def stop
      @is_running = false
      @thread.join
      @thread = nil
    end # def stop

=begin rdoc

=== PRUNE::EventManager#auto_registration(target, obj, prefix, prior = PRUNE::EventManager::PRIOR_USR_ASYNC_NORMAL, *args)

=end

    def auto_registration(target, obj, prefix, prior = PRUNE::EventManager::PRIOR_USR_ASYNC_NORMAL, *args)
      retval = {}
      retval[:Async] = {}
      obj.methods.each do |m|
        if m =~ /\A#{prefix}_([a-zA-Z0-9]+)_async\Z/ then
          subsig = $1
          retval[:Async][subsig] = register(target, :Async, prior, obj, m, subsig, *args)
        end
      end
      retval[:Async][nil] = register(target, :Async, prior, obj, sprintf("%s_async", prefix), nil, *args) if obj.respond_to?(sprintf("%s_async", prefix))
      retval[:Connected] = register(target, :Connected, prior, obj, sprintf("%s_connected", prefix), *args) if obj.respond_to?(sprintf("%s_connected", prefix))
      retval[:Disconnected] = register(target, :Disconnected, prior, obj, sprintf("%s_disconnected", prefix, *args)) if obj.respond_to?(sprintf("%s_disconnected", prefix))
      retval[:Loggedin] = register(target, :Loggedin, prior, obj, sprintf("%s_loggedin", prefix), *args) if obj.respond_to?(sprintf("%s_loggedin", prefix))
      retval[:ListenerLoggedin] = register(target, :ListenerLoggedin, prior, obj, sprintf("%s_lloggedin", prefix), *args) if obj.respond_to?(sprintf("%s_lloggedin", prefix))
      retval[:Received] = {}
      retval[:Received][nil] = register(target, :Received, prior, obj, sprintf("%s_received", prefix), nil, *args) if obj.respond_to?(sprintf("%s_received", prefix))
      PRUNE::PATTERN::Commands.map {|n| n.split('|')}.flatten.push(*PRUNE::PATTERN::Responses.map {|n| n.split(',')[0]}).each do |cmd|
        if obj.respond_to?(sprintf("%s_%s_received", prefix, cmd.downcase)) then
          retval[:Received][PRUNE::Message.sanitize(cmd)] = register(target, :Received, prior, obj, sprintf("%s_%s_received", prefix, cmd.downcase), cmd, *args)
        end
      end
      retval[:Sent] = {}
      retval[:Sent][nil] = register(target, :Sent, prior, obj, sprintf("%s_sent", prefix), nil, *args) if obj.respond_to?(sprintf("%s_sent", prefix))
      PRUNE::PATTERN::Commands.map {|n| n.split('|')}.flatten.push(*PRUNE::PATTERN::Responses.map {|n| n.split(',')[0]}).each do |cmd|
        if obj.respond_to?(sprintf("%s_%s_sent", prefix, cmd.downcase)) then
          retval[:Sent][PRUNE::Message.sanitize(cmd)] = register(target, :Sent, prior, obj, sprintf("%s_%s_sent", prefix, cmd.downcase), cmd, *args)
        end
      end
      retval[:LoadPlugin] = register(target, :LoadPlugin, prior, obj, sprintf("%s_loadplugin", prefix), *args) if obj.respond_to?(sprintf("%s_loadplugin", prefix))
      retval[:Reloaded] = register(target, :Reloaded, prior, obj, sprintf("%s_reloaded", prefix), *args) if obj.respond_to?(sprintf("%s_reloaded", prefix))
      retval[:LoadedPlugin] = register(target, :LoadedPlugin, prior, obj, sprintf("%s_loadedplugin", prefix), *args) if obj.respond_to?(sprintf("%s_loadedplugin", prefix))
      retval[:UserDefined] = {}
      if obj.respond_to?(:plugin_userdefined_subsignals) then
        obj.__send__(:plugin_userdefined_subsignals).each do |subsig|
          retval[:UserDefined][subsig] = register(target, :UserDefined, prior, obj, sprintf("%s_%s_userdefined", prefix, subsig), subsig, *args) if obj.respond_to?(sprintf("%s_%s_userdefined", prefix, subsig))
        end
      end
      retval
    end # def auto_registration

=begin rdoc

=== PRUNE::EventManager#register(target, signal, priority, instance, function, *args)

=end

    def register(target, signal, priority, instance, function, *args)
      PRUNE.Fail(PRUNE::Error::InvalidTarget, target) unless target.kind_of?(PRUNE::TargetKey)
      PRUNE.Fail(PRUNE::Error::InvalidSignal, signal) unless EventManager.is_valid_signal?(signal)
      PRUNE.Fail(PRUNE::Error::NoCallBackFunction, function, instance) unless instance.respond_to?(function)

      hid = Time.now.object_id
      @dispatchlist.synchronize do
        debug('event/debug', "Registering %s#%s for the signal %s with the priority %d on %s [%d]", instance, function, signal, priority, target.unicast.key, hid)
        @dispatchlist[hid] = PRUNE::TYPE::DispatchStruct.new(target.unicast, hid, signal, priority, instance, function, args)
        @signal2hid[signal] ||= {}
        key = nil

        if Signals[signal].class == Array && !args.empty? then
          key = args[0]
          # drop a argument.
          @dispatchlist[hid].args.shift
          unless key.nil? then
            if Signals[signal].compact.empty? then
              # the special case for Async.
              key.downcase!
            elsif Signals[signal].include?(key.upcase) then
              key.upcase!
            else
              warning("Unknown command `%s' is specified.", key)

              return 0
            end
          end
        end
        @signal2hid[signal][key] ||= PRUNE::Queue.new
        @signal2hid[signal][key] << hid
      end

      hid
    end # def register

=begin rdoc

=== PRUNE::EventManager#auto_unregistration(hash)

=end

    def auto_unregistration(hash)
      raise TypeError, sprintf("Can't convert %s into Hash", hash.class) unless hash.kind_of?(Hash)
      @registerer.synchronize do
        _auto_unregistration(hash)
      end
    end # def auto_unregistration

=begin rdoc

=== PRUNE::EventManager#unregister(handler_id)

=end

    def unregister(handler_id)
      @dispatchlist.synchronize do
        unless @dispatchlist.has_key?(handler_id) then
          warning("%s doesn't have a handler id `%s':\n  %s", self, handler_id, caller.join("\n  "))
        else
          stop_emission(handler_id)
          debug('event/debug', "Unregistering %s#%s for the signal %s on %s", @dispatchlist[handler_id].instance, @dispatchlist[handler_id].function, @dispatchlist[handler_id].signal, @dispatchlist[handler_id].target.key)
          @dispatchlist.delete(handler_id)
        end

        @signal2hid.delete_if do |sig, list|
          list.delete_if do |key, ids|
            ids.delete(handler_id)
            ids.empty?
          end
          list.empty?
        end
      end
    end # def unregister

=begin rdoc

=== PRUNE::EventManager#synchronize(mode = true)

=end

    def synchronize(mode = true)
      flag = Thread.current[:on_sync]
      Thread.current[:on_sync] = (mode == true)
      yield self
      Thread.current[:on_sync] = flag
    end # def synchronize

=begin rdoc

=== PRUNE::EventManager#is_on_sync?

=end

    def is_on_sync?
      Thread.current[:on_sync] == true
    end # def is_on_sync?

=begin rdoc

=== PRUNE::EventManager#emit(target, signal, subsignal, *data)

=end

    def emit(target, signal, subsignal, *data)
      PRUNE.Fail(PRUNE::Error::InvalidTarget, target) unless target.kind_of?(PRUNE::TargetKey)
      PRUNE.Fail(PRUNE::Error::OutdateTarget, @lastkey.time.strftime("%Y-%m-%d %H:%M:%S.%L"), target.time.strftime("%Y-%m-%d %H:%M:%S.%L")) if !@lastkey.nil? && @lastkey.time >= target.time
      bt = caller
      f = true
      bt.delete_if {|x| x.include?('emit') && f ? true : f = false}
      v = PRUNE::TYPE::EventQueueStruct.new(bt, target, signal, subsignal, data, is_on_sync? ? Thread.current : nil)
      @eventq << v
      if @thread.nil? then
        warning("No management thread is running")
      else
        @thread.wakeup
      end

      if is_on_sync? then
        Thread.stop
        th = Thread.current[:event_thread]
        th.join
        Thread.current[:event_thread] = nil

        return Thread.current[:discontinued], Thread.current[:result]
      end
    end # def emit

=begin rdoc

=== PRUNE::EventManager#emit_notice(sender, msg)

=end

    def emit_notice(sender, msg)
      t = targets
      target = nil
      t.each {|k, v| target = v if k =~ /\Aclient:/}
      unless target.nil? then
	m = msg
        unless msg.kind_of?(PRUNE::Message::Core) then
          m = PRUNE::Message::NOTICE.new(sender.nil? ? target.host : sender, msg)
        end
        emit(target.broadcast, :Sent, m.command, m)
      end
    end # def emit_notice


=begin rdoc

=== PRUNE::EventManager#stop_emission(handler_id)

=end

    def stop_emission(handler_id)
      @thqgrp.each do |k, v|
        v.list.each do |th|
          if th[:handler_id] == handler_id then
            if th[:keepalive] == true then
              debug('event/debug', "a signal id `%d' of handler id `%d' on %s was attempted to be killing. but hold on it because of the request.", th[:id], th[:handler_id], self)
            else
              debug('event/debug', "a signal id `%d' of handler id `%d' on %s was killed - [%s(%s)]", th[:id], th[:handler_id], self, @dispatchlist[th[:handler_id]], th[:data])
              th.exit
            end
          end
        end
      end
      @thaqgrp.each do |k, v|
        v.each do |kk, vv|
          vv.list.each do |th|
            if th[:handler_id] == handler_id then
              if th[:keepalive] == true then
                debug('event/debug', "a signal id `%d' of handler id `%d' on %s was attempted to be killing. but hold on it because of the request.", th[:id], th[:handler_id], self)
              else
                debug('event/debug', "a signal id `%d' of handler id `%d' on %s was killed - [%s(%s)]", th[:id], th[:handler_id], self, @dispatchlist[th[:handler_id]], th[:data])
                th.exit
              end
            end
          end
        end
      end
    end # def stop_emission

    private

    def _auto_unregistration(hash)
      hash.each do |k, v|
        if v.kind_of?(Hash) then
          _auto_unregistration(v)
        else
          unregister(v)
        end
      end
    end # def _auto_unregistration

  end # class EventManager

=begin rdoc

== PRUNE::EventHandler

=end

  class EventHandler

=begin rdoc

=== PRUNE::EventHandler#new(state)

=end

    def initialize(state)
      @result = nil
      @eventmgr = PRUNE::EventManager.instance
      @state = state
    end # def initialize

    attr_accessor :result
    attr_reader :state

=begin rdoc

=== PRUNE::EventHandler#register(target, signal, priority, instance, function, *args)

=end

    def register(target, signal, priority, instance, function, *args)
      @eventmgr.register(target, signal, priority, instance, function, *args)
    end # def register

=begin rdoc

=== PRUNE::EventHandler#unregister(handler_id)

=end

    def unregister(handler_id)
      @eventmgr.unregister(handler_id)
    end # def unregister

=begin rdoc

=== PRUNE::EventHandler#synchronize(mode = true)

=end

    def synchronize(mode = true)
      @eventmgr.synchronize(mode) do |i|
        yield self
      end
    end # def synchronize

=begin rdoc

=== PRUNE::EventHandler#emit(target, signal, subsignal, *data)

=end

    def emit(target, signal, subsignal, *data)
      if (signal == :Received || signal == :Sent) && data[0].kind_of?(PRUNE::Message::Core) then
        emit_message(target, signal, *data)
      else
        @eventmgr.emit(target, signal, subsignal, *data)
      end
    end # def emit

=begin rdoc

=== PRUNE::EventHandler#emit_message(target, signal, message, *data)

=end

    def emit_message(target, signal, message, *data)
      raise TypeError, sprintf("Can't convert %s into PRUNE::Message::Core", message.class) unless message.kind_of?(PRUNE::Message::Core)

      if message.nick.nil? && message.user.nil? && message.host.nil? then
        message.nick = @state.nick
        message.user = @state.user
        message.host = @state.host
      end

      @eventmgr.emit(target, signal, message.command, message, *data)
    end # def emit_message

=begin rdoc

=== PRUNE::EventHandler#direct_message(destination, target, message)

=end

    def direct_message(destination, target, message)
      if message.nick.nil? && message.user.nil? && message.host.nil? then
        message.nick = @state.nick
        message.user = @state.user
        message.host = @state.host
      end
      destination.call(self, target, message)
    end # def direct_message

=begin rdoc

=== PRUNE::EventHandler#stop_emission(handler_id)

=end

    def stop_emission(handler_id)
      @eventmgr.stop_emission(handler_id)
    end # def stop_emission

  end # class EventHandler

end # module PRUNE

# :enddoc:
if $0 == __FILE__ then
  class Foo
    def foo(*args)
      p args
    end # def foo
  end # class Foo
  class Bar
    include PRUNE::Debug

    def bar(h, *args)
      info("bar: %s", args)

      h.result = "foo"

      true
    end # def 
    def baz(h, *args)
      info("baz: %s", args)

      h.result = "bar"

      true
    end # def 

  end # class 

  m = PRUNE::EventManager.instance
  m.start
  m.max_thread = 5
#  m.category_list.add('event/signal', 'event/dispatch')
  m.category_list.add('event/debug')
  f = Foo.new
  b = Bar.new
  hid = m.register("", :Received, PRUNE::EventManager::PRIOR_SYS_SYNC_NORMAL, f, :foo)
  hid2 = m.register("", :Received, PRUNE::EventManager::PRIOR_SYS_ASYNC_NORMAL, b, :bar)
  hid3 = m.register("", :Received, PRUNE::EventManager::PRIOR_USR_SYNC_NORMAL, b, :baz)
  n = 1
  max = 20
  Thread.new do
    loop do
      if n > 40 then
        m.stop
        break
      elsif n > 30 then
        max = 40
      end
      if n > max then
        m.synchronize do
          p m.emit(nil, :Received, nil, n)
        end
        sleep 1
      else
        m.emit(nil, :Received, nil, n)
      end
      n += 1
    end
    Thread.exit
  end.join
end
